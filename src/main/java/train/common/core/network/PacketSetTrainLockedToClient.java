package train.common.core.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import train.common.Traincraft;
import train.common.api.AbstractTrains;

import java.util.ArrayList;
import java.util.List;

public class PacketSetTrainLockedToClient implements IMessage {

	boolean	bool;
    boolean requestPacket;
	int	entityID;
    int playerEntityID;
    List<String> trustedList = new ArrayList<>();
    public PacketSetTrainLockedToClient(){}

    @Deprecated
	public PacketSetTrainLockedToClient(boolean bool, int trainEntity) {
        this.bool = bool;
		this.entityID = trainEntity;
    }


    /**
     * <p>Client <-> Server communication packet to update lock and trusted list.</p>
     */
    public PacketSetTrainLockedToClient(boolean bool, List<String> trustedList, int trainEntity) {
        this.bool = bool;
        this.entityID = trainEntity;
        this.trustedList = trustedList;
        requestPacket = false;
    }

    /**
     * <p>Client -> Server communication packet to request lock and trusted list from server.</p>>
     */
    public PacketSetTrainLockedToClient(int trainEntity, int playerEntityID) {
        this.entityID = trainEntity;
        this.playerEntityID = playerEntityID;
        requestPacket = true;
    }

    @Override
    public void fromBytes(ByteBuf bbuf) {
        this.entityID = bbuf.readInt();
        if (!bbuf.readBoolean()) {
            requestPacket = false;
            this.bool = bbuf.readBoolean();
            int numberOfTrustedPlayers = bbuf.readInt();
            for (int i = 0; i < numberOfTrustedPlayers; i++) {
                trustedList.add(ByteBufUtils.readUTF8String(bbuf));
            }

        } else {
             requestPacket = true;
            this.playerEntityID = bbuf.readInt();
        }
    }

    @Override
    public void toBytes(ByteBuf bbuf) {
        bbuf.writeInt(this.entityID);
        bbuf.writeBoolean(requestPacket);
        if (!requestPacket) {
            bbuf.writeBoolean(this.bool);
            bbuf.writeInt(trustedList.size());
            for (String trustedPlayer : trustedList) {
                ByteBufUtils.writeUTF8String(bbuf, trustedPlayer);
            }
        } else {
            bbuf.writeInt(playerEntityID);
        }
    }

    public static class Handler implements IMessageHandler<PacketSetTrainLockedToClient, IMessage> {

        @Override
        public IMessage onMessage(PacketSetTrainLockedToClient message, MessageContext context) {
            if (context.side.isServer()) {
                Entity TrainEntity = context.getServerHandler().playerEntity.worldObj.getEntityByID(message.entityID);
                if (!message.requestPacket) {
                    if (TrainEntity instanceof AbstractTrains) {
                        ((AbstractTrains) TrainEntity).setTrainLockedFromPacket(message.bool);
                        ((AbstractTrains) TrainEntity).setTrustedList(message.trustedList);
//                            Traincraft.lockChannel.sendToAllAround(new PacketSetTrainLockedToClient(message.bool, message.trustedList, message.entityID), new NetworkRegistry.TargetPoint(TrainEntity.dimension, TrainEntity.posX, TrainEntity.posY, TrainEntity.posZ, 16D));
                        Traincraft.lockChannel.sendToAll(new PacketSetTrainLockedToClient(message.bool, message.trustedList, message.entityID));
                    }
                } else {
                    if (TrainEntity instanceof AbstractTrains) {
                        if (context.getServerHandler().playerEntity.worldObj.getEntityByID(message.playerEntityID) != null) {
                            Traincraft.lockChannel.sendTo(new PacketSetTrainLockedToClient(((AbstractTrains) TrainEntity).getTrainLockedFromPacket(), ((AbstractTrains) TrainEntity).getTrustedList(), message.entityID), ((EntityPlayerMP) context.getServerHandler().playerEntity.worldObj.getEntityByID(message.playerEntityID)));
                        }
                    }
                }
            } else {
                if (!message.requestPacket) {
                    Entity TrainEntity = Minecraft.getMinecraft().theWorld.getEntityByID(message.entityID);
                    if (TrainEntity instanceof AbstractTrains) {
                        ((AbstractTrains) TrainEntity).setTrustedList(message.trustedList);
                        ((AbstractTrains) TrainEntity).setTrainLockedFromPacket(message.bool);
                    }
                }
            }
            return null;
        }
    }
}