package train.common.entity.rollingStock;

import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import train.common.api.EntityRollingStock;
import train.common.api.IPassenger;
import train.common.overlaytexture.EnumOverlayFonts;
import train.common.overlaytexture.OTSpecificationDynamic;
import train.common.overlaytexture.OTSpecificationFixed;
import train.common.overlaytexture.OverlayTextureManager;

import java.awt.*;

public class EntityPassengerSLRV extends EntityRollingStock implements IPassenger {

	public EntityPassengerSLRV(World world) {
		super(world);
		initOverlayTextures(OverlayTextureManager.Type.BOTH);
		getOverlayTextureContainer().initSpecificationFixed(new OTSpecificationFixed("slrv_fixed_overlay.png", 80, 48, 11, new Point[]{ new Point(41, 154), new Point(88, 154) }));
		getOverlayTextureContainer().initSpecificationDynamic(new OTSpecificationDynamic(
				"Rollsign",
				46, 9, 11, EnumOverlayFonts.OxygenSansSmall, 16f, OTSpecificationDynamic.AlignmentMode.ALIGN_CENTER_AND_FILL,
				new Point[]{ new Point(42, 155), new Point(89, 155) })
		);
//		getOverlayTextureContainer().initSpecificationDynamic(new OTSpecificationDynamic(
//				"Car Number",
//				16, 5, 5, EnumOverlayFonts.OxygenSansSmall, 16f, OTSpecificationDynamic.AlignmentMode.ALIGN_CENTER_AND_FILL,
//				new Point[]{ new Point(118, 96) }
//		));
		textureDescriptionMap.put(0, "DART 'C' Car");
		textureDescriptionMap.put(1, "NET 'C' Car (Fictional)");
		textureDescriptionMap.put(2, "Penn Central 'C' Car (Fictional/Meme)");
	}

	public EntityPassengerSLRV(World world, double d, double d1, double d2) {
		this(world);
		setPosition(d, d1 + (double) yOffset, d2);
		motionX = 0.0D;
		motionY = 0.0D;
		motionZ = 0.0D;
		prevPosX = d;
		prevPosY = d1;
		prevPosZ = d2;
	}

	@Override
	public void updateRiderPosition() {
		if(riddenByEntity==null){return;}
		riddenByEntity.setPosition(posX, posY + getMountedYOffset() + riddenByEntity.getYOffset()-0.2f, posZ - 0.1f);
	}

	@Override
	public void setDead() {
		super.setDead();
		isDead = true;
	}

	@Override
	public boolean interactFirst(EntityPlayer entityplayer) {
		playerEntity = entityplayer;
		if ((super.interactFirst(entityplayer))) {
			return false;
		}
		if (!worldObj.isRemote) {
			ItemStack itemstack = entityplayer.inventory.getCurrentItem();
			if(lockThisCart(itemstack, entityplayer))return true;
			if (riddenByEntity != null && (riddenByEntity instanceof EntityPlayer) && riddenByEntity != entityplayer) {
				return true;
			}
			if (!worldObj.isRemote) {
				entityplayer.mountEntity(this);
			}
		}
		return true;
	}

	@Override
	public boolean canBeRidden() {
		return true;
	}

	@Override
	public boolean isStorageCart() {
		return false;
	}

	@Override
	public boolean isPoweredCart() {
		return false;
	}

	@Override
	public float getOptimalDistance(EntityMinecart cart) {
		return 1.6F;
	}
}