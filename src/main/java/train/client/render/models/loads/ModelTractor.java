//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 10.11.2022 - 21:56:40
// Last changed on: 10.11.2022 - 21:56:40

package train.client.render.models.loads; //Path where the model is located

import tmt.ModelConverter;
import tmt.ModelRendererTurbo;

public class ModelTractor extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 512;

	public ModelTractor() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[59];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 7
		bodyModel[1] = new ModelRendererTurbo(this, 25, 1, textureX, textureY); // Box 8
		bodyModel[2] = new ModelRendererTurbo(this, 73, 1, textureX, textureY); // Box 10
		bodyModel[3] = new ModelRendererTurbo(this, 89, 1, textureX, textureY); // Box 11
		bodyModel[4] = new ModelRendererTurbo(this, 105, 1, textureX, textureY); // Box 12
		bodyModel[5] = new ModelRendererTurbo(this, 129, 1, textureX, textureY); // Box 13
		bodyModel[6] = new ModelRendererTurbo(this, 145, 1, textureX, textureY); // Box 14
		bodyModel[7] = new ModelRendererTurbo(this, 161, 1, textureX, textureY); // Box 15
		bodyModel[8] = new ModelRendererTurbo(this, 185, 1, textureX, textureY); // Box 16
		bodyModel[9] = new ModelRendererTurbo(this, 209, 1, textureX, textureY); // Box 17
		bodyModel[10] = new ModelRendererTurbo(this, 225, 1, textureX, textureY); // Box 18
		bodyModel[11] = new ModelRendererTurbo(this, 249, 1, textureX, textureY); // Box 19
		bodyModel[12] = new ModelRendererTurbo(this, 273, 1, textureX, textureY); // Box 20
		bodyModel[13] = new ModelRendererTurbo(this, 289, 1, textureX, textureY); // Box 21
		bodyModel[14] = new ModelRendererTurbo(this, 305, 1, textureX, textureY); // Box 22
		bodyModel[15] = new ModelRendererTurbo(this, 321, 1, textureX, textureY); // Box 23
		bodyModel[16] = new ModelRendererTurbo(this, 337, 1, textureX, textureY); // Box 24
		bodyModel[17] = new ModelRendererTurbo(this, 361, 1, textureX, textureY); // Box 25
		bodyModel[18] = new ModelRendererTurbo(this, 369, 1, textureX, textureY); // Box 26
		bodyModel[19] = new ModelRendererTurbo(this, 409, 1, textureX, textureY); // Box 29
		bodyModel[20] = new ModelRendererTurbo(this, 441, 1, textureX, textureY); // Box 30
		bodyModel[21] = new ModelRendererTurbo(this, 345, 1, textureX, textureY); // Box 31
		bodyModel[22] = new ModelRendererTurbo(this, 25, 1, textureX, textureY); // Box 36
		bodyModel[23] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 37
		bodyModel[24] = new ModelRendererTurbo(this, 97, 1, textureX, textureY); // Box 39
		bodyModel[25] = new ModelRendererTurbo(this, 73, 1, textureX, textureY); // Box 45
		bodyModel[26] = new ModelRendererTurbo(this, 137, 1, textureX, textureY); // Box 46
		bodyModel[27] = new ModelRendererTurbo(this, 153, 1, textureX, textureY); // Box 47
		bodyModel[28] = new ModelRendererTurbo(this, 489, 1, textureX, textureY); // Box 48
		bodyModel[29] = new ModelRendererTurbo(this, 497, 1, textureX, textureY); // Box 49
		bodyModel[30] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // Box 50
		bodyModel[31] = new ModelRendererTurbo(this, 217, 1, textureX, textureY); // Box 51
		bodyModel[32] = new ModelRendererTurbo(this, 281, 1, textureX, textureY); // Box 52
		bodyModel[33] = new ModelRendererTurbo(this, 9, 9, textureX, textureY); // Box 54
		bodyModel[34] = new ModelRendererTurbo(this, 89, 9, textureX, textureY); // Box 55
		bodyModel[35] = new ModelRendererTurbo(this, 113, 9, textureX, textureY); // Box 56
		bodyModel[36] = new ModelRendererTurbo(this, 225, 9, textureX, textureY); // Box 57
		bodyModel[37] = new ModelRendererTurbo(this, 17, 9, textureX, textureY); // Box 58
		bodyModel[38] = new ModelRendererTurbo(this, 249, 9, textureX, textureY); // Box 59
		bodyModel[39] = new ModelRendererTurbo(this, 297, 1, textureX, textureY); // Box 60
		bodyModel[40] = new ModelRendererTurbo(this, 105, 9, textureX, textureY); // Box 61
		bodyModel[41] = new ModelRendererTurbo(this, 257, 9, textureX, textureY); // Box 116
		bodyModel[42] = new ModelRendererTurbo(this, 265, 9, textureX, textureY); // Box 256
		bodyModel[43] = new ModelRendererTurbo(this, 289, 9, textureX, textureY); // Box 259
		bodyModel[44] = new ModelRendererTurbo(this, 297, 9, textureX, textureY); // Box 260
		bodyModel[45] = new ModelRendererTurbo(this, 305, 9, textureX, textureY); // Box 261
		bodyModel[46] = new ModelRendererTurbo(this, 313, 9, textureX, textureY); // Box 262
		bodyModel[47] = new ModelRendererTurbo(this, 321, 9, textureX, textureY); // Box 263
		bodyModel[48] = new ModelRendererTurbo(this, 329, 9, textureX, textureY); // Box 264
		bodyModel[49] = new ModelRendererTurbo(this, 337, 9, textureX, textureY); // Box 265
		bodyModel[50] = new ModelRendererTurbo(this, 425, 9, textureX, textureY); // Box 266
		bodyModel[51] = new ModelRendererTurbo(this, 433, 9, textureX, textureY); // Box 267
		bodyModel[52] = new ModelRendererTurbo(this, 441, 9, textureX, textureY); // Box 268
		bodyModel[53] = new ModelRendererTurbo(this, 449, 9, textureX, textureY); // Box 269
		bodyModel[54] = new ModelRendererTurbo(this, 457, 9, textureX, textureY); // Box 270
		bodyModel[55] = new ModelRendererTurbo(this, 465, 9, textureX, textureY); // Box 271
		bodyModel[56] = new ModelRendererTurbo(this, 481, 9, textureX, textureY); // Box 273
		bodyModel[57] = new ModelRendererTurbo(this, 489, 9, textureX, textureY); // Box 274
		bodyModel[58] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Box 275

		bodyModel[0].addShapeBox(0F, 0F, 0F, 8, 3, 3, 0F,-2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		bodyModel[0].setRotationPoint(-15F, -2F, -8.5F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 17, 7, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 8
		bodyModel[1].setRotationPoint(-2F, -1F, -3F);

		bodyModel[2].addBox(0F, 0F, 0F, 1, 1, 8, 0F); // Box 10
		bodyModel[2].setRotationPoint(8.5F, 7F, -4F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 1F, 0F, 1F, 1F, 0F, 0F, 0F, 0F); // Box 11
		bodyModel[3].setRotationPoint(-9F, -2F, -9F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 8, 3, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F); // Box 12
		bodyModel[4].setRotationPoint(-15F, 9F, -8.5F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 1, 8, 3, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 13
		bodyModel[5].setRotationPoint(-16F, 0.999999999999999F, -8.5F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 1, 8, 3, 0F,0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 14
		bodyModel[6].setRotationPoint(-7F, 0.999999999999999F, -8.5F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 8, 8, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 15
		bodyModel[7].setRotationPoint(-15F, 0.999999999999999F, -8.5F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 8, 8, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		bodyModel[8].setRotationPoint(-15F, 0.999999999999999F, 5.5F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 1, 8, 3, 0F,0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F); // Box 17
		bodyModel[9].setRotationPoint(-16F, 0.999999999999999F, 5.5F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 8, 3, 3, 0F,-2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		bodyModel[10].setRotationPoint(-15F, -2F, 5.5F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 8, 3, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F, -2F, -2F, 0F); // Box 19
		bodyModel[11].setRotationPoint(-15F, 9F, 5.5F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 1, 8, 3, 0F,0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F); // Box 20
		bodyModel[12].setRotationPoint(-7F, 0.999999999999999F, 5.5F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 1F, 0F); // Box 21
		bodyModel[13].setRotationPoint(-16F, -2F, -9F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 1F, 0F, 1F, 1F, 0F, 0F, 0F, 0F); // Box 22
		bodyModel[14].setRotationPoint(-9F, -2F, 6F);

		bodyModel[15].addShapeBox(0F, 0F, 0F, 2, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 1F, 0F); // Box 23
		bodyModel[15].setRotationPoint(-16F, -2F, 6F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		bodyModel[16].setRotationPoint(-14F, -2F, 6F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 5, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		bodyModel[17].setRotationPoint(-14F, -2F, -9F);

		bodyModel[18].addBox(0F, 0F, 0F, 13, 1, 11, 0F); // Box 26
		bodyModel[18].setRotationPoint(-15F, 5F, -5.5F);

		bodyModel[19].addBox(0F, 0F, 0F, 13, 6, 1, 0F); // Box 29
		bodyModel[19].setRotationPoint(-15F, -1F, -6F);

		bodyModel[20].addBox(0F, 0F, 0F, 13, 6, 1, 0F); // Box 30
		bodyModel[20].setRotationPoint(-15F, -1F, 5F);

		bodyModel[21].addBox(0F, 0F, 0F, 1, 6, 10, 0F); // Box 31
		bodyModel[21].setRotationPoint(-3F, -1F, -5F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,-0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F); // Box 36
		bodyModel[22].setRotationPoint(6F, 5.5F, -5F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		bodyModel[23].setRotationPoint(7F, 5.5F, -5F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,-1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 39
		bodyModel[24].setRotationPoint(7F, 4.5F, -5F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F); // Box 45
		bodyModel[25].setRotationPoint(11F, 5.5F, -5F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F); // Box 46
		bodyModel[26].setRotationPoint(7F, 9.5F, -5F);

		bodyModel[27].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 47
		bodyModel[27].setRotationPoint(8.5F, 6F, -0.5F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F); // Box 48
		bodyModel[28].setRotationPoint(11F, 5.5F, 4F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 4, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		bodyModel[29].setRotationPoint(7F, 5.5F, 4F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,-0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -1F, 0F); // Box 50
		bodyModel[30].setRotationPoint(6F, 5.5F, 4F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,-1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 51
		bodyModel[31].setRotationPoint(7F, 4.5F, 4F);

		bodyModel[32].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, 0F); // Box 52
		bodyModel[32].setRotationPoint(7F, 9.5F, 4F);

		bodyModel[33].addBox(0F, 0F, 0F, 1, 3, 1, 0F); // Box 54
		bodyModel[33].setRotationPoint(-12F, 2F, -0.5F);

		bodyModel[34].addBox(0F, 0F, 0F, 4, 1, 4, 0F); // Box 55
		bodyModel[34].setRotationPoint(-13F, 0.999999999999999F, -2F);

		bodyModel[35].addBox(0F, 0F, 0F, 5, 1, 4, 0F); // Box 56
		bodyModel[35].setRotationPoint(-14F, -3.75F, -2F);
		bodyModel[35].rotateAngleZ = -1.30899694F;

		bodyModel[36].addShapeBox(0F, 0F, 0F, 7, 1, 1, 0F,0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 57
		bodyModel[36].setRotationPoint(-5.25F, -1F, -0.5F);
		bodyModel[36].rotateAngleZ = -1.08210414F;

		bodyModel[37].addBox(0F, 0F, 0F, 0, 3, 3, 0F); // Box 58
		bodyModel[37].setRotationPoint(-4.5F, -1.45F, -1.5F);
		bodyModel[37].rotateAngleZ = -1.08210414F;

		bodyModel[38].addBox(0F, 0F, 0F, 1, 7, 1, 0F); // Box 59
		bodyModel[38].setRotationPoint(9F, -8F, -2F);

		bodyModel[39].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.75F, 0F); // Box 60
		bodyModel[39].setRotationPoint(9.2F, -8.75F, -2F);
		bodyModel[39].rotateAngleZ = -0.33161256F;

		bodyModel[40].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, -0.5F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0.5F, 0F, -0.5F, 0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 61
		bodyModel[40].setRotationPoint(13.25F, 0.499999999999999F, -4.55F);

		bodyModel[41].addShapeBox(0F, 0F, 0F, 1, 2, 1, 0F,0F, -0.5F, 0.5F, -0.25F, 0F, 0.5F, -0.25F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0.5F, -0.25F, 0F, 0.5F, -0.25F, 0F, 0F, 0F, 0F, 0F); // Box 116
		bodyModel[41].setRotationPoint(13.25F, 0.499999999999999F, 3.55F);

		bodyModel[42].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 256
		bodyModel[42].setRotationPoint(15F, -1F, -3F);
		bodyModel[42].rotateAngleZ = -0.13962634F;

		bodyModel[43].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 259
		bodyModel[43].setRotationPoint(15F, -1F, 2.75F);
		bodyModel[43].rotateAngleZ = -0.13962634F;

		bodyModel[44].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 260
		bodyModel[44].setRotationPoint(15F, -1F, 1.75F);
		bodyModel[44].rotateAngleZ = -0.13962634F;

		bodyModel[45].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 261
		bodyModel[45].setRotationPoint(15F, -1F, 2.25F);
		bodyModel[45].rotateAngleZ = -0.13962634F;

		bodyModel[46].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 262
		bodyModel[46].setRotationPoint(15F, -1F, 0.75F);
		bodyModel[46].rotateAngleZ = -0.13962634F;

		bodyModel[47].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 263
		bodyModel[47].setRotationPoint(15F, -1F, 0F);
		bodyModel[47].rotateAngleZ = -0.13962634F;

		bodyModel[48].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 264
		bodyModel[48].setRotationPoint(15F, -1F, 1.25F);
		bodyModel[48].rotateAngleZ = -0.13962634F;

		bodyModel[49].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 265
		bodyModel[49].setRotationPoint(15F, -1F, -0.5F);
		bodyModel[49].rotateAngleZ = -0.13962634F;

		bodyModel[50].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 266
		bodyModel[50].setRotationPoint(15F, -1F, -1F);
		bodyModel[50].rotateAngleZ = -0.13962634F;

		bodyModel[51].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 267
		bodyModel[51].setRotationPoint(15F, -1F, -0.25F);
		bodyModel[51].rotateAngleZ = -0.13962634F;

		bodyModel[52].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 268
		bodyModel[52].setRotationPoint(15F, -1F, -2F);
		bodyModel[52].rotateAngleZ = -0.13962634F;

		bodyModel[53].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 269
		bodyModel[53].setRotationPoint(15F, -1F, -2.5F);
		bodyModel[53].rotateAngleZ = -0.13962634F;

		bodyModel[54].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 270
		bodyModel[54].setRotationPoint(15F, -1F, -1.5F);
		bodyModel[54].rotateAngleZ = -0.13962634F;

		bodyModel[55].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 271
		bodyModel[55].setRotationPoint(15F, -1F, 0.25F);
		bodyModel[55].rotateAngleZ = -0.13962634F;

		bodyModel[56].addShapeBox(0F, 0F, 0F, 1, 7, 1, 0F,0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 273
		bodyModel[56].setRotationPoint(9F, -1F, -2F);
		bodyModel[56].rotateAngleZ = -0.13962634F;

		bodyModel[57].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F,-0.25F, -0.2F, 0F, -0.75F, -0.2F, 0F, -0.75F, -0.2F, 0F, -0.25F, -0.2F, 0F, 0F, -0.75F, 0F, -0.75F, -0.75F, 0F, -0.75F, -0.75F, 0F, 0F, -0.75F, 0F); // Box 274
		bodyModel[57].setRotationPoint(15F, -1.25F, -3F);
		bodyModel[57].rotateAngleZ = -0.13962634F;

		bodyModel[58].addShapeBox(0F, 0F, 0F, 1, 1, 6, 0F,0F, -0.75F, 0F, -0.75F, -0.75F, 0F, -0.75F, -0.75F, 0F, 0F, -0.75F, 0F, -0.25F, -0.2F, 0F, -0.75F, -0.2F, 0F, -0.75F, -0.2F, 0F, -0.25F, -0.2F, 0F); // Box 275
		bodyModel[58].setRotationPoint(14.1F, 5.2F, -3F);
		bodyModel[58].rotateAngleZ = -0.13962634F;
	}
}