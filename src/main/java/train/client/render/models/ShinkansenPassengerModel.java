//FMT-Marker DFM-1.0
//Creator: Test Account

//Using PER-GROUP-INIT mode with limit '500'!
package train.client.render.models;


import tmt.ModelConverter;
import tmt.ModelRendererTurbo;

/** This file was exported via the (Default) FlansMod Exporter of<br>
 *  FMT (Fex's Modelling Toolbox) v.2.7.3 &copy; 2022 - Fexcraft.net<br>
 *  All rights reserved. For this Model's License contact the Author/Creator.
 */
public class ShinkansenPassengerModel extends ModelConverter {

	private int textureX = 512;
	private int textureY = 256;

	public ShinkansenPassengerModel(){
		bodyModel = new ModelRendererTurbo[802];
		//
		initGroup_bodyModel0();
		initGroup_bodyModel1();
	}

	private final void initGroup_bodyModel0(){
		bodyModel[0] = new ModelRendererTurbo(this, 119, 223, textureX, textureY);
		bodyModel[0].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[0].setRotationPoint(-75.25f, -7.0f, -11.0f);

		bodyModel[1] = new ModelRendererTurbo(this, 112, 223, textureX, textureY);
		bodyModel[1].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[1].setRotationPoint(-75.25f, -9.0f, -10.625f);

		bodyModel[2] = new ModelRendererTurbo(this, 424, 67, textureX, textureY);
		bodyModel[2].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[2].setRotationPoint(-68.75f, -5.0f, -11.0f);

		bodyModel[3] = new ModelRendererTurbo(this, 456, 65, textureX, textureY);
		bodyModel[3].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[3].setRotationPoint(-68.75f, -7.0f, -11.0f);

		bodyModel[4] = new ModelRendererTurbo(this, 484, 45, textureX, textureY);
		bodyModel[4].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[4].setRotationPoint(-68.75f, -9.0f, -10.625f);

		bodyModel[5] = new ModelRendererTurbo(this, 105, 223, textureX, textureY);
		bodyModel[5].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[5].setRotationPoint(-56.25f, -5.0f, -11.0f);

		bodyModel[6] = new ModelRendererTurbo(this, 98, 223, textureX, textureY);
		bodyModel[6].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[6].setRotationPoint(-56.25f, -7.0f, -11.0f);

		bodyModel[7] = new ModelRendererTurbo(this, 91, 223, textureX, textureY);
		bodyModel[7].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[7].setRotationPoint(-56.25f, -9.0f, -10.625f);

		bodyModel[8] = new ModelRendererTurbo(this, 443, 199, textureX, textureY);
		bodyModel[8].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[8].setRotationPoint(-54.25f, -9.0f, -10.625f);

		bodyModel[9] = new ModelRendererTurbo(this, 7, 223, textureX, textureY);
		bodyModel[9].addShapeBox(0, 0, 0, 2, 3, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[9].setRotationPoint(-75.25f, -3.0f, 10.0f);

		bodyModel[10] = new ModelRendererTurbo(this, 504, 222, textureX, textureY);
		bodyModel[10].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.125f, 0, -0.25f, -0.125f, 0, -0.25f, -0.375f, 0, -0.25f, -0.375f);
		bodyModel[10].setRotationPoint(-75.25f, 0.0f, 9.75f);

		bodyModel[11] = new ModelRendererTurbo(this, 32, 92, textureX, textureY);
		bodyModel[11].addShapeBox(0, 0, 0, 132, 1, 1, 0, 0, 0, 0, 0.375f, 0, 0, 0.375f, 0, -0.5f, 0, 0, -0.5f, 0, -0.75f, 0, 0.375f, -0.75f, 0, 0.375f, -0.75f, 0, 0, -0.75f, 0);
		bodyModel[11].setRotationPoint(-75.25f, 1.75f, 9.875f);

		bodyModel[12] = new ModelRendererTurbo(this, 32, 89, textureX, textureY);
		bodyModel[12].addShapeBox(0, 0, 0, 132, 1, 1, 0, 0, 0, 0, 0.375f, 0, 0, 0.375f, 0, 0, 0, 0, 0, 0, -0.875f, 0, 0.375f, -0.875f, 0, 0.375f, -0.875f, -0.5f, 0, -0.875f, -0.5f);
		bodyModel[12].setRotationPoint(-75.25f, 2.0f, 9.875f);

		bodyModel[13] = new ModelRendererTurbo(this, 32, 24, textureX, textureY);
		bodyModel[13].addShapeBox(0, 0, 0, 132, 3, 21, 0, 0, 0, 0.625f, 0.375f, 0, 0.625f, 0.375f, 0, 0.125f, 0, 0, 0.125f, 0, -0.25f, -0.875f, 0.375f, -0.25f, -0.875f, 0.375f, -0.25f, -1.375f, 0, -0.25f, -1.375f);
		bodyModel[13].setRotationPoint(-75.25f, 2.75f, -10.25f);

		bodyModel[14] = new ModelRendererTurbo(this, 410, 15, textureX, textureY);
		bodyModel[14].addShapeBox(0, 0, 0, 9, 2, 18, 0, 0, 0, 0.625f, 0, 0, 0.625f, 0, 0, 0.125f, 0, 0, 0.125f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.875f, 0, -0.4375f, -0.875f);
		bodyModel[14].setRotationPoint(-75.25f, 5.5f, -8.75f);

		bodyModel[15] = new ModelRendererTurbo(this, 240, 156, textureX, textureY);
		bodyModel[15].addShapeBox(0, 0, 0, 2, 1, 19, 0, 0, 0, 0.125f, -0.0625f, 0, 0.125f, -0.0625f, 0, -0.375f, 0, 0, -0.375f, 0, -0.25f, -0.35500002f, -0.0625f, -1, 0.125f, -0.0625f, -1, -0.375f, 0, -0.25f, -0.855f);
		bodyModel[15].setRotationPoint(-64.625f, 5.5f, -9.25f);

		bodyModel[16] = new ModelRendererTurbo(this, 32, 104, textureX, textureY);
		bodyModel[16].addShapeBox(0, 0, 0, 109, 1, 1, 0, 0, 0, -0.5f, -0.5f, 0, -0.5f, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.5f, -0.5f, 0, -0.5f, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[16].setRotationPoint(-68.75f, -4.0f, 10.0f);

		bodyModel[17] = new ModelRendererTurbo(this, 467, 222, textureX, textureY);
		bodyModel[17].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[17].setRotationPoint(-75.25f, -5.0f, -11.0f);

		bodyModel[18] = new ModelRendererTurbo(this, 436, 222, textureX, textureY);
		bodyModel[18].addShapeBox(0, 0, 0, 2, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[18].setRotationPoint(-75.25f, -3.0f, -11.0f);

		bodyModel[19] = new ModelRendererTurbo(this, 393, 222, textureX, textureY);
		bodyModel[19].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, -0.375f, 0, -0.25f, -0.375f, 0, -0.25f, -0.125f, 0, -0.25f, -0.125f);
		bodyModel[19].setRotationPoint(-75.25f, 0.0f, -10.75f);

		bodyModel[20] = new ModelRendererTurbo(this, 0, 128, textureX, textureY);
		bodyModel[20].addShapeBox(0, 0, 0, 108, 3, 1, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.25f, 0.5f, 0, -0.25f, 0.5f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[20].setRotationPoint(-68.75f, -3.0f, -11.0f);

		bodyModel[21] = new ModelRendererTurbo(this, 0, 124, textureX, textureY);
		bodyModel[21].addShapeBox(0, 0, 0, 108, 2, 1, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, -0.375f, 0.5f, -0.25f, -0.375f, 0.5f, -0.25f, -0.125f, 0, -0.25f, -0.125f);
		bodyModel[21].setRotationPoint(-68.75f, 0.0f, -10.75f);

		bodyModel[22] = new ModelRendererTurbo(this, 0, 101, textureX, textureY);
		bodyModel[22].addShapeBox(0, 0, 0, 109, 1, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[22].setRotationPoint(-68.75f, -4.0f, -11.0f);

		bodyModel[23] = new ModelRendererTurbo(this, 449, 33, textureX, textureY);
		bodyModel[23].addShapeBox(0, 0, 0, 9, 1, 16, 0, 0, 0, 0.625f, 0, 0, 0.625f, 0, 0, 0.125f, 0, 0, 0.125f, 0, -0.25f, -0.1875f, 0, -0.25f, -0.1875f, 0, -0.25f, -0.6875f, 0, -0.25f, -0.6875f);
		bodyModel[23].setRotationPoint(-75.25f, 7.0625f, -7.75f);

		bodyModel[24] = new ModelRendererTurbo(this, 404, 37, textureX, textureY);
		bodyModel[24].addShapeBox(0, 0, 0, 9, 1, 14, 0, 0, 0, 0.8125f, 0, 0, 0.8125f, 0, 0, 0.3125f, 0, 0, 0.3125f, 0, -0.25f, -0.0625f, 0, -0.25f, -0.0625f, 0, -0.25f, -0.5625f, 0, -0.25f, -0.5625f);
		bodyModel[24].setRotationPoint(-75.25f, 7.8125f, -6.75f);

		bodyModel[25] = new ModelRendererTurbo(this, 453, 140, textureX, textureY);
		bodyModel[25].addShapeBox(0, 0, 0, 2, 1, 19, 0, 0, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.375f, 0, 0, -0.375f, 0, -0.25f, -0.35500002f, -0.375f, -0.25f, -0.35500002f, -0.375f, -0.25f, -0.855f, 0, -0.25f, -0.855f);
		bodyModel[25].setRotationPoint(-66.25f, 5.5f, -9.25f);

		bodyModel[26] = new ModelRendererTurbo(this, 448, 74, textureX, textureY);
		bodyModel[26].addShapeBox(0, 0, 0, 2, 1, 19, 0, 0, 0, -0.35500002f, -0.375f, 0, -0.35500002f, -0.375f, 0, -0.855f, 0, 0, -0.855f, 0, -0.1875f, -0.875f, -1, -0.1875f, -0.875f, -1, -0.1875f, -1.375f, 0, -0.1875f, -1.375f);
		bodyModel[26].setRotationPoint(-66.25f, 6.25f, -9.25f);

		bodyModel[27] = new ModelRendererTurbo(this, 472, 74, textureX, textureY);
		bodyModel[27].addShapeBox(0, 0, 0, 1, 1, 16, 0, 0, 0, 0.625f, 0, 0, 0.625f, 0, 0, 0.125f, 0, 0, 0.125f, 0, -0.25f, -0.1875f, -0.3125f, -0.25f, -0.1875f, -0.3125f, -0.25f, -0.6875f, 0, -0.25f, -0.6875f);
		bodyModel[27].setRotationPoint(-66.25f, 7.0625f, -7.75f);

		bodyModel[28] = new ModelRendererTurbo(this, 392, 156, textureX, textureY);
		bodyModel[28].addShapeBox(0, 0, 0, 1, 1, 14, 0, 0, 0, 0.8125f, -0.3125f, 0, 0.8125f, -0.3125f, 0, 0.3125f, 0, 0, 0.3125f, 0, -0.25f, -0.0625f, -0.625f, -0.25f, -0.0625f, -0.625f, -0.25f, -0.5625f, 0, -0.25f, -0.5625f);
		bodyModel[28].setRotationPoint(-66.25f, 7.8125f, -6.75f);

		bodyModel[29] = new ModelRendererTurbo(this, 398, 74, textureX, textureY);
		bodyModel[29].addShapeBox(0, 0, 0, 2, 1, 19, 0, -0.0625f, 0, 0.125f, 0, 0, 0.125f, 0, 0, -0.375f, -0.0625f, 0, -0.375f, -0.0625f, -1, 0.125f, 0, -0.25f, -0.35500002f, 0, -0.25f, -0.855f, -0.0625f, -1, -0.375f);
		bodyModel[29].setRotationPoint(-41.75f, 5.5f, -9.25f);

		bodyModel[30] = new ModelRendererTurbo(this, 355, 73, textureX, textureY);
		bodyModel[30].addShapeBox(0, 0, 0, 2, 1, 19, 0, -0.375f, 0, 0.125f, 0, 0, 0.125f, 0, 0, -0.375f, -0.375f, 0, -0.375f, -0.375f, -0.25f, -0.35500002f, 0, -0.25f, -0.35500002f, 0, -0.25f, -0.855f, -0.375f, -0.25f, -0.855f);
		bodyModel[30].setRotationPoint(-40.125f, 5.5f, -9.25f);

		bodyModel[31] = new ModelRendererTurbo(this, 424, 72, textureX, textureY);
		bodyModel[31].addShapeBox(0, 0, 0, 2, 1, 19, 0, -0.375f, 0, -0.35500002f, 0, 0, -0.35500002f, 0, 0, -0.855f, -0.375f, 0, -0.855f, -1, -0.1875f, -0.875f, 0, -0.1875f, -0.875f, 0, -0.1875f, -1.375f, -1, -0.1875f, -1.375f);
		bodyModel[31].setRotationPoint(-40.125f, 6.25f, -9.25f);

		bodyModel[32] = new ModelRendererTurbo(this, 379, 73, textureX, textureY);
		bodyModel[32].addShapeBox(0, 0, 0, 1, 1, 16, 0, 0, 0, 0.625f, 0, 0, 0.625f, 0, 0, 0.125f, 0, 0, 0.125f, -0.3125f, -0.25f, -0.1875f, 0, -0.25f, -0.1875f, 0, -0.25f, -0.6875f, -0.3125f, -0.25f, -0.6875f);
		bodyModel[32].setRotationPoint(-39.125f, 7.0625f, -7.75f);

		bodyModel[33] = new ModelRendererTurbo(this, 361, 156, textureX, textureY);
		bodyModel[33].addShapeBox(0, 0, 0, 1, 1, 14, 0, -0.3125f, 0, 0.8125f, 0, 0, 0.8125f, 0, 0, 0.3125f, -0.3125f, 0, 0.3125f, -0.625f, -0.25f, -0.0625f, 0, -0.25f, -0.0625f, 0, -0.25f, -0.5625f, -0.625f, -0.25f, -0.5625f);
		bodyModel[33].setRotationPoint(-39.125f, 7.8125f, -6.75f);

		bodyModel[34] = new ModelRendererTurbo(this, 283, 101, textureX, textureY);
		bodyModel[34].addShapeBox(0, 0, 0, 58, 2, 18, 0, 0, 0, 0.625f, 0.125f, 0, 0.625f, 0.125f, 0, 0.125f, 0, 0, 0.125f, 0, -0.4375f, -0.375f, 0.125f, -0.4375f, -0.375f, 0.125f, -0.4375f, -0.875f, 0, -0.4375f, -0.875f);
		bodyModel[34].setRotationPoint(-38.125f, 5.5f, -8.75f);

		bodyModel[35] = new ModelRendererTurbo(this, 277, 122, textureX, textureY);
		bodyModel[35].addShapeBox(0, 0, 0, 58, 1, 16, 0, 0, 0, 0.625f, 0.125f, 0, 0.625f, 0.125f, 0, 0.125f, 0, 0, 0.125f, 0, -0.25f, -0.1875f, 0.125f, -0.25f, -0.1875f, 0.125f, -0.25f, -0.6875f, 0, -0.25f, -0.6875f);
		bodyModel[35].setRotationPoint(-38.125f, 7.0625f, -7.75f);

		bodyModel[36] = new ModelRendererTurbo(this, 263, 140, textureX, textureY);
		bodyModel[36].addShapeBox(0, 0, 0, 58, 1, 14, 0, 0, 0, 0.8125f, 0.125f, 0, 0.8125f, 0.125f, 0, 0.3125f, 0, 0, 0.3125f, 0, -0.25f, -0.0625f, 0.125f, -0.25f, -0.0625f, 0.125f, -0.25f, -0.5625f, 0, -0.25f, -0.5625f);
		bodyModel[36].setRotationPoint(-38.125f, 7.8125f, -6.75f);

		bodyModel[37] = new ModelRendererTurbo(this, 32, 86, textureX, textureY);
		bodyModel[37].addShapeBox(0, 0, 0, 132, 1, 1, 0, 0, 0, -0.5f, 0.375f, 0, -0.5f, 0.375f, 0, 0, 0, 0, 0, 0, -0.75f, 0, 0.375f, -0.75f, 0, 0.375f, -0.75f, 0, 0, -0.75f, 0);
		bodyModel[37].setRotationPoint(-75.25f, 1.75f, -10.875f);

		bodyModel[38] = new ModelRendererTurbo(this, 32, 83, textureX, textureY);
		bodyModel[38].addShapeBox(0, 0, 0, 132, 1, 1, 0, 0, 0, 0, 0.375f, 0, 0, 0.375f, 0, 0, 0, 0, 0, 0, -0.875f, -0.5f, 0.375f, -0.875f, -0.5f, 0.375f, -0.875f, 0, 0, -0.875f, 0);
		bodyModel[38].setRotationPoint(-75.25f, 2.0f, -10.875f);

		bodyModel[39] = new ModelRendererTurbo(this, 32, 80, textureX, textureY);
		bodyModel[39].addShapeBox(0, 0, 0, 132, 1, 1, 0, 0, 0, -0.5f, 0.375f, 0, -0.5f, 0.375f, 0, 0, 0, 0, 0, 0, -0.625f, -0.5f, 0.375f, -0.625f, -0.5f, 0.375f, -0.625f, 0, 0, -0.625f, 0);
		bodyModel[39].setRotationPoint(-75.25f, 2.125f, -10.875f);

		bodyModel[40] = new ModelRendererTurbo(this, 32, 0, textureX, textureY);
		bodyModel[40].addShapeBox(0, 0, 0, 132, 1, 22, 0, 0, -0.75f, -0.125f, 0.375f, -0.75f, -0.125f, 0.375f, -0.75f, -0.125f, 0, -0.75f, -0.125f, 0, 0, -0.125f, 0.375f, 0, -0.125f, 0.375f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[40].setRotationPoint(-75.25f, 1.75f, -11.0f);

		bodyModel[41] = new ModelRendererTurbo(this, 331, 222, textureX, textureY);
		bodyModel[41].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -1.0625f, 0, 0, -1.0625f, 0, 0, 0.5625f, 0, 0, 0.5625f, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f);
		bodyModel[41].setRotationPoint(-75.25f, -10.75f, -9.9375f);

		bodyModel[42] = new ModelRendererTurbo(this, 0, 120, textureX, textureY);
		bodyModel[42].addShapeBox(0, 0, 0, 108, 2, 1, 0, 0, 0, -1.0625f, 0.5f, 0, -1.0625f, 0.5f, 0, 0.5625f, 0, 0, 0.5625f, 0, -0.25f, 0, 0.5f, -0.25f, 0, 0.5f, -0.25f, -0.5f, 0, -0.25f, -0.5f);
		bodyModel[42].setRotationPoint(-68.75f, -10.75f, -9.9375f);

		bodyModel[43] = new ModelRendererTurbo(this, 32, 67, textureX, textureY);
		bodyModel[43].addShapeBox(0, 0, 0, 132, 1, 2, 0, 0, -0.375f, 0.25f, 0.375f, -0.375f, 0.25f, 0.375f, -0.125f, 0, 0, -0.125f, 0, 0, -0.125f, 0.25f, 0.375f, -0.125f, 0.25f, 0.375f, -0.5f, 0, 0, -0.5f, 0);
		bodyModel[43].setRotationPoint(-75.25f, -14.25f, -2.0f);

		bodyModel[44] = new ModelRendererTurbo(this, 314, 222, textureX, textureY);
		bodyModel[44].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[44].setRotationPoint(-75.25f, -7.0f, 10.0f);

		bodyModel[45] = new ModelRendererTurbo(this, 307, 222, textureX, textureY);
		bodyModel[45].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[45].setRotationPoint(-75.25f, -9.0f, 9.625f);

		bodyModel[46] = new ModelRendererTurbo(this, 485, 12, textureX, textureY);
		bodyModel[46].addShapeBox(0, 0, 0, 8, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[46].setRotationPoint(-68.75f, -5.0f, 10.0f);

		bodyModel[47] = new ModelRendererTurbo(this, 437, 43, textureX, textureY);
		bodyModel[47].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[47].setRotationPoint(-68.75f, -7.0f, 10.0f);

		bodyModel[48] = new ModelRendererTurbo(this, 406, 6, textureX, textureY);
		bodyModel[48].addShapeBox(0, 0, 0, 8, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[48].setRotationPoint(-68.75f, -9.0f, 9.625f);

		bodyModel[49] = new ModelRendererTurbo(this, 269, 222, textureX, textureY);
		bodyModel[49].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[49].setRotationPoint(-56.25f, -5.0f, 10.0f);

		bodyModel[50] = new ModelRendererTurbo(this, 140, 222, textureX, textureY);
		bodyModel[50].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[50].setRotationPoint(-56.25f, -7.0f, 10.0f);

		bodyModel[51] = new ModelRendererTurbo(this, 84, 222, textureX, textureY);
		bodyModel[51].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[51].setRotationPoint(-56.25f, -9.0f, 9.625f);

		bodyModel[52] = new ModelRendererTurbo(this, 33, 222, textureX, textureY);
		bodyModel[52].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[52].setRotationPoint(-36.75f, -5.0f, 10.0f);

		bodyModel[53] = new ModelRendererTurbo(this, 21, 222, textureX, textureY);
		bodyModel[53].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[53].setRotationPoint(-36.75f, -7.0f, 10.0f);

		bodyModel[54] = new ModelRendererTurbo(this, 0, 222, textureX, textureY);
		bodyModel[54].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[54].setRotationPoint(-36.75f, -9.0f, 9.625f);

		bodyModel[55] = new ModelRendererTurbo(this, 475, 221, textureX, textureY);
		bodyModel[55].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[55].setRotationPoint(-30.25f, -5.0f, 10.0f);

		bodyModel[56] = new ModelRendererTurbo(this, 453, 221, textureX, textureY);
		bodyModel[56].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[56].setRotationPoint(-30.25f, -7.0f, 10.0f);

		bodyModel[57] = new ModelRendererTurbo(this, 429, 221, textureX, textureY);
		bodyModel[57].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[57].setRotationPoint(-30.25f, -9.0f, 9.625f);

		bodyModel[58] = new ModelRendererTurbo(this, 386, 221, textureX, textureY);
		bodyModel[58].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[58].setRotationPoint(-23.75f, -5.0f, 10.0f);

		bodyModel[59] = new ModelRendererTurbo(this, 365, 221, textureX, textureY);
		bodyModel[59].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[59].setRotationPoint(-23.75f, -7.0f, 10.0f);

		bodyModel[60] = new ModelRendererTurbo(this, 358, 221, textureX, textureY);
		bodyModel[60].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[60].setRotationPoint(-23.75f, -9.0f, 9.625f);

		bodyModel[61] = new ModelRendererTurbo(this, 300, 221, textureX, textureY);
		bodyModel[61].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[61].setRotationPoint(-17.25f, -5.0f, 10.0f);

		bodyModel[62] = new ModelRendererTurbo(this, 293, 221, textureX, textureY);
		bodyModel[62].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[62].setRotationPoint(-17.25f, -7.0f, 10.0f);

		bodyModel[63] = new ModelRendererTurbo(this, 286, 221, textureX, textureY);
		bodyModel[63].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[63].setRotationPoint(-17.25f, -9.0f, 9.625f);

		bodyModel[64] = new ModelRendererTurbo(this, 360, 199, textureX, textureY);
		bodyModel[64].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[64].setRotationPoint(-54.25f, -9.0f, 9.625f);

		bodyModel[65] = new ModelRendererTurbo(this, 144, 199, textureX, textureY);
		bodyModel[65].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[65].setRotationPoint(-34.75f, -9.0f, 9.625f);

		bodyModel[66] = new ModelRendererTurbo(this, 432, 198, textureX, textureY);
		bodyModel[66].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[66].setRotationPoint(-28.25f, -9.0f, 9.625f);

		bodyModel[67] = new ModelRendererTurbo(this, 421, 198, textureX, textureY);
		bodyModel[67].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[67].setRotationPoint(-21.75f, -9.0f, 9.625f);

		bodyModel[68] = new ModelRendererTurbo(this, 410, 198, textureX, textureY);
		bodyModel[68].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[68].setRotationPoint(-15.25f, -9.0f, 9.625f);

		bodyModel[69] = new ModelRendererTurbo(this, 262, 221, textureX, textureY);
		bodyModel[69].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[69].setRotationPoint(-75.25f, -5.0f, 10.0f);

		bodyModel[70] = new ModelRendererTurbo(this, 255, 221, textureX, textureY);
		bodyModel[70].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.5625f, 0, 0, 0.5625f, 0, 0, -1.0625f, 0, 0, -1.0625f, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[70].setRotationPoint(-75.25f, -10.75f, 8.9375f);

		bodyModel[71] = new ModelRendererTurbo(this, 32, 116, textureX, textureY);
		bodyModel[71].addShapeBox(0, 0, 0, 108, 2, 1, 0, 0, 0, 0.5625f, 0.5f, 0, 0.5625f, 0.5f, 0, -1.0625f, 0, 0, -1.0625f, 0, -0.25f, -0.5f, 0.5f, -0.25f, -0.5f, 0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[71].setRotationPoint(-68.75f, -10.75f, 8.9375f);

		bodyModel[72] = new ModelRendererTurbo(this, 32, 63, textureX, textureY);
		bodyModel[72].addShapeBox(0, 0, 0, 132, 1, 2, 0, 0, -0.125f, 0, 0.375f, -0.125f, 0, 0.375f, -0.375f, 0.25f, 0, -0.375f, 0.25f, 0, -0.5f, 0, 0.375f, -0.5f, 0, 0.375f, -0.125f, 0.25f, 0, -0.125f, 0.25f);
		bodyModel[72].setRotationPoint(-75.25f, -14.25f, 0.0f);

		bodyModel[73] = new ModelRendererTurbo(this, 32, 56, textureX, textureY);
		bodyModel[73].addShapeBox(0, 0, 0, 132, 1, 5, 0, 0, -1.4375f, -0.25f, 0.375f, -1.4375f, -0.25f, 0.375f, 0, 0, 0, 0, 0, 0, 0.9375f, -0.25f, 0.375f, 0.9375f, -0.25f, 0.375f, -0.5f, 0, 0, -0.5f, 0);
		bodyModel[73].setRotationPoint(-75.25f, -13.875f, -7.25f);

		bodyModel[74] = new ModelRendererTurbo(this, 32, 77, textureX, textureY);
		bodyModel[74].addShapeBox(0, 0, 0, 132, 1, 1, 0, 0, 0.8125f, -2.0625f, 0.375f, 0.8125f, -2.0625f, 0.375f, 0.3125f, 1.0625f, 0, 0.3125f, 1.0625f, 0, -0.125f, -0.1875f, 0.375f, -0.125f, -0.1875f, 0.375f, -0.125f, -0.3125f, 0, -0.125f, -0.3125f);
		bodyModel[74].setRotationPoint(-75.25f, -11.625f, -9.0625f);

		bodyModel[75] = new ModelRendererTurbo(this, 32, 49, textureX, textureY);
		bodyModel[75].addShapeBox(0, 0, 0, 132, 1, 5, 0, 0, 0, 0, 0.375f, 0, 0, 0.375f, -1.4375f, -0.25f, 0, -1.4375f, -0.25f, 0, -0.5f, 0, 0.375f, -0.5f, 0, 0.375f, 0.9375f, -0.25f, 0, 0.9375f, -0.25f);
		bodyModel[75].setRotationPoint(-75.25f, -13.875f, 2.25f);

		bodyModel[76] = new ModelRendererTurbo(this, 32, 74, textureX, textureY);
		bodyModel[76].addShapeBox(0, 0, 0, 132, 1, 1, 0, 0, 0.3125f, 1.0625f, 0.375f, 0.3125f, 1.0625f, 0.375f, 0.8125f, -2.0625f, 0, 0.8125f, -2.0625f, 0, -0.125f, -0.3125f, 0.375f, -0.125f, -0.3125f, 0.375f, -0.125f, -0.1875f, 0, -0.125f, -0.1875f);
		bodyModel[76].setRotationPoint(-75.25f, -11.625f, 8.0625f);

		bodyModel[77] = new ModelRendererTurbo(this, 32, 71, textureX, textureY);
		bodyModel[77].addShapeBox(0, 0, 0, 132, 1, 1, 0, 0, 0, -0.5f, 0.375f, 0, -0.5f, 0.375f, 0, 0, 0, 0, 0, 0, -0.625f, -0.5f, 0.375f, -0.625f, -0.5f, 0.375f, -0.625f, 0, 0, -0.625f, 0);
		bodyModel[77].setRotationPoint(-75.25f, 2.125f, 9.375f);

		bodyModel[78] = new ModelRendererTurbo(this, 0, 159, textureX, textureY);
		bodyModel[78].addShapeBox(0, 0, 0, 97, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[78].setRotationPoint(-60.75f, -5.0f, -10.75f);

		bodyModel[79] = new ModelRendererTurbo(this, 0, 156, textureX, textureY);
		bodyModel[79].addShapeBox(0, 0, 0, 97, 2, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0.375f, 0, 0, 0.375f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[79].setRotationPoint(-60.75f, -7.0f, -10.75f);

		bodyModel[80] = new ModelRendererTurbo(this, 0, 153, textureX, textureY);
		bodyModel[80].addShapeBox(0, 0, 0, 97, 2, 0, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.6875f, 0, 0, 0.6875f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[80].setRotationPoint(-60.75f, -9.0f, -10.375f);

		bodyModel[81] = new ModelRendererTurbo(this, 0, 151, textureX, textureY);
		bodyModel[81].addShapeBox(0, 0, 0, 97, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0.5f, 0, 0, 0.5f);
		bodyModel[81].setRotationPoint(-60.75f, -5.0f, 10.25f);

		bodyModel[82] = new ModelRendererTurbo(this, 0, 148, textureX, textureY);
		bodyModel[82].addShapeBox(0, 0, 0, 97, 2, 0, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0.125f, 0, 0, 0.125f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0.5f, 0, 0, 0.5f);
		bodyModel[82].setRotationPoint(-60.75f, -7.0f, 10.25f);

		bodyModel[83] = new ModelRendererTurbo(this, 0, 145, textureX, textureY);
		bodyModel[83].addShapeBox(0, 0, 0, 97, 2, 0, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.1875f, 0, 0, -0.1875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0.5f, 0, 0, 0.5f);
		bodyModel[83].setRotationPoint(-60.75f, -9.0f, 9.875f);

		bodyModel[84] = new ModelRendererTurbo(this, 341, 172, textureX, textureY);
		bodyModel[84].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, -0.5f, -0.25f, -0.625f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[84].setRotationPoint(-75.25f, 0.0f, -10.5f);

		bodyModel[85] = new ModelRendererTurbo(this, 322, 172, textureX, textureY);
		bodyModel[85].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[85].setRotationPoint(-75.25f, -7.0f, -10.5f);

		bodyModel[86] = new ModelRendererTurbo(this, 220, 172, textureX, textureY);
		bodyModel[86].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -1.0625f, -0.5f, 0, -1.0625f, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[86].setRotationPoint(-75.25f, -9.0f, -10.5f);

		bodyModel[87] = new ModelRendererTurbo(this, 201, 172, textureX, textureY);
		bodyModel[87].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[87].setRotationPoint(-75.25f, -5.0f, -10.5f);

		bodyModel[88] = new ModelRendererTurbo(this, 182, 172, textureX, textureY);
		bodyModel[88].addShapeBox(0, 0, 0, 1, 3, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[88].setRotationPoint(-75.25f, -3.0f, -10.5f);

		bodyModel[89] = new ModelRendererTurbo(this, 293, 173, textureX, textureY);
		bodyModel[89].addShapeBox(0, 0, 0, 1, 2, 7, 0, 0, 0, -1.125f, -0.5f, 0, -1.125f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, -0.0625f, -0.5f, -0.25f, -0.0625f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[89].setRotationPoint(-75.25f, -10.75f, -9.5f);

		bodyModel[90] = new ModelRendererTurbo(this, 163, 172, textureX, textureY);
		bodyModel[90].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f, 0, -0.25f, 0, -0.5f, -0.25f, 0, -0.5f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[90].setRotationPoint(-75.25f, 0.0f, 2.5f);

		bodyModel[91] = new ModelRendererTurbo(this, 125, 172, textureX, textureY);
		bodyModel[91].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[91].setRotationPoint(-75.25f, -7.0f, 2.5f);

		bodyModel[92] = new ModelRendererTurbo(this, 106, 172, textureX, textureY);
		bodyModel[92].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -1.0625f, 0, 0, -1.0625f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[92].setRotationPoint(-75.25f, -9.0f, 2.5f);

		bodyModel[93] = new ModelRendererTurbo(this, 87, 172, textureX, textureY);
		bodyModel[93].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[93].setRotationPoint(-75.25f, -5.0f, 2.5f);

		bodyModel[94] = new ModelRendererTurbo(this, 68, 172, textureX, textureY);
		bodyModel[94].addShapeBox(0, 0, 0, 1, 3, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[94].setRotationPoint(-75.25f, -3.0f, 2.5f);

		bodyModel[95] = new ModelRendererTurbo(this, 276, 173, textureX, textureY);
		bodyModel[95].addShapeBox(0, 0, 0, 1, 2, 7, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -1.125f, 0, 0, -1.125f, 0, -0.25f, 0, -0.5f, -0.25f, 0, -0.5f, -0.25f, -0.0625f, 0, -0.25f, -0.0625f);
		bodyModel[95].setRotationPoint(-75.25f, -10.75f, 2.5f);

		bodyModel[96] = new ModelRendererTurbo(this, 297, 156, textureX, textureY);
		bodyModel[96].addShapeBox(0, 0, 0, 1, 1, 15, 0, 0, 0, -1.375f, -0.5f, 0, -1.375f, -0.5f, 0, 0.375f, 0, 0, 0.375f, 0, 0.1875f, 0, -0.5f, 0.1875f, 0, -0.5f, 0.1875f, 1.75f, 0, 0.1875f, 1.75f);
		bodyModel[96].setRotationPoint(-75.25f, -11.9375f, -8.375f);

		bodyModel[97] = new ModelRendererTurbo(this, 447, 23, textureX, textureY);
		bodyModel[97].addShapeBox(0, 0, 0, 1, 1, 6, 0, 0, 0, -1.375f, -0.5f, 0, -1.375f, -0.5f, 0, -0.125f, 0, 0, -0.125f, 0, 0.4375f, 3.375f, -0.5f, 0.4375f, 3.375f, -0.5f, 0.4375f, 4.625f, 0, 0.4375f, 4.625f);
		bodyModel[97].setRotationPoint(-75.25f, -13.375f, -3.625f);

		bodyModel[98] = new ModelRendererTurbo(this, 474, 189, textureX, textureY);
		bodyModel[98].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0, 0, -2.5f, -0.5f, 0, -2.5f, -0.5f, 0, -2.5f, 0, 0, -2.5f, 0, -0.625f, -0.25f, -0.5f, -0.625f, -0.25f, -0.5f, -0.625f, -0.25f, 0, -0.625f, -0.25f);
		bodyModel[98].setRotationPoint(-75.25f, -13.75f, -2.5f);

		bodyModel[99] = new ModelRendererTurbo(this, 144, 173, textureX, textureY);
		bodyModel[99].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, 0, 0.375f, -0.5f, 0, 0.375f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, 0.375f, -0.5f, -0.25f, 0.375f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[99].setRotationPoint(-75.25f, 1.75f, -9.5f);

		bodyModel[100] = new ModelRendererTurbo(this, 394, 172, textureX, textureY);
		bodyModel[100].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, 0, 0.375f, -0.5f, 0, 0.375f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, 0.375f, -0.5f, -0.25f, 0.375f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[100].setRotationPoint(-75.25f, 1.75f, 2.875f);

		bodyModel[101] = new ModelRendererTurbo(this, 355, 73, textureX, textureY);
		bodyModel[101].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.5f, 0.0625f, 0, -0.5f, 0.0625f, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.125f, 0.0625f, 0, -0.125f, 0.0625f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[101].setRotationPoint(-73.25f, -7.0f, -11.0f);

		bodyModel[102] = new ModelRendererTurbo(this, 448, 72, textureX, textureY);
		bodyModel[102].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.8125f, 0.0625f, 0, -0.8125f, 0.0625f, 0, 0.0625f, 0, 0, 0.0625f, 0, 0, -0.125f, 0.0625f, 0, -0.125f, 0.0625f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[102].setRotationPoint(-73.25f, -9.0f, -10.625f);

		bodyModel[103] = new ModelRendererTurbo(this, 174, 176, textureX, textureY);
		bodyModel[103].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[103].setRotationPoint(-73.25f, -5.0f, -11.0f);

		bodyModel[104] = new ModelRendererTurbo(this, 317, 190, textureX, textureY);
		bodyModel[104].addShapeBox(0, 0, 0, 5, 3, 1, 0, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[104].setRotationPoint(-73.25f, -3.0f, -11.0f);

		bodyModel[105] = new ModelRendererTurbo(this, 136, 176, textureX, textureY);
		bodyModel[105].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, -0.5f, -0.5f, -0.25f, -0.5f, -0.5f, -0.25f, -0.25f, 0, -0.25f, -0.25f);
		bodyModel[105].setRotationPoint(-73.25f, 0.0f, -10.75f);

		bodyModel[106] = new ModelRendererTurbo(this, 117, 176, textureX, textureY);
		bodyModel[106].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -1.1875f, -0.5f, 0, -1.1875f, -0.5f, 0, 0.4375f, 0, 0, 0.4375f, 0, -0.25f, -0.125f, -0.5f, -0.25f, -0.125f, -0.5f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[106].setRotationPoint(-73.25f, -10.75f, -9.9375f);

		bodyModel[107] = new ModelRendererTurbo(this, 331, 71, textureX, textureY);
		bodyModel[107].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.5f, 0.0625f, 0, -0.5f, 0.0625f, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.125f, 0.0625f, 0, -0.125f, 0.0625f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[107].setRotationPoint(-69.8125f, -7.0f, -11.0f);

		bodyModel[108] = new ModelRendererTurbo(this, 345, 70, textureX, textureY);
		bodyModel[108].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.8125f, 0.0625f, 0, -0.8125f, 0.0625f, 0, 0.0625f, 0, 0, 0.0625f, 0, 0, -0.125f, 0.0625f, 0, -0.125f, 0.0625f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[108].setRotationPoint(-69.8125f, -9.0f, -10.625f);

		bodyModel[109] = new ModelRendererTurbo(this, 424, 60, textureX, textureY);
		bodyModel[109].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.5625f);
		bodyModel[109].setRotationPoint(-72.5625f, -7.0f, -11.0f);

		bodyModel[110] = new ModelRendererTurbo(this, 414, 60, textureX, textureY);
		bodyModel[110].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.75f, -0.375f, 0, -0.75f, -0.375f, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.5625f);
		bodyModel[110].setRotationPoint(-72.5625f, -9.0f, -10.625f);

		bodyModel[111] = new ModelRendererTurbo(this, 400, 60, textureX, textureY);
		bodyModel[111].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.5625f);
		bodyModel[111].setRotationPoint(-70.4375f, -7.0f, -11.0f);

		bodyModel[112] = new ModelRendererTurbo(this, 371, 59, textureX, textureY);
		bodyModel[112].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.75f, -0.375f, 0, -0.75f, -0.375f, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.5625f);
		bodyModel[112].setRotationPoint(-70.4375f, -9.0f, -10.625f);

		bodyModel[113] = new ModelRendererTurbo(this, 81, 212, textureX, textureY);
		bodyModel[113].addShapeBox(0, 0, 0, 3, 1, 1, 0, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f);
		bodyModel[113].setRotationPoint(-72.5f, -5.5f, -11.25f);

		bodyModel[114] = new ModelRendererTurbo(this, 72, 212, textureX, textureY);
		bodyModel[114].addShapeBox(0, 0, 0, 3, 1, 1, 0, -0.3125f, -0.375f, -0.453125f, -0.3125f, -0.375f, -0.453125f, -0.3125f, -0.375f, -0.171875f, -0.3125f, -0.375f, -0.171875f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f);
		bodyModel[114].setRotationPoint(-72.5f, -9.625f, -10.1875f);

		bodyModel[115] = new ModelRendererTurbo(this, 357, 59, textureX, textureY);
		bodyModel[115].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.25f, 0.0625f, 0, -0.25f, 0.0625f, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[115].setRotationPoint(-73.25f, -7.0f, 10.0f);

		bodyModel[116] = new ModelRendererTurbo(this, 470, 58, textureX, textureY);
		bodyModel[116].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, 0.0625f, 0.0625f, 0, 0.0625f, 0.0625f, 0, -0.8125f, 0, 0, -0.8125f, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[116].setRotationPoint(-73.25f, -9.0f, 9.625f);

		bodyModel[117] = new ModelRendererTurbo(this, 98, 176, textureX, textureY);
		bodyModel[117].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[117].setRotationPoint(-73.25f, -5.0f, 10.0f);

		bodyModel[118] = new ModelRendererTurbo(this, 222, 190, textureX, textureY);
		bodyModel[118].addShapeBox(0, 0, 0, 5, 3, 1, 0, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[118].setRotationPoint(-73.25f, -3.0f, 10.0f);

		bodyModel[119] = new ModelRendererTurbo(this, 79, 176, textureX, textureY);
		bodyModel[119].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.125f, 0, 0, -0.125f, 0, -0.25f, -0.25f, -0.5f, -0.25f, -0.25f, -0.5f, -0.25f, -0.5f, 0, -0.25f, -0.5f);
		bodyModel[119].setRotationPoint(-73.25f, 0.0f, 9.75f);

		bodyModel[120] = new ModelRendererTurbo(this, 30, 176, textureX, textureY);
		bodyModel[120].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, 0.4375f, -0.5f, 0, 0.4375f, -0.5f, 0, -1.1875f, 0, 0, -1.1875f, 0, -0.25f, -0.625f, -0.5f, -0.25f, -0.625f, -0.5f, -0.25f, -0.125f, 0, -0.25f, -0.125f);
		bodyModel[120].setRotationPoint(-73.25f, -10.75f, 8.9375f);

		bodyModel[121] = new ModelRendererTurbo(this, 456, 58, textureX, textureY);
		bodyModel[121].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.25f, 0.0625f, 0, -0.25f, 0.0625f, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[121].setRotationPoint(-69.8125f, -7.0f, 10.0f);

		bodyModel[122] = new ModelRendererTurbo(this, 381, 56, textureX, textureY);
		bodyModel[122].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, 0.0625f, 0.0625f, 0, 0.0625f, 0.0625f, 0, -0.8125f, 0, 0, -0.8125f, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[122].setRotationPoint(-69.8125f, -9.0f, 9.625f);

		bodyModel[123] = new ModelRendererTurbo(this, 0, 56, textureX, textureY);
		bodyModel[123].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.0625f);
		bodyModel[123].setRotationPoint(-72.5625f, -7.0f, 10.0f);

		bodyModel[124] = new ModelRendererTurbo(this, 424, 53, textureX, textureY);
		bodyModel[124].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.75f, -0.375f, 0, -0.75f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.0625f);
		bodyModel[124].setRotationPoint(-72.5625f, -9.0f, 9.625f);

		bodyModel[125] = new ModelRendererTurbo(this, 414, 53, textureX, textureY);
		bodyModel[125].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.0625f);
		bodyModel[125].setRotationPoint(-70.4375f, -7.0f, 10.0f);

		bodyModel[126] = new ModelRendererTurbo(this, 400, 52, textureX, textureY);
		bodyModel[126].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.75f, -0.375f, 0, -0.75f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.0625f);
		bodyModel[126].setRotationPoint(-70.4375f, -9.0f, 9.625f);

		bodyModel[127] = new ModelRendererTurbo(this, 36, 212, textureX, textureY);
		bodyModel[127].addShapeBox(0, 0, 0, 3, 1, 1, 0, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f);
		bodyModel[127].setRotationPoint(-72.5f, -5.5f, 10.25f);

		bodyModel[128] = new ModelRendererTurbo(this, 27, 212, textureX, textureY);
		bodyModel[128].addShapeBox(0, 0, 0, 3, 1, 1, 0, -0.3125f, -0.375f, -0.171875f, -0.3125f, -0.375f, -0.171875f, -0.3125f, -0.375f, -0.453125f, -0.3125f, -0.375f, -0.453125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f);
		bodyModel[128].setRotationPoint(-72.5f, -9.625f, 9.1875f);

		bodyModel[129] = new ModelRendererTurbo(this, 248, 221, textureX, textureY);
		bodyModel[129].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.25f, 0.0625f, 0, -0.25f, 0.0625f, 0, -0.75f, 0, 0, -0.75f);
		bodyModel[129].setRotationPoint(-72.125f, -7.0f, -11.0f);

		bodyModel[130] = new ModelRendererTurbo(this, 241, 221, textureX, textureY);
		bodyModel[130].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.9375f, 0.0625f, 0, -0.9375f, 0.0625f, 0, -0.0625f, 0, 0, -0.0625f, 0, 0, -0.25f, 0.0625f, 0, -0.25f, 0.0625f, 0, -0.75f, 0, 0, -0.75f);
		bodyModel[130].setRotationPoint(-72.125f, -9.0f, -10.625f);

		bodyModel[131] = new ModelRendererTurbo(this, 188, 221, textureX, textureY);
		bodyModel[131].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0.0625f, 0, -0.375f, 0.0625f, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.75f, 0.0625f, 0, -0.75f, 0.0625f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[131].setRotationPoint(-72.125f, -7.0f, 10.0f);

		bodyModel[132] = new ModelRendererTurbo(this, 181, 221, textureX, textureY);
		bodyModel[132].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.0625f, 0.0625f, 0, -0.0625f, 0.0625f, 0, -0.9375f, 0, 0, -0.9375f, 0, 0, -0.75f, 0.0625f, 0, -0.75f, 0.0625f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[132].setRotationPoint(-72.125f, -9.0f, 9.625f);

		bodyModel[133] = new ModelRendererTurbo(this, 32, 111, textureX, textureY);
		bodyModel[133].addShapeBox(0, 0, 0, 108, 3, 1, 0, 0, 0, -0.5f, 0.5f, 0, -0.5f, 0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0.5f, 0, -0.25f, 0.5f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[133].setRotationPoint(-68.75f, -3.0f, 10.0f);

		bodyModel[134] = new ModelRendererTurbo(this, 32, 107, textureX, textureY);
		bodyModel[134].addShapeBox(0, 0, 0, 108, 2, 1, 0, 0, 0, -0.5f, 0.5f, 0, -0.5f, 0.5f, 0, 0, 0, 0, 0, 0, -0.25f, -0.125f, 0.5f, -0.25f, -0.125f, 0.5f, -0.25f, -0.375f, 0, -0.25f, -0.375f);
		bodyModel[134].setRotationPoint(-68.75f, 0.0f, 9.75f);

		bodyModel[135] = new ModelRendererTurbo(this, 174, 221, textureX, textureY);
		bodyModel[135].addShapeBox(0, 0, 0, 2, 12, 1, 0, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f);
		bodyModel[135].setRotationPoint(-77.125f, -10.0f, -3.375f);

		bodyModel[136] = new ModelRendererTurbo(this, 133, 221, textureX, textureY);
		bodyModel[136].addShapeBox(0, 0, 0, 2, 12, 1, 0, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f);
		bodyModel[136].setRotationPoint(-77.125f, -10.0f, 2.375f);

		bodyModel[137] = new ModelRendererTurbo(this, 130, 189, textureX, textureY);
		bodyModel[137].addShapeBox(0, 0, 0, 2, 1, 4, 0, 0, -0.3125f, -0.3125f, -0.125f, -0.3125f, -0.3125f, -0.125f, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f, -0.125f, -0.3125f, -0.3125f, -0.125f, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f);
		bodyModel[137].setRotationPoint(-77.125f, -11.6875f, -2.0f);

		bodyModel[138] = new ModelRendererTurbo(this, 126, 221, textureX, textureY);
		bodyModel[138].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.5625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.3125f, 0, 0, -0.3125f, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f);
		bodyModel[138].setRotationPoint(-76.875f, -7.0f, -11.0f);

		bodyModel[139] = new ModelRendererTurbo(this, 69, 221, textureX, textureY);
		bodyModel[139].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.875f, -0.375f, 0, -0.875f, -0.375f, 0, 0, 0, 0, 0, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f);
		bodyModel[139].setRotationPoint(-76.875f, -9.0f, -10.625f);

		bodyModel[140] = new ModelRendererTurbo(this, 62, 221, textureX, textureY);
		bodyModel[140].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f);
		bodyModel[140].setRotationPoint(-76.875f, -5.0f, -11.0f);

		bodyModel[141] = new ModelRendererTurbo(this, 55, 221, textureX, textureY);
		bodyModel[141].addShapeBox(0, 0, 0, 2, 3, 1, 0, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, 0, 0, -0.4375f);
		bodyModel[141].setRotationPoint(-76.875f, -3.0f, -11.0f);

		bodyModel[142] = new ModelRendererTurbo(this, 48, 221, textureX, textureY);
		bodyModel[142].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f, 0, -0.25f, -0.5625f, -0.375f, -0.25f, -0.5625f, -0.375f, -0.25f, -0.3125f, 0, -0.25f, -0.3125f);
		bodyModel[142].setRotationPoint(-76.875f, 0.0f, -10.75f);

		bodyModel[143] = new ModelRendererTurbo(this, 14, 221, textureX, textureY);
		bodyModel[143].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -1.25f, -0.375f, 0, -1.25f, -0.375f, 0, 0.375f, 0, 0, 0.375f, 0, -0.25f, -0.1875f, -0.375f, -0.25f, -0.1875f, -0.375f, -0.25f, -0.6875f, 0, -0.25f, -0.6875f);
		bodyModel[143].setRotationPoint(-76.875f, -10.75f, -9.9375f);

		bodyModel[144] = new ModelRendererTurbo(this, 489, 220, textureX, textureY);
		bodyModel[144].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.3125f, -0.375f, 0, -0.3125f, -0.375f, 0, -0.5625f, 0, 0, -0.5625f, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f);
		bodyModel[144].setRotationPoint(-76.875f, -7.0f, 10.0f);

		bodyModel[145] = new ModelRendererTurbo(this, 482, 220, textureX, textureY);
		bodyModel[145].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f);
		bodyModel[145].setRotationPoint(-76.875f, -9.0f, 9.625f);

		bodyModel[146] = new ModelRendererTurbo(this, 446, 220, textureX, textureY);
		bodyModel[146].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f);
		bodyModel[146].setRotationPoint(-76.875f, -5.0f, 10.0f);

		bodyModel[147] = new ModelRendererTurbo(this, 422, 220, textureX, textureY);
		bodyModel[147].addShapeBox(0, 0, 0, 2, 3, 1, 0, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f, 0, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, 0, 0, -0.4375f);
		bodyModel[147].setRotationPoint(-76.875f, -3.0f, 10.0f);

		bodyModel[148] = new ModelRendererTurbo(this, 415, 220, textureX, textureY);
		bodyModel[148].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f, 0, -0.25f, -0.3125f, -0.375f, -0.25f, -0.3125f, -0.375f, -0.25f, -0.5625f, 0, -0.25f, -0.5625f);
		bodyModel[148].setRotationPoint(-76.875f, 0.0f, 9.75f);

		bodyModel[149] = new ModelRendererTurbo(this, 408, 220, textureX, textureY);
		bodyModel[149].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.375f, -0.375f, 0, 0.375f, -0.375f, 0, -1.25f, 0, 0, -1.25f, 0, -0.25f, -0.6875f, -0.375f, -0.25f, -0.6875f, -0.375f, -0.25f, -0.1875f, 0, -0.25f, -0.1875f);
		bodyModel[149].setRotationPoint(-76.875f, -10.75f, 8.9375f);

		bodyModel[150] = new ModelRendererTurbo(this, 304, 188, textureX, textureY);
		bodyModel[150].addShapeBox(0, 0, 0, 2, 3, 4, 0, 0, -0.25f, 0.125f, -0.5f, -0.25f, 0.125f, -0.5f, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0.125f, -0.5f, -0.25f, 0.125f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[150].setRotationPoint(-76.75f, 2.5f, -1.875f);

		bodyModel[151] = new ModelRendererTurbo(this, 169, 198, textureX, textureY);
		bodyModel[151].addShapeBox(0, 0, 0, 1, 1, 4, 0, 0, -0.125f, -0.1875f, -0.75f, -0.125f, -0.1875f, -0.75f, -0.125f, 0, 0, -0.125f, 0, 0, 0, -0.1875f, -0.75f, 0, -0.1875f, -0.75f, 0, 0, 0, 0, 0);
		bodyModel[151].setRotationPoint(-77.0f, 2.4375f, -2.0625f);

		bodyModel[152] = new ModelRendererTurbo(this, 222, 197, textureX, textureY);
		bodyModel[152].addShapeBox(0, 0, 0, 1, 2, 4, 0, 0, -0.125f, 0.125f, -0.75f, -0.125f, 0.125f, -0.75f, -0.125f, 0, 0, -0.125f, 0, 0, -0.0625f, 0.125f, -0.75f, -0.0625f, 0.125f, -0.75f, -0.0625f, 0, 0, -0.0625f, 0);
		bodyModel[152].setRotationPoint(-77.0f, 3.3125f, -1.875f);

		bodyModel[153] = new ModelRendererTurbo(this, 68, 188, textureX, textureY);
		bodyModel[153].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, -0.625f, 0, -0.5f, -0.625f, 0, -0.5f, -0.625f, 0, 0, -0.625f, 0);
		bodyModel[153].setRotationPoint(-75.25f, -10.75f, -2.5f);

		bodyModel[154] = new ModelRendererTurbo(this, 47, 188, textureX, textureY);
		bodyModel[154].addShapeBox(0, 0, 0, 1, 13, 5, 0, -0.125f, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.125f, 0, 0, -0.125f, -0.125f, 0, -0.625f, -0.125f, 0, -0.625f, -0.125f, 0, -0.125f, -0.125f, 0);
		bodyModel[154].setRotationPoint(-75.25f, -10.375f, -2.5f);

		bodyModel[155] = new ModelRendererTurbo(this, 0, 98, textureX, textureY);
		bodyModel[155].addShapeBox(0, 0, 0, 131, 1, 1, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0.5f, -0.5f, 0, 0.5f, -0.5f, -0.5f, 0, -0.5f, -0.5f);
		bodyModel[155].setRotationPoint(-74.75f, -13.0f, -4.5f);

		bodyModel[156] = new ModelRendererTurbo(this, 0, 95, textureX, textureY);
		bodyModel[156].addShapeBox(0, 0, 0, 131, 1, 1, 0, 0, 0, -0.5f, 0.5f, 0, -0.5f, 0.5f, 0, 0, 0, 0, 0, 0, -0.5f, -0.5f, 0.5f, -0.5f, -0.5f, 0.5f, -0.5f, 0, 0, -0.5f, 0);
		bodyModel[156].setRotationPoint(-74.75f, -13.0f, 3.5f);

		bodyModel[157] = new ModelRendererTurbo(this, 38, 172, textureX, textureY);
		bodyModel[157].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, -0.625f, -0.5f, -0.25f, -0.625f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[157].setRotationPoint(-65.25f, 0.0f, -10.5f);

		bodyModel[158] = new ModelRendererTurbo(this, 19, 172, textureX, textureY);
		bodyModel[158].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[158].setRotationPoint(-65.25f, -7.0f, -10.5f);

		bodyModel[159] = new ModelRendererTurbo(this, 0, 172, textureX, textureY);
		bodyModel[159].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, -1.0625f, -0.5f, 0, -1.0625f, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[159].setRotationPoint(-65.25f, -9.0f, -10.5f);

		bodyModel[160] = new ModelRendererTurbo(this, 445, 171, textureX, textureY);
		bodyModel[160].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[160].setRotationPoint(-65.25f, -5.0f, -10.5f);

		bodyModel[161] = new ModelRendererTurbo(this, 488, 170, textureX, textureY);
		bodyModel[161].addShapeBox(0, 0, 0, 1, 3, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[161].setRotationPoint(-65.25f, -3.0f, -10.5f);

		bodyModel[162] = new ModelRendererTurbo(this, 377, 172, textureX, textureY);
		bodyModel[162].addShapeBox(0, 0, 0, 1, 2, 7, 0, 0, 0, -1.125f, -0.5f, 0, -1.125f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, -0.0625f, -0.5f, -0.25f, -0.0625f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[162].setRotationPoint(-65.25f, -10.75f, -9.5f);

		bodyModel[163] = new ModelRendererTurbo(this, 426, 170, textureX, textureY);
		bodyModel[163].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f, 0, -0.25f, 0, -0.5f, -0.25f, 0, -0.5f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[163].setRotationPoint(-65.25f, 0.0f, 2.5f);

		bodyModel[164] = new ModelRendererTurbo(this, 415, 167, textureX, textureY);
		bodyModel[164].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[164].setRotationPoint(-65.25f, -7.0f, 2.5f);

		bodyModel[165] = new ModelRendererTurbo(this, 239, 164, textureX, textureY);
		bodyModel[165].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -1.0625f, 0, 0, -1.0625f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[165].setRotationPoint(-65.25f, -9.0f, 2.5f);

		bodyModel[166] = new ModelRendererTurbo(this, 477, 162, textureX, textureY);
		bodyModel[166].addShapeBox(0, 0, 0, 1, 2, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[166].setRotationPoint(-65.25f, -5.0f, 2.5f);

		bodyModel[167] = new ModelRendererTurbo(this, 458, 161, textureX, textureY);
		bodyModel[167].addShapeBox(0, 0, 0, 1, 3, 8, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[167].setRotationPoint(-65.25f, -3.0f, 2.5f);

		bodyModel[168] = new ModelRendererTurbo(this, 360, 172, textureX, textureY);
		bodyModel[168].addShapeBox(0, 0, 0, 1, 2, 7, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -1.125f, 0, 0, -1.125f, 0, -0.25f, 0, -0.5f, -0.25f, 0, -0.5f, -0.25f, -0.0625f, 0, -0.25f, -0.0625f);
		bodyModel[168].setRotationPoint(-65.25f, -10.75f, 2.5f);

		bodyModel[169] = new ModelRendererTurbo(this, 264, 156, textureX, textureY);
		bodyModel[169].addShapeBox(0, 0, 0, 1, 1, 15, 0, 0, 0, -1.375f, -0.5f, 0, -1.375f, -0.5f, 0, 0.375f, 0, 0, 0.375f, 0, 0.1875f, 0, -0.5f, 0.1875f, 0, -0.5f, 0.1875f, 1.75f, 0, 0.1875f, 1.75f);
		bodyModel[169].setRotationPoint(-65.25f, -11.9375f, -8.375f);

		bodyModel[170] = new ModelRendererTurbo(this, 392, 23, textureX, textureY);
		bodyModel[170].addShapeBox(0, 0, 0, 1, 1, 6, 0, 0, 0, -1.375f, -0.5f, 0, -1.375f, -0.5f, 0, -0.125f, 0, 0, -0.125f, 0, 0.4375f, 3.375f, -0.5f, 0.4375f, 3.375f, -0.5f, 0.4375f, 4.625f, 0, 0.4375f, 4.625f);
		bodyModel[170].setRotationPoint(-65.25f, -13.375f, -3.625f);

		bodyModel[171] = new ModelRendererTurbo(this, 376, 187, textureX, textureY);
		bodyModel[171].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0, 0, -2.5f, -0.5f, 0, -2.5f, -0.5f, 0, -2.5f, 0, 0, -2.5f, 0, -0.625f, -0.25f, -0.5f, -0.625f, -0.25f, -0.5f, -0.625f, -0.25f, 0, -0.625f, -0.25f);
		bodyModel[171].setRotationPoint(-65.25f, -13.75f, -2.5f);

		bodyModel[172] = new ModelRendererTurbo(this, 327, 156, textureX, textureY);
		bodyModel[172].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, 0, 0.375f, -0.5f, 0, 0.375f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, 0.375f, -0.5f, -0.25f, 0.375f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[172].setRotationPoint(-65.25f, 1.75f, -9.5f);

		bodyModel[173] = new ModelRendererTurbo(this, 259, 145, textureX, textureY);
		bodyModel[173].addShapeBox(0, 0, 0, 1, 1, 7, 0, 0, 0, 0.375f, -0.5f, 0, 0.375f, -0.5f, 0, 0, 0, 0, 0, 0, -0.25f, 0.375f, -0.5f, -0.25f, 0.375f, -0.5f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[173].setRotationPoint(-65.25f, 1.75f, 2.875f);

		bodyModel[174] = new ModelRendererTurbo(this, 347, 187, textureX, textureY);
		bodyModel[174].addShapeBox(0, 0, 0, 1, 1, 5, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, -0.625f, 0, -0.5f, -0.625f, 0, -0.5f, -0.625f, 0, 0, -0.625f, 0);
		bodyModel[174].setRotationPoint(-65.25f, -10.75f, -2.5f);

		bodyModel[175] = new ModelRendererTurbo(this, 397, 186, textureX, textureY);
		bodyModel[175].addShapeBox(0, 0, 0, 1, 13, 5, 0, -0.125f, 0, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.125f, 0, 0, -0.125f, -0.125f, 0, -0.625f, -0.125f, 0, -0.625f, -0.125f, 0, -0.125f, -0.125f, 0);
		bodyModel[175].setRotationPoint(-65.25f, -10.375f, -2.5f);

		bodyModel[176] = new ModelRendererTurbo(this, 291, 189, textureX, textureY);
		bodyModel[176].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[176].setRotationPoint(22.75f, 4.0f, -6.01f);

		bodyModel[177] = new ModelRendererTurbo(this, 261, 188, textureX, textureY);
		bodyModel[177].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[177].setRotationPoint(22.75f, 4.0f, 6.01f);

		bodyModel[178] = new ModelRendererTurbo(this, 418, 0, textureX, textureY);
		bodyModel[178].addShapeBox(0, 0, 0, 21, 2, 12, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, 0, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, 0, 0);
		bodyModel[178].setRotationPoint(23.75f, 5.5f, -6.0f);

		bodyModel[179] = new ModelRendererTurbo(this, 466, 187, textureX, textureY);
		bodyModel[179].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[179].setRotationPoint(39.375f, 4.0f, -6.01f);

		bodyModel[180] = new ModelRendererTurbo(this, 248, 187, textureX, textureY);
		bodyModel[180].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[180].setRotationPoint(39.375f, 4.0f, 6.01f);

		bodyModel[181] = new ModelRendererTurbo(this, 235, 187, textureX, textureY);
		bodyModel[181].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[181].setRotationPoint(-63.5f, 4.0f, -6.01f);

		bodyModel[182] = new ModelRendererTurbo(this, 178, 184, textureX, textureY);
		bodyModel[182].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[182].setRotationPoint(-63.5f, 4.0f, 6.01f);

		bodyModel[183] = new ModelRendererTurbo(this, 351, 0, textureX, textureY);
		bodyModel[183].addShapeBox(0, 0, 0, 21, 2, 12, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, 0, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, 0, 0);
		bodyModel[183].setRotationPoint(-62.5f, 5.5f, -6.0f);

		bodyModel[184] = new ModelRendererTurbo(this, 339, 183, textureX, textureY);
		bodyModel[184].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[184].setRotationPoint(-46.875f, 4.0f, -6.01f);

		bodyModel[185] = new ModelRendererTurbo(this, 326, 183, textureX, textureY);
		bodyModel[185].addShapeBox(0, 0, 0, 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[185].setRotationPoint(-46.875f, 4.0f, 6.01f);

		bodyModel[186] = new ModelRendererTurbo(this, 331, 70, textureX, textureY);
		bodyModel[186].addShapeBox(0, 0, 0, 2, 1, 19, 0, 0, 0, 0.125f, -0.0625f, 0, 0.125f, -0.0625f, 0, -0.375f, 0, 0, -0.375f, 0, -0.25f, -0.35500002f, -0.0625f, -1, 0.125f, -0.0625f, -1, -0.375f, 0, -0.25f, -0.855f);
		bodyModel[186].setRotationPoint(21.625f, 5.5f, -9.25f);

		bodyModel[187] = new ModelRendererTurbo(this, 456, 53, textureX, textureY);
		bodyModel[187].addShapeBox(0, 0, 0, 2, 1, 19, 0, 0, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.375f, 0, 0, -0.375f, 0, -0.25f, -0.35500002f, -0.375f, -0.25f, -0.35500002f, -0.375f, -0.25f, -0.855f, 0, -0.25f, -0.855f);
		bodyModel[187].setRotationPoint(20.0f, 5.5f, -9.25f);

		bodyModel[188] = new ModelRendererTurbo(this, 400, 53, textureX, textureY);
		bodyModel[188].addShapeBox(0, 0, 0, 2, 1, 19, 0, 0, 0, -0.35500002f, -0.375f, 0, -0.35500002f, -0.375f, 0, -0.855f, 0, 0, -0.855f, 0, -0.1875f, -0.875f, -1, -0.1875f, -0.875f, -1, -0.1875f, -1.375f, 0, -0.1875f, -1.375f);
		bodyModel[188].setRotationPoint(20.0f, 6.25f, -9.25f);

		bodyModel[189] = new ModelRendererTurbo(this, 381, 52, textureX, textureY);
		bodyModel[189].addShapeBox(0, 0, 0, 1, 1, 16, 0, 0, 0, 0.625f, 0, 0, 0.625f, 0, 0, 0.125f, 0, 0, 0.125f, 0, -0.25f, -0.1875f, -0.3125f, -0.25f, -0.1875f, -0.3125f, -0.25f, -0.6875f, 0, -0.25f, -0.6875f);
		bodyModel[189].setRotationPoint(20.0f, 7.0625f, -7.75f);

		bodyModel[190] = new ModelRendererTurbo(this, 330, 156, textureX, textureY);
		bodyModel[190].addShapeBox(0, 0, 0, 1, 1, 14, 0, 0, 0, 0.8125f, -0.3125f, 0, 0.8125f, -0.3125f, 0, 0.3125f, 0, 0, 0.3125f, 0, -0.25f, -0.0625f, -0.625f, -0.25f, -0.0625f, -0.625f, -0.25f, -0.5625f, 0, -0.25f, -0.5625f);
		bodyModel[190].setRotationPoint(20.0f, 7.8125f, -6.75f);

		bodyModel[191] = new ModelRendererTurbo(this, 355, 15, textureX, textureY);
		bodyModel[191].addShapeBox(0, 0, 0, 9, 2, 18, 0, 0, 0, 0.625f, 0, 0, 0.625f, 0, 0, 0.125f, 0, 0, 0.125f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.875f, 0, -0.4375f, -0.875f);
		bodyModel[191].setRotationPoint(48.125f, 5.5f, -8.75f);

		bodyModel[192] = new ModelRendererTurbo(this, 357, 52, textureX, textureY);
		bodyModel[192].addShapeBox(0, 0, 0, 2, 1, 19, 0, -0.0625f, 0, 0.125f, 0, 0, 0.125f, 0, 0, -0.375f, -0.0625f, 0, -0.375f, -0.0625f, -1, 0.125f, 0, -0.25f, -0.35500002f, 0, -0.25f, -0.855f, -0.0625f, -1, -0.375f);
		bodyModel[192].setRotationPoint(44.5f, 5.5f, -9.25f);

		bodyModel[193] = new ModelRendererTurbo(this, 447, 15, textureX, textureY);
		bodyModel[193].addShapeBox(0, 0, 0, 9, 1, 16, 0, 0, 0, 0.625f, 0, 0, 0.625f, 0, 0, 0.125f, 0, 0, 0.125f, 0, -0.25f, -0.1875f, 0, -0.25f, -0.1875f, 0, -0.25f, -0.6875f, 0, -0.25f, -0.6875f);
		bodyModel[193].setRotationPoint(48.125f, 7.0625f, -7.75f);

		bodyModel[194] = new ModelRendererTurbo(this, 357, 36, textureX, textureY);
		bodyModel[194].addShapeBox(0, 0, 0, 9, 1, 14, 0, 0, 0, 0.8125f, 0, 0, 0.8125f, 0, 0, 0.3125f, 0, 0, 0.3125f, 0, -0.25f, -0.0625f, 0, -0.25f, -0.0625f, 0, -0.25f, -0.5625f, 0, -0.25f, -0.5625f);
		bodyModel[194].setRotationPoint(48.125f, 7.8125f, -6.75f);

		bodyModel[195] = new ModelRendererTurbo(this, 432, 51, textureX, textureY);
		bodyModel[195].addShapeBox(0, 0, 0, 2, 1, 19, 0, -0.375f, 0, 0.125f, 0, 0, 0.125f, 0, 0, -0.375f, -0.375f, 0, -0.375f, -0.375f, -0.25f, -0.35500002f, 0, -0.25f, -0.35500002f, 0, -0.25f, -0.855f, -0.375f, -0.25f, -0.855f);
		bodyModel[195].setRotationPoint(46.125f, 5.5f, -9.25f);

		bodyModel[196] = new ModelRendererTurbo(this, 333, 49, textureX, textureY);
		bodyModel[196].addShapeBox(0, 0, 0, 2, 1, 19, 0, -0.375f, 0, -0.35500002f, 0, 0, -0.35500002f, 0, 0, -0.855f, -0.375f, 0, -0.855f, -1, -0.1875f, -0.875f, 0, -0.1875f, -0.875f, 0, -0.1875f, -1.375f, -1, -0.1875f, -1.375f);
		bodyModel[196].setRotationPoint(46.125f, 6.25f, -9.25f);

		bodyModel[197] = new ModelRendererTurbo(this, 392, 15, textureX, textureY);
		bodyModel[197].addShapeBox(0, 0, 0, 1, 1, 16, 0, 0, 0, 0.625f, 0, 0, 0.625f, 0, 0, 0.125f, 0, 0, 0.125f, -0.3125f, -0.25f, -0.1875f, 0, -0.25f, -0.1875f, 0, -0.25f, -0.6875f, -0.3125f, -0.25f, -0.6875f);
		bodyModel[197].setRotationPoint(47.125f, 7.0625f, -7.75f);

		bodyModel[198] = new ModelRendererTurbo(this, 480, 51, textureX, textureY);
		bodyModel[198].addShapeBox(0, 0, 0, 1, 1, 14, 0, -0.3125f, 0, 0.8125f, 0, 0, 0.8125f, 0, 0, 0.3125f, -0.3125f, 0, 0.3125f, -0.625f, -0.25f, -0.0625f, 0, -0.25f, -0.0625f, 0, -0.25f, -0.5625f, -0.625f, -0.25f, -0.5625f);
		bodyModel[198].setRotationPoint(47.125f, 7.8125f, -6.75f);

		bodyModel[199] = new ModelRendererTurbo(this, 228, 161, textureX, textureY);
		bodyModel[199].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, -0.5f, 0, 0, -0.5f, -0.25f, -0.625f, 0, -0.25f, -0.625f, 0, -0.25f, 0, -0.5f, -0.25f, 0);
		bodyModel[199].setRotationPoint(56.125f, 0.0f, -10.5f);

		bodyModel[200] = new ModelRendererTurbo(this, 209, 161, textureX, textureY);
		bodyModel[200].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[200].setRotationPoint(56.125f, -7.0f, -10.5f);

		bodyModel[201] = new ModelRendererTurbo(this, 190, 161, textureX, textureY);
		bodyModel[201].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, -1.0625f, 0, 0, -1.0625f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[201].setRotationPoint(56.125f, -9.0f, -10.5f);

		bodyModel[202] = new ModelRendererTurbo(this, 171, 161, textureX, textureY);
		bodyModel[202].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[202].setRotationPoint(56.125f, -5.0f, -10.5f);

		bodyModel[203] = new ModelRendererTurbo(this, 152, 161, textureX, textureY);
		bodyModel[203].addShapeBox(0, 0, 0, 1, 3, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[203].setRotationPoint(56.125f, -3.0f, -10.5f);

		bodyModel[204] = new ModelRendererTurbo(this, 495, 142, textureX, textureY);
		bodyModel[204].addShapeBox(0, 0, 0, 1, 2, 7, 0, -0.5f, 0, -1.125f, 0, 0, -1.125f, 0, 0, 0, -0.5f, 0, 0, -0.5f, -0.25f, -0.0625f, 0, -0.25f, -0.0625f, 0, -0.25f, 0, -0.5f, -0.25f, 0);
		bodyModel[204].setRotationPoint(56.125f, -10.75f, -9.5f);

		bodyModel[205] = new ModelRendererTurbo(this, 133, 161, textureX, textureY);
		bodyModel[205].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, -0.625f, -0.5f, -0.25f, -0.625f);
		bodyModel[205].setRotationPoint(56.125f, 0.0f, 2.5f);

		bodyModel[206] = new ModelRendererTurbo(this, 114, 161, textureX, textureY);
		bodyModel[206].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[206].setRotationPoint(56.125f, -7.0f, 2.5f);

		bodyModel[207] = new ModelRendererTurbo(this, 95, 161, textureX, textureY);
		bodyModel[207].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -1.0625f, -0.5f, 0, -1.0625f, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f);
		bodyModel[207].setRotationPoint(56.125f, -9.0f, 2.5f);

		bodyModel[208] = new ModelRendererTurbo(this, 76, 161, textureX, textureY);
		bodyModel[208].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[208].setRotationPoint(56.125f, -5.0f, 2.5f);

		bodyModel[209] = new ModelRendererTurbo(this, 57, 161, textureX, textureY);
		bodyModel[209].addShapeBox(0, 0, 0, 1, 3, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f);
		bodyModel[209].setRotationPoint(56.125f, -3.0f, 2.5f);

		bodyModel[210] = new ModelRendererTurbo(this, 495, 126, textureX, textureY);
		bodyModel[210].addShapeBox(0, 0, 0, 1, 2, 7, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -1.125f, -0.5f, 0, -1.125f, -0.5f, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, -0.0625f, -0.5f, -0.25f, -0.0625f);
		bodyModel[210].setRotationPoint(56.125f, -10.75f, 2.5f);

		bodyModel[211] = new ModelRendererTurbo(this, 477, 142, textureX, textureY);
		bodyModel[211].addShapeBox(0, 0, 0, 1, 1, 15, 0, -0.5f, 0, -1.375f, 0, 0, -1.375f, 0, 0, 0.375f, -0.5f, 0, 0.375f, -0.5f, 0.1875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 1.75f, -0.5f, 0.1875f, 1.75f);
		bodyModel[211].setRotationPoint(56.125f, -11.9375f, -8.375f);

		bodyModel[212] = new ModelRendererTurbo(this, 447, 15, textureX, textureY);
		bodyModel[212].addShapeBox(0, 0, 0, 1, 1, 6, 0, -0.5f, 0, -1.375f, 0, 0, -1.375f, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0.4375f, 3.375f, 0, 0.4375f, 3.375f, 0, 0.4375f, 4.625f, -0.5f, 0.4375f, 4.625f);
		bodyModel[212].setRotationPoint(56.125f, -13.375f, -3.625f);

		bodyModel[213] = new ModelRendererTurbo(this, 313, 183, textureX, textureY);
		bodyModel[213].addShapeBox(0, 0, 0, 1, 1, 5, 0, -0.5f, 0, -2.5f, 0, 0, -2.5f, 0, 0, -2.5f, -0.5f, 0, -2.5f, -0.5f, -0.625f, -0.25f, 0, -0.625f, -0.25f, 0, -0.625f, -0.25f, -0.5f, -0.625f, -0.25f);
		bodyModel[213].setRotationPoint(56.125f, -13.75f, -2.5f);

		bodyModel[214] = new ModelRendererTurbo(this, 276, 126, textureX, textureY);
		bodyModel[214].addShapeBox(0, 0, 0, 1, 1, 7, 0, -0.5f, 0, 0.375f, 0, 0, 0.375f, 0, 0, 0, -0.5f, 0, 0, -0.5f, -0.25f, 0.375f, 0, -0.25f, 0.375f, 0, -0.25f, 0, -0.5f, -0.25f, 0);
		bodyModel[214].setRotationPoint(56.125f, 1.75f, -9.5f);

		bodyModel[215] = new ModelRendererTurbo(this, 283, 110, textureX, textureY);
		bodyModel[215].addShapeBox(0, 0, 0, 1, 1, 7, 0, -0.5f, 0, 0.375f, 0, 0, 0.375f, 0, 0, 0, -0.5f, 0, 0, -0.5f, -0.25f, 0.375f, 0, -0.25f, 0.375f, 0, -0.25f, 0, -0.5f, -0.25f, 0);
		bodyModel[215].setRotationPoint(56.125f, 1.75f, 2.875f);

		bodyModel[216] = new ModelRendererTurbo(this, 401, 220, textureX, textureY);
		bodyModel[216].addShapeBox(0, 0, 0, 2, 12, 1, 0, -0.125f, 0, -0.3125f, 0, 0, -0.3125f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f);
		bodyModel[216].setRotationPoint(57.0f, -10.0f, -3.375f);

		bodyModel[217] = new ModelRendererTurbo(this, 379, 220, textureX, textureY);
		bodyModel[217].addShapeBox(0, 0, 0, 2, 12, 1, 0, -0.125f, 0, -0.3125f, 0, 0, -0.3125f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f);
		bodyModel[217].setRotationPoint(57.0f, -10.0f, 2.375f);

		bodyModel[218] = new ModelRendererTurbo(this, 291, 183, textureX, textureY);
		bodyModel[218].addShapeBox(0, 0, 0, 2, 1, 4, 0, -0.125f, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f, -0.125f, -0.3125f, -0.3125f, -0.125f, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f, 0, -0.3125f, -0.3125f, -0.125f, -0.3125f, -0.3125f);
		bodyModel[218].setRotationPoint(57.0f, -11.6875f, -2.0f);

		bodyModel[219] = new ModelRendererTurbo(this, 278, 183, textureX, textureY);
		bodyModel[219].addShapeBox(0, 0, 0, 2, 3, 4, 0, -0.5f, -0.25f, 0.125f, 0, -0.25f, 0.125f, 0, -0.25f, 0, -0.5f, -0.25f, 0, -0.5f, -0.25f, 0.125f, 0, -0.25f, 0.125f, 0, -0.25f, 0, -0.5f, -0.25f, 0);
		bodyModel[219].setRotationPoint(56.625f, 2.5f, -1.875f);

		bodyModel[220] = new ModelRendererTurbo(this, 67, 196, textureX, textureY);
		bodyModel[220].addShapeBox(0, 0, 0, 1, 1, 4, 0, -0.75f, -0.125f, -0.1875f, 0, -0.125f, -0.1875f, 0, -0.125f, 0, -0.75f, -0.125f, 0, -0.75f, 0, -0.1875f, 0, 0, -0.1875f, 0, 0, 0, -0.75f, 0, 0);
		bodyModel[220].setRotationPoint(57.875f, 2.4375f, -2.0625f);

		bodyModel[221] = new ModelRendererTurbo(this, 133, 195, textureX, textureY);
		bodyModel[221].addShapeBox(0, 0, 0, 1, 2, 4, 0, -0.75f, -0.125f, 0.125f, 0, -0.125f, 0.125f, 0, -0.125f, 0, -0.75f, -0.125f, 0, -0.75f, -0.0625f, 0.125f, 0, -0.0625f, 0.125f, 0, -0.0625f, 0, -0.75f, -0.0625f, 0);
		bodyModel[221].setRotationPoint(57.875f, 3.3125f, -1.875f);

		bodyModel[222] = new ModelRendererTurbo(this, 222, 183, textureX, textureY);
		bodyModel[222].addShapeBox(0, 0, 0, 1, 1, 5, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, -0.625f, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.5f, -0.625f, 0);
		bodyModel[222].setRotationPoint(56.125f, -10.75f, -2.5f);

		bodyModel[223] = new ModelRendererTurbo(this, 209, 183, textureX, textureY);
		bodyModel[223].addShapeBox(0, 0, 0, 1, 13, 5, 0, -0.625f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.625f, 0, 0, -0.625f, -0.125f, 0, -0.125f, -0.125f, 0, -0.125f, -0.125f, 0, -0.625f, -0.125f, 0);
		bodyModel[223].setRotationPoint(56.125f, -10.375f, -2.5f);

		bodyModel[224] = new ModelRendererTurbo(this, 325, 197, textureX, textureY);
		bodyModel[224].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[224].setRotationPoint(-60.75f, -9.0f, -10.625f);

		bodyModel[225] = new ModelRendererTurbo(this, 301, 196, textureX, textureY);
		bodyModel[225].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[225].setRotationPoint(-60.75f, -9.0f, 9.625f);

		bodyModel[226] = new ModelRendererTurbo(this, 351, 220, textureX, textureY);
		bodyModel[226].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[226].setRotationPoint(-49.75f, -5.0f, -11.0f);

		bodyModel[227] = new ModelRendererTurbo(this, 344, 220, textureX, textureY);
		bodyModel[227].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[227].setRotationPoint(-49.75f, -7.0f, -11.0f);

		bodyModel[228] = new ModelRendererTurbo(this, 324, 220, textureX, textureY);
		bodyModel[228].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[228].setRotationPoint(-49.75f, -9.0f, -10.625f);

		bodyModel[229] = new ModelRendererTurbo(this, 183, 191, textureX, textureY);
		bodyModel[229].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[229].setRotationPoint(-47.75f, -9.0f, -10.625f);

		bodyModel[230] = new ModelRendererTurbo(this, 279, 220, textureX, textureY);
		bodyModel[230].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[230].setRotationPoint(-49.75f, -5.0f, 10.0f);

		bodyModel[231] = new ModelRendererTurbo(this, 234, 220, textureX, textureY);
		bodyModel[231].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[231].setRotationPoint(-49.75f, -7.0f, 10.0f);

		bodyModel[232] = new ModelRendererTurbo(this, 227, 220, textureX, textureY);
		bodyModel[232].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[232].setRotationPoint(-49.75f, -9.0f, 9.625f);

		bodyModel[233] = new ModelRendererTurbo(this, 68, 184, textureX, textureY);
		bodyModel[233].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[233].setRotationPoint(-47.75f, -9.0f, 9.625f);

		bodyModel[234] = new ModelRendererTurbo(this, 220, 220, textureX, textureY);
		bodyModel[234].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[234].setRotationPoint(-43.25f, -5.0f, -11.0f);

		bodyModel[235] = new ModelRendererTurbo(this, 213, 220, textureX, textureY);
		bodyModel[235].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[235].setRotationPoint(-43.25f, -7.0f, -11.0f);

		bodyModel[236] = new ModelRendererTurbo(this, 206, 220, textureX, textureY);
		bodyModel[236].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[236].setRotationPoint(-43.25f, -9.0f, -10.625f);

		bodyModel[237] = new ModelRendererTurbo(this, 414, 178, textureX, textureY);
		bodyModel[237].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[237].setRotationPoint(-41.25f, -9.0f, -10.625f);

		bodyModel[238] = new ModelRendererTurbo(this, 199, 220, textureX, textureY);
		bodyModel[238].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[238].setRotationPoint(-43.25f, -5.0f, 10.0f);

		bodyModel[239] = new ModelRendererTurbo(this, 167, 220, textureX, textureY);
		bodyModel[239].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[239].setRotationPoint(-43.25f, -7.0f, 10.0f);

		bodyModel[240] = new ModelRendererTurbo(this, 160, 220, textureX, textureY);
		bodyModel[240].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[240].setRotationPoint(-43.25f, -9.0f, 9.625f);

		bodyModel[241] = new ModelRendererTurbo(this, 286, 177, textureX, textureY);
		bodyModel[241].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[241].setRotationPoint(-41.25f, -9.0f, 9.625f);

		bodyModel[242] = new ModelRendererTurbo(this, 119, 220, textureX, textureY);
		bodyModel[242].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[242].setRotationPoint(-36.75f, -5.0f, -11.0f);

		bodyModel[243] = new ModelRendererTurbo(this, 77, 220, textureX, textureY);
		bodyModel[243].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[243].setRotationPoint(-36.75f, -7.0f, -11.0f);

		bodyModel[244] = new ModelRendererTurbo(this, 41, 220, textureX, textureY);
		bodyModel[244].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[244].setRotationPoint(-36.75f, -9.0f, -10.625f);

		bodyModel[245] = new ModelRendererTurbo(this, 505, 219, textureX, textureY);
		bodyModel[245].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[245].setRotationPoint(-30.25f, -5.0f, -11.0f);

		bodyModel[246] = new ModelRendererTurbo(this, 498, 219, textureX, textureY);
		bodyModel[246].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[246].setRotationPoint(-30.25f, -7.0f, -11.0f);

		bodyModel[247] = new ModelRendererTurbo(this, 461, 219, textureX, textureY);
		bodyModel[247].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[247].setRotationPoint(-30.25f, -9.0f, -10.625f);

		bodyModel[248] = new ModelRendererTurbo(this, 394, 219, textureX, textureY);
		bodyModel[248].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[248].setRotationPoint(-23.75f, -5.0f, -11.0f);

		bodyModel[249] = new ModelRendererTurbo(this, 372, 219, textureX, textureY);
		bodyModel[249].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[249].setRotationPoint(-23.75f, -7.0f, -11.0f);

		bodyModel[250] = new ModelRendererTurbo(this, 337, 219, textureX, textureY);
		bodyModel[250].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[250].setRotationPoint(-23.75f, -9.0f, -10.625f);

		bodyModel[251] = new ModelRendererTurbo(this, 317, 219, textureX, textureY);
		bodyModel[251].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[251].setRotationPoint(-17.25f, -5.0f, -11.0f);

		bodyModel[252] = new ModelRendererTurbo(this, 153, 219, textureX, textureY);
		bodyModel[252].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[252].setRotationPoint(-17.25f, -7.0f, -11.0f);

		bodyModel[253] = new ModelRendererTurbo(this, 146, 219, textureX, textureY);
		bodyModel[253].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[253].setRotationPoint(-17.25f, -9.0f, -10.625f);

		bodyModel[254] = new ModelRendererTurbo(this, 154, 177, textureX, textureY);
		bodyModel[254].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[254].setRotationPoint(-34.75f, -9.0f, -10.625f);

		bodyModel[255] = new ModelRendererTurbo(this, 60, 177, textureX, textureY);
		bodyModel[255].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[255].setRotationPoint(-28.25f, -9.0f, -10.625f);

		bodyModel[256] = new ModelRendererTurbo(this, 387, 176, textureX, textureY);
		bodyModel[256].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[256].setRotationPoint(-21.75f, -9.0f, -10.625f);

		bodyModel[257] = new ModelRendererTurbo(this, 370, 176, textureX, textureY);
		bodyModel[257].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[257].setRotationPoint(-15.25f, -9.0f, -10.625f);

		bodyModel[258] = new ModelRendererTurbo(this, 461, 183, textureX, textureY);
		bodyModel[258].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[258].setRotationPoint(34.75f, -9.0f, 9.625f);

		bodyModel[259] = new ModelRendererTurbo(this, 352, 176, textureX, textureY);
		bodyModel[259].addShapeBox(0, 0, 0, 5, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[259].setRotationPoint(34.75f, -5.0f, 10.0f);

		bodyModel[260] = new ModelRendererTurbo(this, 129, 183, textureX, textureY);
		bodyModel[260].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[260].setRotationPoint(34.75f, -7.0f, 10.0f);

		bodyModel[261] = new ModelRendererTurbo(this, 332, 176, textureX, textureY);
		bodyModel[261].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[261].setRotationPoint(34.75f, -9.0f, -10.625f);

		bodyModel[262] = new ModelRendererTurbo(this, 211, 176, textureX, textureY);
		bodyModel[262].addShapeBox(0, 0, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[262].setRotationPoint(34.75f, -5.0f, -11.0f);

		bodyModel[263] = new ModelRendererTurbo(this, 192, 176, textureX, textureY);
		bodyModel[263].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[263].setRotationPoint(34.75f, -7.0f, -11.0f);

		bodyModel[264] = new ModelRendererTurbo(this, 417, 95, textureX, textureY);
		bodyModel[264].addShapeBox(0, 0, 0, 13, 2, 1, 0, 0, 0, -0.375f, -0.125f, 0, -0.375f, -0.125f, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[264].setRotationPoint(44.25f, -7.0f, -11.0f);

		bodyModel[265] = new ModelRendererTurbo(this, 388, 95, textureX, textureY);
		bodyModel[265].addShapeBox(0, 0, 0, 13, 2, 1, 0, 0, 0, -0.6875f, -0.125f, 0, -0.6875f, -0.125f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[265].setRotationPoint(44.25f, -9.0f, -10.625f);

		bodyModel[266] = new ModelRendererTurbo(this, 359, 94, textureX, textureY);
		bodyModel[266].addShapeBox(0, 0, 0, 13, 3, 1, 0, 0, 0, -0.5f, -0.125f, 0, -0.5f, -0.125f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.125f, 0, -0.25f, -0.125f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[266].setRotationPoint(44.25f, -3.0f, 10.0f);

		bodyModel[267] = new ModelRendererTurbo(this, 330, 94, textureX, textureY);
		bodyModel[267].addShapeBox(0, 0, 0, 13, 2, 1, 0, 0, 0, -0.5f, -0.125f, 0, -0.5f, -0.125f, 0, 0, 0, 0, 0, 0, -0.25f, -0.125f, -0.125f, -0.25f, -0.125f, -0.125f, -0.25f, -0.375f, 0, -0.25f, -0.375f);
		bodyModel[267].setRotationPoint(44.25f, 0.0f, 9.75f);

		bodyModel[268] = new ModelRendererTurbo(this, 482, 27, textureX, textureY);
		bodyModel[268].addShapeBox(0, 0, 0, 13, 2, 1, 0, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[268].setRotationPoint(44.25f, -5.0f, -11.0f);

		bodyModel[269] = new ModelRendererTurbo(this, 480, 67, textureX, textureY);
		bodyModel[269].addShapeBox(0, 0, 0, 13, 3, 1, 0, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.25f, -0.125f, 0, -0.25f, -0.125f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[269].setRotationPoint(44.25f, -3.0f, -11.0f);

		bodyModel[270] = new ModelRendererTurbo(this, 482, 23, textureX, textureY);
		bodyModel[270].addShapeBox(0, 0, 0, 13, 2, 1, 0, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, -0.375f, -0.125f, -0.25f, -0.375f, -0.125f, -0.25f, -0.125f, 0, -0.25f, -0.125f);
		bodyModel[270].setRotationPoint(44.25f, 0.0f, -10.75f);

		bodyModel[271] = new ModelRendererTurbo(this, 482, 19, textureX, textureY);
		bodyModel[271].addShapeBox(0, 0, 0, 13, 2, 1, 0, 0, 0, -1.0625f, -0.125f, 0, -1.0625f, -0.125f, 0, 0.5625f, 0, 0, 0.5625f, 0, -0.25f, 0, -0.125f, -0.25f, 0, -0.125f, -0.25f, -0.5f, 0, -0.25f, -0.5f);
		bodyModel[271].setRotationPoint(44.25f, -10.75f, -9.9375f);

		bodyModel[272] = new ModelRendererTurbo(this, 482, 15, textureX, textureY);
		bodyModel[272].addShapeBox(0, 0, 0, 13, 2, 1, 0, 0, 0, -0.125f, -0.125f, 0, -0.125f, -0.125f, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, -0.125f, 0, -0.5f, -0.125f, 0, 0, 0, 0, 0);
		bodyModel[272].setRotationPoint(44.25f, -7.0f, 10.0f);

		bodyModel[273] = new ModelRendererTurbo(this, 473, 8, textureX, textureY);
		bodyModel[273].addShapeBox(0, 0, 0, 13, 2, 1, 0, 0, 0, 0.1875f, -0.125f, 0, 0.1875f, -0.125f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, -0.125f, 0, -0.5f, -0.125f, 0, 0, 0, 0, 0);
		bodyModel[273].setRotationPoint(44.25f, -9.0f, 9.625f);

		bodyModel[274] = new ModelRendererTurbo(this, 473, 4, textureX, textureY);
		bodyModel[274].addShapeBox(0, 0, 0, 13, 2, 1, 0, 0, 0, -0.5f, -0.125f, 0, -0.5f, -0.125f, 0, 0, 0, 0, 0, 0, 0, -0.5f, -0.125f, 0, -0.5f, -0.125f, 0, 0, 0, 0, 0);
		bodyModel[274].setRotationPoint(44.25f, -5.0f, 10.0f);

		bodyModel[275] = new ModelRendererTurbo(this, 473, 0, textureX, textureY);
		bodyModel[275].addShapeBox(0, 0, 0, 13, 2, 1, 0, 0, 0, 0.5625f, -0.125f, 0, 0.5625f, -0.125f, 0, -1.0625f, 0, 0, -1.0625f, 0, -0.25f, -0.5f, -0.125f, -0.25f, -0.5f, -0.125f, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[275].setRotationPoint(44.25f, -10.75f, 8.9375f);

		bodyModel[276] = new ModelRendererTurbo(this, 470, 51, textureX, textureY);
		bodyModel[276].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.5f, 0.0625f, 0, -0.5f, 0.0625f, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.125f, 0.0625f, 0, -0.125f, 0.0625f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[276].setRotationPoint(39.75f, -7.0f, -11.0f);

		bodyModel[277] = new ModelRendererTurbo(this, 456, 51, textureX, textureY);
		bodyModel[277].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.8125f, 0.0625f, 0, -0.8125f, 0.0625f, 0, 0.0625f, 0, 0, 0.0625f, 0, 0, -0.125f, 0.0625f, 0, -0.125f, 0.0625f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[277].setRotationPoint(39.75f, -9.0f, -10.625f);

		bodyModel[278] = new ModelRendererTurbo(this, 11, 176, textureX, textureY);
		bodyModel[278].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[278].setRotationPoint(39.75f, -5.0f, -11.0f);

		bodyModel[279] = new ModelRendererTurbo(this, 376, 182, textureX, textureY);
		bodyModel[279].addShapeBox(0, 0, 0, 5, 3, 1, 0, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[279].setRotationPoint(39.75f, -3.0f, -11.0f);

		bodyModel[280] = new ModelRendererTurbo(this, 499, 174, textureX, textureY);
		bodyModel[280].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, -0.5f, -0.5f, -0.25f, -0.5f, -0.5f, -0.25f, -0.25f, 0, -0.25f, -0.25f);
		bodyModel[280].setRotationPoint(39.75f, 0.0f, -10.75f);

		bodyModel[281] = new ModelRendererTurbo(this, 472, 173, textureX, textureY);
		bodyModel[281].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -1.1875f, -0.5f, 0, -1.1875f, -0.5f, 0, 0.4375f, 0, 0, 0.4375f, 0, -0.25f, -0.125f, -0.5f, -0.25f, -0.125f, -0.5f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[281].setRotationPoint(39.75f, -10.75f, -9.9375f);

		bodyModel[282] = new ModelRendererTurbo(this, 0, 49, textureX, textureY);
		bodyModel[282].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.5f, 0.0625f, 0, -0.5f, 0.0625f, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.125f, 0.0625f, 0, -0.125f, 0.0625f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[282].setRotationPoint(43.1875f, -7.0f, -11.0f);

		bodyModel[283] = new ModelRendererTurbo(this, 506, 45, textureX, textureY);
		bodyModel[283].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.8125f, 0.0625f, 0, -0.8125f, 0.0625f, 0, 0.0625f, 0, 0, 0.0625f, 0, 0, -0.125f, 0.0625f, 0, -0.125f, 0.0625f, 0, -0.625f, 0, 0, -0.625f);
		bodyModel[283].setRotationPoint(43.1875f, -9.0f, -10.625f);

		bodyModel[284] = new ModelRendererTurbo(this, 390, 43, textureX, textureY);
		bodyModel[284].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.5625f);
		bodyModel[284].setRotationPoint(40.4375f, -7.0f, -11.0f);

		bodyModel[285] = new ModelRendererTurbo(this, 0, 38, textureX, textureY);
		bodyModel[285].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.75f, -0.375f, 0, -0.75f, -0.375f, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.5625f);
		bodyModel[285].setRotationPoint(40.4375f, -9.0f, -10.625f);

		bodyModel[286] = new ModelRendererTurbo(this, 484, 36, textureX, textureY);
		bodyModel[286].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.5625f);
		bodyModel[286].setRotationPoint(42.5625f, -7.0f, -11.0f);

		bodyModel[287] = new ModelRendererTurbo(this, 437, 36, textureX, textureY);
		bodyModel[287].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.75f, -0.375f, 0, -0.75f, -0.375f, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.5625f);
		bodyModel[287].setRotationPoint(42.5625f, -9.0f, -10.625f);

		bodyModel[288] = new ModelRendererTurbo(this, 18, 212, textureX, textureY);
		bodyModel[288].addShapeBox(0, 0, 0, 3, 1, 1, 0, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f);
		bodyModel[288].setRotationPoint(40.5f, -5.5f, -11.25f);

		bodyModel[289] = new ModelRendererTurbo(this, 9, 212, textureX, textureY);
		bodyModel[289].addShapeBox(0, 0, 0, 3, 1, 1, 0, -0.3125f, -0.375f, -0.453125f, -0.3125f, -0.375f, -0.453125f, -0.3125f, -0.375f, -0.171875f, -0.3125f, -0.375f, -0.171875f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f);
		bodyModel[289].setRotationPoint(40.5f, -9.625f, -10.1875f);

		bodyModel[290] = new ModelRendererTurbo(this, 404, 36, textureX, textureY);
		bodyModel[290].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.25f, 0.0625f, 0, -0.25f, 0.0625f, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[290].setRotationPoint(39.75f, -7.0f, 10.0f);

		bodyModel[291] = new ModelRendererTurbo(this, 390, 36, textureX, textureY);
		bodyModel[291].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, 0.0625f, 0.0625f, 0, 0.0625f, 0.0625f, 0, -0.8125f, 0, 0, -0.8125f, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[291].setRotationPoint(39.75f, -9.0f, 9.625f);

		bodyModel[292] = new ModelRendererTurbo(this, 456, 173, textureX, textureY);
		bodyModel[292].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[292].setRotationPoint(39.75f, -5.0f, 10.0f);

		bodyModel[293] = new ModelRendererTurbo(this, 316, 173, textureX, textureY);
		bodyModel[293].addShapeBox(0, 0, 0, 5, 3, 1, 0, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[293].setRotationPoint(39.75f, -3.0f, 10.0f);

		bodyModel[294] = new ModelRendererTurbo(this, 303, 173, textureX, textureY);
		bodyModel[294].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, -0.625f, -0.5f, 0, -0.625f, -0.5f, 0, -0.125f, 0, 0, -0.125f, 0, -0.25f, -0.25f, -0.5f, -0.25f, -0.25f, -0.5f, -0.25f, -0.5f, 0, -0.25f, -0.5f);
		bodyModel[294].setRotationPoint(39.75f, 0.0f, 9.75f);

		bodyModel[295] = new ModelRendererTurbo(this, 286, 173, textureX, textureY);
		bodyModel[295].addShapeBox(0, 0, 0, 5, 2, 1, 0, 0, 0, 0.4375f, -0.5f, 0, 0.4375f, -0.5f, 0, -1.1875f, 0, 0, -1.1875f, 0, -0.25f, -0.625f, -0.5f, -0.25f, -0.625f, -0.5f, -0.25f, -0.125f, 0, -0.25f, -0.125f);
		bodyModel[295].setRotationPoint(39.75f, -10.75f, 8.9375f);

		bodyModel[296] = new ModelRendererTurbo(this, 507, 31, textureX, textureY);
		bodyModel[296].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, -0.25f, 0.0625f, 0, -0.25f, 0.0625f, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[296].setRotationPoint(43.1875f, -7.0f, 10.0f);

		bodyModel[297] = new ModelRendererTurbo(this, 0, 31, textureX, textureY);
		bodyModel[297].addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, 0.0625f, 0.0625f, 0, 0.0625f, 0.0625f, 0, -0.8125f, 0, 0, -0.8125f, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.125f, 0, 0, -0.125f);
		bodyModel[297].setRotationPoint(43.1875f, -9.0f, 9.625f);

		bodyModel[298] = new ModelRendererTurbo(this, 423, 27, textureX, textureY);
		bodyModel[298].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.0625f);
		bodyModel[298].setRotationPoint(40.4375f, -7.0f, 10.0f);

		bodyModel[299] = new ModelRendererTurbo(this, 350, 24, textureX, textureY);
		bodyModel[299].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.75f, -0.375f, 0, -0.75f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.0625f);
		bodyModel[299].setRotationPoint(40.4375f, -9.0f, 9.625f);

		bodyModel[300] = new ModelRendererTurbo(this, 0, 24, textureX, textureY);
		bodyModel[300].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.4375f, -0.375f, 0, -0.4375f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.5625f, -0.375f, -0.125f, -0.0625f, -0.375f, -0.125f, -0.0625f);
		bodyModel[300].setRotationPoint(42.5625f, -7.0f, 10.0f);

		bodyModel[301] = new ModelRendererTurbo(this, 447, 23, textureX, textureY);
		bodyModel[301].addShapeBox(0, 0, 0, 1, 2, 1, 0, -0.375f, 0, 0.125f, -0.375f, 0, 0.125f, -0.375f, 0, -0.75f, -0.375f, 0, -0.75f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.0625f, -0.375f, 0, -0.0625f);
		bodyModel[301].setRotationPoint(42.5625f, -9.0f, 9.625f);

		bodyModel[302] = new ModelRendererTurbo(this, 0, 212, textureX, textureY);
		bodyModel[302].addShapeBox(0, 0, 0, 3, 1, 1, 0, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f);
		bodyModel[302].setRotationPoint(40.5f, -5.5f, 10.25f);

		bodyModel[303] = new ModelRendererTurbo(this, 478, 211, textureX, textureY);
		bodyModel[303].addShapeBox(0, 0, 0, 3, 1, 1, 0, -0.3125f, -0.375f, -0.171875f, -0.3125f, -0.375f, -0.171875f, -0.3125f, -0.375f, -0.453125f, -0.3125f, -0.375f, -0.453125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f, -0.3125f, -0.375f, -0.3125f);
		bodyModel[303].setRotationPoint(40.5f, -9.625f, 9.1875f);

		bodyModel[304] = new ModelRendererTurbo(this, 112, 219, textureX, textureY);
		bodyModel[304].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.625f, 0.0625f, 0, -0.625f, 0.0625f, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.25f, 0.0625f, 0, -0.25f, 0.0625f, 0, -0.75f, 0, 0, -0.75f);
		bodyModel[304].setRotationPoint(40.875f, -7.0f, -11.0f);

		bodyModel[305] = new ModelRendererTurbo(this, 105, 219, textureX, textureY);
		bodyModel[305].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.9375f, 0.0625f, 0, -0.9375f, 0.0625f, 0, -0.0625f, 0, 0, -0.0625f, 0, 0, -0.25f, 0.0625f, 0, -0.25f, 0.0625f, 0, -0.75f, 0, 0, -0.75f);
		bodyModel[305].setRotationPoint(40.875f, -9.0f, -10.625f);

		bodyModel[306] = new ModelRendererTurbo(this, 98, 219, textureX, textureY);
		bodyModel[306].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0.0625f, 0, -0.375f, 0.0625f, 0, -0.625f, 0, 0, -0.625f, 0, 0, -0.75f, 0.0625f, 0, -0.75f, 0.0625f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[306].setRotationPoint(40.875f, -7.0f, 10.0f);

		bodyModel[307] = new ModelRendererTurbo(this, 91, 219, textureX, textureY);
		bodyModel[307].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.0625f, 0.0625f, 0, -0.0625f, 0.0625f, 0, -0.9375f, 0, 0, -0.9375f, 0, 0, -0.75f, 0.0625f, 0, -0.75f, 0.0625f, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[307].setRotationPoint(40.875f, -9.0f, 9.625f);

		bodyModel[308] = new ModelRendererTurbo(this, 38, 161, textureX, textureY);
		bodyModel[308].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, -0.5f, 0, 0, -0.5f, -0.25f, -0.625f, 0, -0.25f, -0.625f, 0, -0.25f, 0, -0.5f, -0.25f, 0);
		bodyModel[308].setRotationPoint(37.0f, 0.0f, -10.5f);

		bodyModel[309] = new ModelRendererTurbo(this, 19, 161, textureX, textureY);
		bodyModel[309].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[309].setRotationPoint(37.0f, -7.0f, -10.5f);

		bodyModel[310] = new ModelRendererTurbo(this, 0, 161, textureX, textureY);
		bodyModel[310].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, -1.0625f, 0, 0, -1.0625f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[310].setRotationPoint(37.0f, -9.0f, -10.5f);

		bodyModel[311] = new ModelRendererTurbo(this, 488, 159, textureX, textureY);
		bodyModel[311].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[311].setRotationPoint(37.0f, -5.0f, -10.5f);

		bodyModel[312] = new ModelRendererTurbo(this, 439, 159, textureX, textureY);
		bodyModel[312].addShapeBox(0, 0, 0, 1, 3, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[312].setRotationPoint(37.0f, -3.0f, -10.5f);

		bodyModel[313] = new ModelRendererTurbo(this, 474, 122, textureX, textureY);
		bodyModel[313].addShapeBox(0, 0, 0, 1, 2, 7, 0, -0.5f, 0, -1.125f, 0, 0, -1.125f, 0, 0, 0, -0.5f, 0, 0, -0.5f, -0.25f, -0.0625f, 0, -0.25f, -0.0625f, 0, -0.25f, 0, -0.5f, -0.25f, 0);
		bodyModel[313].setRotationPoint(37.0f, -10.75f, -9.5f);

		bodyModel[314] = new ModelRendererTurbo(this, 428, 156, textureX, textureY);
		bodyModel[314].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f, -0.5f, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, -0.625f, -0.5f, -0.25f, -0.625f);
		bodyModel[314].setRotationPoint(37.0f, 0.0f, 2.5f);

		bodyModel[315] = new ModelRendererTurbo(this, 409, 156, textureX, textureY);
		bodyModel[315].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[315].setRotationPoint(37.0f, -7.0f, 2.5f);

		bodyModel[316] = new ModelRendererTurbo(this, 293, 156, textureX, textureY);
		bodyModel[316].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -1.0625f, -0.5f, 0, -1.0625f, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.375f, -0.5f, 0, -0.375f);
		bodyModel[316].setRotationPoint(37.0f, -9.0f, 2.5f);

		bodyModel[317] = new ModelRendererTurbo(this, 493, 105, textureX, textureY);
		bodyModel[317].addShapeBox(0, 0, 0, 1, 2, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[317].setRotationPoint(37.0f, -5.0f, 2.5f);

		bodyModel[318] = new ModelRendererTurbo(this, 333, 55, textureX, textureY);
		bodyModel[318].addShapeBox(0, 0, 0, 1, 3, 8, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -0.25f, -0.5f, 0, -0.25f);
		bodyModel[318].setRotationPoint(37.0f, -3.0f, 2.5f);

		bodyModel[319] = new ModelRendererTurbo(this, 495, 116, textureX, textureY);
		bodyModel[319].addShapeBox(0, 0, 0, 1, 2, 7, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, -1.125f, -0.5f, 0, -1.125f, -0.5f, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, -0.0625f, -0.5f, -0.25f, -0.0625f);
		bodyModel[319].setRotationPoint(37.0f, -10.75f, 2.5f);

		bodyModel[320] = new ModelRendererTurbo(this, 477, 125, textureX, textureY);
		bodyModel[320].addShapeBox(0, 0, 0, 1, 1, 15, 0, -0.5f, 0, -1.375f, 0, 0, -1.375f, 0, 0, 0.375f, -0.5f, 0, 0.375f, -0.5f, 0.1875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 1.75f, -0.5f, 0.1875f, 1.75f);
		bodyModel[320].setRotationPoint(37.0f, -11.9375f, -8.375f);

		bodyModel[321] = new ModelRendererTurbo(this, 392, 15, textureX, textureY);
		bodyModel[321].addShapeBox(0, 0, 0, 1, 1, 6, 0, -0.5f, 0, -1.375f, 0, 0, -1.375f, 0, 0, -0.125f, -0.5f, 0, -0.125f, -0.5f, 0.4375f, 3.375f, 0, 0.4375f, 3.375f, 0, 0.4375f, 4.625f, -0.5f, 0.4375f, 4.625f);
		bodyModel[321].setRotationPoint(37.0f, -13.375f, -3.625f);

		bodyModel[322] = new ModelRendererTurbo(this, 231, 172, textureX, textureY);
		bodyModel[322].addShapeBox(0, 0, 0, 1, 1, 5, 0, -0.5f, 0, -2.5f, 0, 0, -2.5f, 0, 0, -2.5f, -0.5f, 0, -2.5f, -0.5f, -0.625f, -0.25f, 0, -0.625f, -0.25f, 0, -0.625f, -0.25f, -0.5f, -0.625f, -0.25f);
		bodyModel[322].setRotationPoint(37.0f, -13.75f, -2.5f);

		bodyModel[323] = new ModelRendererTurbo(this, 283, 101, textureX, textureY);
		bodyModel[323].addShapeBox(0, 0, 0, 1, 1, 7, 0, -0.5f, 0, 0.375f, 0, 0, 0.375f, 0, 0, 0, -0.5f, 0, 0, -0.5f, -0.25f, 0.375f, 0, -0.25f, 0.375f, 0, -0.25f, 0, -0.5f, -0.25f, 0);
		bodyModel[323].setRotationPoint(37.0f, 1.75f, -9.5f);

		bodyModel[324] = new ModelRendererTurbo(this, 411, 15, textureX, textureY);
		bodyModel[324].addShapeBox(0, 0, 0, 1, 1, 7, 0, -0.5f, 0, 0.375f, 0, 0, 0.375f, 0, 0, 0, -0.5f, 0, 0, -0.5f, -0.25f, 0.375f, 0, -0.25f, 0.375f, 0, -0.25f, 0, -0.5f, -0.25f, 0);
		bodyModel[324].setRotationPoint(37.0f, 1.75f, 2.875f);

		bodyModel[325] = new ModelRendererTurbo(this, 451, 42, textureX, textureY);
		bodyModel[325].addShapeBox(0, 0, 0, 1, 1, 5, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, -0.625f, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.5f, -0.625f, 0);
		bodyModel[325].setRotationPoint(37.0f, -10.75f, -2.5f);

		bodyModel[326] = new ModelRendererTurbo(this, 196, 183, textureX, textureY);
		bodyModel[326].addShapeBox(0, 0, 0, 1, 13, 5, 0, -0.625f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.625f, 0, 0, -0.625f, -0.125f, 0, -0.125f, -0.125f, 0, -0.125f, -0.125f, 0, -0.625f, -0.125f, 0);
		bodyModel[326].setRotationPoint(37.0f, -10.375f, -2.5f);

		bodyModel[327] = new ModelRendererTurbo(this, 406, 0, textureX, textureY);
		bodyModel[327].addShapeBox(0, 0, 0, 7, 2, 3, 0, 2.875f, -2, -0.34375f, 0, 0.6875f, -0.34375f, 0, 0.6875f, -0.34375f, 2.875f, -2, -0.34375f, 2.875f, 0, -0.34375f, 0, 0, -0.34375f, 0, 0, -0.34375f, 2.875f, 0, -0.34375f);
		bodyModel[327].setRotationPoint(48.8125f, -15.9375f, -1.5f);

		bodyModel[328] = new ModelRendererTurbo(this, 351, 15, textureX, textureY);
		bodyModel[328].addShapeBox(0, 0, 0, 1, 1, 1, 0, -0.3125f, -0.25f, -0.25f, -0.375f, -0.25f, -0.25f, -0.375f, -0.25f, -0.25f, -0.3125f, -0.25f, -0.25f, -0.3125f, -0.25f, -0.25f, -0.375f, -0.25f, -0.25f, -0.375f, -0.25f, -0.25f, -0.3125f, -0.25f, -0.25f);
		bodyModel[328].setRotationPoint(57.53125f, -16.796875f, -0.5f);

		bodyModel[329] = new ModelRendererTurbo(this, 18, 0, textureX, textureY);
		bodyModel[329].addShapeBox(0, 0, 0, 0, 1, 1, 0, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f);
		bodyModel[329].setRotationPoint(58.15625f, -17.296875f, -0.5f);

		bodyModel[330] = new ModelRendererTurbo(this, 424, 9, textureX, textureY);
		bodyModel[330].addShapeBox(0, 0, 0, 1, 0, 1, 0, -0.375f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.375f, 0, -0.25f, -0.375f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.375f, 0, -0.25f);
		bodyModel[330].setRotationPoint(57.46875f, -17.046875f, -0.5f);

		bodyModel[331] = new ModelRendererTurbo(this, 0, 8, textureX, textureY);
		bodyModel[331].addShapeBox(0, 0, 0, 1, 0, 1, 0, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.5f, -0.3125f, 0, -0.5f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.5f, -0.3125f, 0, -0.5f, -0.3125f, 0, -0.25f);
		bodyModel[331].setRotationPoint(57.84375f, -17.046875f, -0.5f);

		bodyModel[332] = new ModelRendererTurbo(this, 34, 219, textureX, textureY);
		bodyModel[332].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[332].setRotationPoint(-10.75f, -5.0f, -11.0f);

		bodyModel[333] = new ModelRendererTurbo(this, 27, 219, textureX, textureY);
		bodyModel[333].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[333].setRotationPoint(-10.75f, -7.0f, -11.0f);

		bodyModel[334] = new ModelRendererTurbo(this, 7, 219, textureX, textureY);
		bodyModel[334].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[334].setRotationPoint(-10.75f, -9.0f, -10.625f);

		bodyModel[335] = new ModelRendererTurbo(this, 476, 218, textureX, textureY);
		bodyModel[335].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[335].setRotationPoint(-4.25f, -5.0f, -11.0f);

		bodyModel[336] = new ModelRendererTurbo(this, 469, 218, textureX, textureY);
		bodyModel[336].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[336].setRotationPoint(-4.25f, -7.0f, -11.0f);

		bodyModel[337] = new ModelRendererTurbo(this, 439, 218, textureX, textureY);
		bodyModel[337].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[337].setRotationPoint(-4.25f, -9.0f, -10.625f);

		bodyModel[338] = new ModelRendererTurbo(this, 437, 175, textureX, textureY);
		bodyModel[338].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[338].setRotationPoint(-8.75f, -9.0f, -10.625f);

		bodyModel[339] = new ModelRendererTurbo(this, 485, 173, textureX, textureY);
		bodyModel[339].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[339].setRotationPoint(-2.25f, -9.0f, -10.625f);

		bodyModel[340] = new ModelRendererTurbo(this, 387, 218, textureX, textureY);
		bodyModel[340].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[340].setRotationPoint(2.25f, -5.0f, -11.0f);

		bodyModel[341] = new ModelRendererTurbo(this, 310, 218, textureX, textureY);
		bodyModel[341].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[341].setRotationPoint(2.25f, -7.0f, -11.0f);

		bodyModel[342] = new ModelRendererTurbo(this, 272, 218, textureX, textureY);
		bodyModel[342].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[342].setRotationPoint(2.25f, -9.0f, -10.625f);

		bodyModel[343] = new ModelRendererTurbo(this, 193, 218, textureX, textureY);
		bodyModel[343].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[343].setRotationPoint(8.75f, -5.0f, -11.0f);

		bodyModel[344] = new ModelRendererTurbo(this, 139, 218, textureX, textureY);
		bodyModel[344].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[344].setRotationPoint(8.75f, -7.0f, -11.0f);

		bodyModel[345] = new ModelRendererTurbo(this, 84, 218, textureX, textureY);
		bodyModel[345].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[345].setRotationPoint(8.75f, -9.0f, -10.625f);

		bodyModel[346] = new ModelRendererTurbo(this, 404, 172, textureX, textureY);
		bodyModel[346].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[346].setRotationPoint(4.25f, -9.0f, -10.625f);

		bodyModel[347] = new ModelRendererTurbo(this, 426, 167, textureX, textureY);
		bodyModel[347].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[347].setRotationPoint(10.75f, -9.0f, -10.625f);

		bodyModel[348] = new ModelRendererTurbo(this, 71, 218, textureX, textureY);
		bodyModel[348].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[348].setRotationPoint(15.25f, -5.0f, -11.0f);

		bodyModel[349] = new ModelRendererTurbo(this, 20, 218, textureX, textureY);
		bodyModel[349].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[349].setRotationPoint(15.25f, -7.0f, -11.0f);

		bodyModel[350] = new ModelRendererTurbo(this, 0, 218, textureX, textureY);
		bodyModel[350].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[350].setRotationPoint(15.25f, -9.0f, -10.625f);

		bodyModel[351] = new ModelRendererTurbo(this, 492, 217, textureX, textureY);
		bodyModel[351].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[351].setRotationPoint(21.75f, -5.0f, -11.0f);

		bodyModel[352] = new ModelRendererTurbo(this, 454, 217, textureX, textureY);
		bodyModel[352].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[352].setRotationPoint(21.75f, -7.0f, -11.0f);

		bodyModel[353] = new ModelRendererTurbo(this, 78, 215, textureX, textureY);
		bodyModel[353].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[353].setRotationPoint(21.75f, -9.0f, -10.625f);

		bodyModel[354] = new ModelRendererTurbo(this, 409, 167, textureX, textureY);
		bodyModel[354].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[354].setRotationPoint(17.25f, -9.0f, -10.625f);

		bodyModel[355] = new ModelRendererTurbo(this, 378, 167, textureX, textureY);
		bodyModel[355].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[355].setRotationPoint(23.75f, -9.0f, -10.625f);

		bodyModel[356] = new ModelRendererTurbo(this, 71, 215, textureX, textureY);
		bodyModel[356].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[356].setRotationPoint(28.25f, -5.0f, -11.0f);

		bodyModel[357] = new ModelRendererTurbo(this, 35, 215, textureX, textureY);
		bodyModel[357].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[357].setRotationPoint(28.25f, -7.0f, -11.0f);

		bodyModel[358] = new ModelRendererTurbo(this, 28, 215, textureX, textureY);
		bodyModel[358].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f);
		bodyModel[358].setRotationPoint(28.25f, -9.0f, -10.625f);

		bodyModel[359] = new ModelRendererTurbo(this, 347, 167, textureX, textureY);
		bodyModel[359].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, -0.6875f, 0.5f, 0, -0.6875f, 0.5f, 0, 0.1875f, 0, 0, 0.1875f, 0, 0.5f, -0.1717f, 0.5f, 0.5f, -0.1717f, 0.5f, 0.5f, -0.328125f, 0, 0.5f, -0.328125f);
		bodyModel[359].setRotationPoint(30.25f, -9.0f, -10.625f);

		bodyModel[360] = new ModelRendererTurbo(this, 415, 172, textureX, textureY);
		bodyModel[360].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[360].setRotationPoint(-10.75f, -5.0f, 10.0f);

		bodyModel[361] = new ModelRendererTurbo(this, 426, 170, textureX, textureY);
		bodyModel[361].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[361].setRotationPoint(-10.75f, -7.0f, 10.0f);

		bodyModel[362] = new ModelRendererTurbo(this, 458, 165, textureX, textureY);
		bodyModel[362].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[362].setRotationPoint(-10.75f, -9.0f, 9.625f);

		bodyModel[363] = new ModelRendererTurbo(this, 239, 164, textureX, textureY);
		bodyModel[363].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[363].setRotationPoint(-4.25f, -5.0f, 10.0f);

		bodyModel[364] = new ModelRendererTurbo(this, 488, 161, textureX, textureY);
		bodyModel[364].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[364].setRotationPoint(-4.25f, -7.0f, 10.0f);

		bodyModel[365] = new ModelRendererTurbo(this, 439, 159, textureX, textureY);
		bodyModel[365].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[365].setRotationPoint(-4.25f, -9.0f, 9.625f);

		bodyModel[366] = new ModelRendererTurbo(this, 484, 122, textureX, textureY);
		bodyModel[366].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[366].setRotationPoint(-8.75f, -9.0f, 9.625f);

		bodyModel[367] = new ModelRendererTurbo(this, 340, 98, textureX, textureY);
		bodyModel[367].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[367].setRotationPoint(-2.25f, -9.0f, 9.625f);

		bodyModel[368] = new ModelRendererTurbo(this, 495, 116, textureX, textureY);
		bodyModel[368].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[368].setRotationPoint(2.25f, -5.0f, 10.0f);

		bodyModel[369] = new ModelRendererTurbo(this, 272, 156, textureX, textureY);
		bodyModel[369].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[369].setRotationPoint(2.25f, -7.0f, 10.0f);

		bodyModel[370] = new ModelRendererTurbo(this, 485, 142, textureX, textureY);
		bodyModel[370].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[370].setRotationPoint(2.25f, -9.0f, 9.625f);

		bodyModel[371] = new ModelRendererTurbo(this, 504, 110, textureX, textureY);
		bodyModel[371].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[371].setRotationPoint(8.75f, -5.0f, 10.0f);

		bodyModel[372] = new ModelRendererTurbo(this, 484, 125, textureX, textureY);
		bodyModel[372].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[372].setRotationPoint(8.75f, -7.0f, 10.0f);

		bodyModel[373] = new ModelRendererTurbo(this, 505, 95, textureX, textureY);
		bodyModel[373].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[373].setRotationPoint(8.75f, -9.0f, 9.625f);

		bodyModel[374] = new ModelRendererTurbo(this, 329, 98, textureX, textureY);
		bodyModel[374].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[374].setRotationPoint(4.25f, -9.0f, 9.625f);

		bodyModel[375] = new ModelRendererTurbo(this, 500, 92, textureX, textureY);
		bodyModel[375].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[375].setRotationPoint(10.75f, -9.0f, 9.625f);

		bodyModel[376] = new ModelRendererTurbo(this, 459, 88, textureX, textureY);
		bodyModel[376].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[376].setRotationPoint(15.25f, -5.0f, 10.0f);

		bodyModel[377] = new ModelRendererTurbo(this, 505, 81, textureX, textureY);
		bodyModel[377].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[377].setRotationPoint(15.25f, -7.0f, 10.0f);

		bodyModel[378] = new ModelRendererTurbo(this, 436, 81, textureX, textureY);
		bodyModel[378].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[378].setRotationPoint(15.25f, -9.0f, 9.625f);

		bodyModel[379] = new ModelRendererTurbo(this, 480, 74, textureX, textureY);
		bodyModel[379].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[379].setRotationPoint(21.75f, -5.0f, 10.0f);

		bodyModel[380] = new ModelRendererTurbo(this, 436, 74, textureX, textureY);
		bodyModel[380].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[380].setRotationPoint(21.75f, -7.0f, 10.0f);

		bodyModel[381] = new ModelRendererTurbo(this, 387, 73, textureX, textureY);
		bodyModel[381].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[381].setRotationPoint(21.75f, -9.0f, 9.625f);

		bodyModel[382] = new ModelRendererTurbo(this, 342, 91, textureX, textureY);
		bodyModel[382].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[382].setRotationPoint(17.25f, -9.0f, 9.625f);

		bodyModel[383] = new ModelRendererTurbo(this, 331, 91, textureX, textureY);
		bodyModel[383].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[383].setRotationPoint(23.75f, -9.0f, 9.625f);

		bodyModel[384] = new ModelRendererTurbo(this, 443, 57, textureX, textureY);
		bodyModel[384].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[384].setRotationPoint(28.25f, -5.0f, 10.0f);

		bodyModel[385] = new ModelRendererTurbo(this, 443, 64, textureX, textureY);
		bodyModel[385].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, -0.125f, 0, 0, -0.125f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[385].setRotationPoint(28.25f, -7.0f, 10.0f);

		bodyModel[386] = new ModelRendererTurbo(this, 344, 58, textureX, textureY);
		bodyModel[386].addShapeBox(0, 0, 0, 2, 2, 1, 0, 0, 0, 0.1875f, 0, 0, 0.1875f, 0, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0);
		bodyModel[386].setRotationPoint(28.25f, -9.0f, 9.625f);

		bodyModel[387] = new ModelRendererTurbo(this, 448, 88, textureX, textureY);
		bodyModel[387].addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0.1875f, 0.5f, 0, 0.1875f, 0.5f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0.5f, -0.328125f, 0.5f, 0.5f, -0.328125f, 0.5f, 0.5f, -0.1717f, 0, 0.5f, -0.1717f);
		bodyModel[387].setRotationPoint(30.25f, -9.0f, 9.625f);

		bodyModel[388] = new ModelRendererTurbo(this, 0, 139, textureX, textureY);
		bodyModel[388].addShapeBox(0, 0, 0, 102, 1, 4, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0.75f, -1, 0, 0.75f, -1, 0, -0.5f, 0, 0.5f, -0.5f, 0, 0.5f, -1.5f, -0.5f, 0, -1.5f, -0.5f);
		bodyModel[388].setRotationPoint(-64.75f, -9.375f, -9.5f);

		bodyModel[389] = new ModelRendererTurbo(this, 0, 133, textureX, textureY);
		bodyModel[389].addShapeBox(0, 0, 0, 102, 1, 4, 0, 0, 0.75f, -1, 0.5f, 0.75f, -1, 0.5f, 0, 0, 0, 0, 0, 0, -1.5f, -0.5f, 0.5f, -1.5f, -0.5f, 0.5f, -0.5f, 0, 0, -0.5f, 0);
		bodyModel[389].setRotationPoint(-64.75f, -9.375f, 5.5f);

		bodyModel[390] = new ModelRendererTurbo(this, 126, 217, textureX, textureY);
		bodyModel[390].addShapeBox(0, 0, 0, 2, 2, 1, 0, -0.375f, 0, -0.5625f, 0, 0, -0.5625f, 0, 0, -0.3125f, -0.375f, 0, -0.3125f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f, 0, 0, -0.6875f, -0.375f, 0, -0.6875f);
		bodyModel[390].setRotationPoint(56.75f, -7.0f, -11.0f);

		bodyModel[391] = new ModelRendererTurbo(this, 64, 213, textureX, textureY);
		bodyModel[391].addShapeBox(0, 0, 0, 2, 2, 1, 0, -0.375f, 0, -0.875f, 0, 0, -0.875f, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, -0.1875f, 0, 0, -0.1875f, 0, 0, -0.6875f, -0.375f, 0, -0.6875f);
		bodyModel[391].setRotationPoint(56.75f, -9.0f, -10.625f);

		bodyModel[392] = new ModelRendererTurbo(this, 62, 217, textureX, textureY);
		bodyModel[392].addShapeBox(0, 0, 0, 2, 2, 1, 0, -0.375f, 0, -0.1875f, 0, 0, -0.1875f, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.1875f, 0, 0, -0.1875f, 0, 0, -0.6875f, -0.375f, 0, -0.6875f);
		bodyModel[392].setRotationPoint(56.75f, -5.0f, -11.0f);

		bodyModel[393] = new ModelRendererTurbo(this, 55, 216, textureX, textureY);
		bodyModel[393].addShapeBox(0, 0, 0, 2, 3, 1, 0, -0.375f, 0, -0.1875f, 0, 0, -0.1875f, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, 0, -0.4375f, 0, 0, -0.4375f, 0, 0, -0.4375f, -0.375f, 0, -0.4375f);
		bodyModel[393].setRotationPoint(56.75f, -3.0f, -11.0f);

		bodyModel[394] = new ModelRendererTurbo(this, 48, 217, textureX, textureY);
		bodyModel[394].addShapeBox(0, 0, 0, 2, 2, 1, 0, -0.375f, 0, -0.1875f, 0, 0, -0.1875f, 0, 0, -0.6875f, -0.375f, 0, -0.6875f, -0.375f, -0.25f, -0.5625f, 0, -0.25f, -0.5625f, 0, -0.25f, -0.3125f, -0.375f, -0.25f, -0.3125f);
		bodyModel[394].setRotationPoint(56.75f, 0.0f, -10.75f);

		bodyModel[395] = new ModelRendererTurbo(this, 45, 213, textureX, textureY);
		bodyModel[395].addShapeBox(0, 0, 0, 2, 2, 1, 0, -0.375f, 0, -1.25f, 0, 0, -1.25f, 0, 0, 0.375f, -0.375f, 0, 0.375f, -0.375f, -0.25f, -0.1875f, 0, -0.25f, -0.1875f, 0, -0.25f, -0.6875f, -0.375f, -0.25f, -0.6875f);
		bodyModel[395].setRotationPoint(56.75f, -10.75f, -9.9375f);

		bodyModel[396] = new ModelRendererTurbo(this, 487, 212, textureX, textureY);
		bodyModel[396].addShapeBox(0, 0, 0, 2, 2, 1, 0, -0.375f, 0, -0.3125f, 0, 0, -0.3125f, 0, 0, -0.5625f, -0.375f, 0, -0.5625f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.1875f, -0.375f, 0, -0.1875f);
		bodyModel[396].setRotationPoint(56.75f, -7.0f, 10.0f);

		bodyModel[397] = new ModelRendererTurbo(this, 484, 216, textureX, textureY);
		bodyModel[397].addShapeBox(0, 0, 0, 2, 2, 1, 0, -0.375f, 0, 0, 0, 0, 0, 0, 0, -0.875f, -0.375f, 0, -0.875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.1875f, -0.375f, 0, -0.1875f);
		bodyModel[397].setRotationPoint(56.75f, -9.0f, 9.625f);

		bodyModel[398] = new ModelRendererTurbo(this, 446, 216, textureX, textureY);
		bodyModel[398].addShapeBox(0, 0, 0, 2, 2, 1, 0, -0.375f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.1875f, -0.375f, 0, -0.1875f);
		bodyModel[398].setRotationPoint(56.75f, -5.0f, 10.0f);

		bodyModel[399] = new ModelRendererTurbo(this, 422, 215, textureX, textureY);
		bodyModel[399].addShapeBox(0, 0, 0, 2, 3, 1, 0, -0.375f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, 0, -0.4375f, 0, 0, -0.4375f, 0, 0, -0.4375f, -0.375f, 0, -0.4375f);
		bodyModel[399].setRotationPoint(56.75f, -3.0f, 10.0f);

		bodyModel[400] = new ModelRendererTurbo(this, 415, 216, textureX, textureY);
		bodyModel[400].addShapeBox(0, 0, 0, 2, 2, 1, 0, -0.375f, 0, -0.6875f, 0, 0, -0.6875f, 0, 0, -0.1875f, -0.375f, 0, -0.1875f, -0.375f, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.5625f, -0.375f, -0.25f, -0.5625f);
		bodyModel[400].setRotationPoint(56.75f, 0.0f, 9.75f);

		bodyModel[401] = new ModelRendererTurbo(this, 408, 216, textureX, textureY);
		bodyModel[401].addShapeBox(0, 0, 0, 2, 2, 1, 0, -0.375f, 0, 0.375f, 0, 0, 0.375f, 0, 0, -1.25f, -0.375f, 0, -1.25f, -0.375f, -0.25f, -0.6875f, 0, -0.25f, -0.6875f, 0, -0.25f, -0.1875f, -0.375f, -0.25f, -0.1875f);
		bodyModel[401].setRotationPoint(56.75f, -10.75f, 8.9375f);

		bodyModel[402] = new ModelRendererTurbo(this, 1, 1, textureX, textureY);
		bodyModel[402].addShapeBox(0, 0, 0, 2, 1, 2, 0, 0, 0, -0.5f, 0.03125f, -0.21875f, 0, 0.03125f, -0.21875f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0.03125f, 1.21875f, 0, 0.03125f, 1.21875f, 0, 0, 0, -0.5f);
		bodyModel[402].setRotationPoint(-75.96875f, -16.8125f, -1.0f);

		bodyModel[403] = new ModelRendererTurbo(this, 1, 1, textureX, textureY);
		bodyModel[403].addShapeBox(0, 0, 0, 2, 1, 2, 0, 0.03125f, -0.21875f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0.03125f, -0.21875f, 0, 0.03125f, 1.21875f, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0.03125f, 1.21875f, 0);
		bodyModel[403].setRotationPoint(55.84375f, -16.8125f, -1.0f);

		bodyModel[404] = new ModelRendererTurbo(this, 133, 221, textureX, textureY);
		bodyModel[404].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0.6875f, -0.125f, 0, 0.6875f, -0.125f, 0.375f, -1.6875f, 0, 0.375f, -1.6875f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f);
		bodyModel[404].setRotationPoint(-77.125f, -11.0f, 2.375f);

		bodyModel[405] = new ModelRendererTurbo(this, 133, 221, textureX, textureY);
		bodyModel[405].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0.375f, -1.6875f, -0.125f, 0.375f, -1.6875f, -0.125f, 0, 0.6875f, 0, 0, 0.6875f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f);
		bodyModel[405].setRotationPoint(-77.125f, -11.0f, -3.375f);

		bodyModel[406] = new ModelRendererTurbo(this, 133, 221, textureX, textureY);
		bodyModel[406].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0.6875f, -0.125f, 0, 0.6875f, -0.125f, 0.375f, -1.6875f, 0, 0.375f, -1.6875f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f);
		bodyModel[406].setRotationPoint(57.125f, -11.0f, 2.375f);

		bodyModel[407] = new ModelRendererTurbo(this, 133, 221, textureX, textureY);
		bodyModel[407].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0.375f, -1.6875f, -0.125f, 0.375f, -1.6875f, -0.125f, 0, 0.6875f, 0, 0, 0.6875f, 0, 0, -0.3125f, -0.125f, 0, -0.3125f, -0.125f, 0, -0.3125f, 0, 0, -0.3125f);
		bodyModel[407].setRotationPoint(57.125f, -11.0f, -3.375f);

		bodyModel[408] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[408].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[408].setRotationPoint(-60.0f, 0.0f, -8.875f);

		bodyModel[409] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[409].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[409].setRotationPoint(-61.0f, -1.0f, -9.875f);

		bodyModel[410] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[410].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[410].setRotationPoint(-61.0f, -1.5f, -6.875f);

		bodyModel[411] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[411].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[411].setRotationPoint(-61.0f, -1.5f, -9.875f);

		bodyModel[412] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[412].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[412].setRotationPoint(-61.0f, -4.5f, -9.875f);

		bodyModel[413] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[413].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[413].setRotationPoint(-60.5f, -4.5f, -6.875f);

		bodyModel[414] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[414].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[414].setRotationPoint(-60.5f, -4.5f, -9.875f);

		bodyModel[415] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[415].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[415].setRotationPoint(-61.25f, -2.25f, -4.875f);

		bodyModel[416] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[416].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[416].setRotationPoint(-61.25f, -2.375f, -4.875f);

		bodyModel[417] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[417].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[417].setRotationPoint(-61.25f, -2.375f, -4.875f);

		bodyModel[418] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[418].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[418].setRotationPoint(-61.25f, -2.25f, -10.25f);

		bodyModel[419] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[419].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[419].setRotationPoint(-61.25f, -2.375f, -10.25f);

		bodyModel[420] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[420].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[420].setRotationPoint(-61.25f, -2.375f, -10.25f);

		bodyModel[421] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[421].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[421].setRotationPoint(-60.0f, 0.0f, 5.875f);

		bodyModel[422] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[422].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[422].setRotationPoint(-61.0f, -1.0f, 4.875f);

		bodyModel[423] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[423].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[423].setRotationPoint(-61.0f, -1.5f, 7.875f);

		bodyModel[424] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[424].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[424].setRotationPoint(-61.0f, -1.5f, 4.875f);

		bodyModel[425] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[425].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[425].setRotationPoint(-61.0f, -4.5f, 4.875f);

		bodyModel[426] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[426].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[426].setRotationPoint(-60.5f, -4.5f, 7.875f);

		bodyModel[427] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[427].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[427].setRotationPoint(-60.5f, -4.5f, 4.875f);

		bodyModel[428] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[428].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[428].setRotationPoint(-61.25f, -2.25f, 9.875f);

		bodyModel[429] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[429].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[429].setRotationPoint(-61.25f, -2.375f, 9.875f);

		bodyModel[430] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[430].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[430].setRotationPoint(-61.25f, -2.375f, 9.875f);

		bodyModel[431] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[431].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[431].setRotationPoint(-61.25f, -2.25f, 4.5f);

		bodyModel[432] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[432].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[432].setRotationPoint(-61.25f, -2.375f, 4.5f);

		bodyModel[433] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[433].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[433].setRotationPoint(-61.25f, -2.375f, 4.5f);

		bodyModel[434] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[434].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[434].setRotationPoint(-53.5f, 0.0f, -8.875f);

		bodyModel[435] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[435].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[435].setRotationPoint(-54.5f, -1.0f, -9.875f);

		bodyModel[436] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[436].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[436].setRotationPoint(-54.5f, -1.5f, -6.875f);

		bodyModel[437] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[437].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[437].setRotationPoint(-54.5f, -1.5f, -9.875f);

		bodyModel[438] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[438].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[438].setRotationPoint(-54.5f, -4.5f, -9.875f);

		bodyModel[439] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[439].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[439].setRotationPoint(-54.0f, -4.5f, -6.875f);

		bodyModel[440] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[440].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[440].setRotationPoint(-54.0f, -4.5f, -9.875f);

		bodyModel[441] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[441].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[441].setRotationPoint(-54.75f, -2.25f, -4.875f);

		bodyModel[442] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[442].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[442].setRotationPoint(-54.75f, -2.375f, -4.875f);

		bodyModel[443] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[443].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[443].setRotationPoint(-54.75f, -2.375f, -4.875f);

		bodyModel[444] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[444].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[444].setRotationPoint(-54.75f, -2.25f, -10.25f);

		bodyModel[445] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[445].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[445].setRotationPoint(-54.75f, -2.375f, -10.25f);

		bodyModel[446] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[446].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[446].setRotationPoint(-54.75f, -2.375f, -10.25f);

		bodyModel[447] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[447].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[447].setRotationPoint(-53.5f, 0.0f, 5.875f);

		bodyModel[448] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[448].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[448].setRotationPoint(-54.5f, -1.0f, 4.875f);

		bodyModel[449] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[449].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[449].setRotationPoint(-54.5f, -1.5f, 7.875f);

		bodyModel[450] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[450].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[450].setRotationPoint(-54.5f, -1.5f, 4.875f);

		bodyModel[451] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[451].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[451].setRotationPoint(-54.5f, -4.5f, 4.875f);

		bodyModel[452] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[452].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[452].setRotationPoint(-54.0f, -4.5f, 7.875f);

		bodyModel[453] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[453].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[453].setRotationPoint(-54.0f, -4.5f, 4.875f);

		bodyModel[454] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[454].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[454].setRotationPoint(-54.75f, -2.25f, 9.875f);

		bodyModel[455] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[455].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[455].setRotationPoint(-54.75f, -2.375f, 9.875f);

		bodyModel[456] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[456].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[456].setRotationPoint(-54.75f, -2.375f, 9.875f);

		bodyModel[457] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[457].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[457].setRotationPoint(-54.75f, -2.25f, 4.5f);

		bodyModel[458] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[458].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[458].setRotationPoint(-54.75f, -2.375f, 4.5f);

		bodyModel[459] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[459].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[459].setRotationPoint(-54.75f, -2.375f, 4.5f);

		bodyModel[460] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[460].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[460].setRotationPoint(-47.0f, 0.0f, -8.875f);

		bodyModel[461] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[461].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[461].setRotationPoint(-48.0f, -1.0f, -9.875f);

		bodyModel[462] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[462].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[462].setRotationPoint(-48.0f, -1.5f, -6.875f);

		bodyModel[463] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[463].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[463].setRotationPoint(-48.0f, -1.5f, -9.875f);

		bodyModel[464] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[464].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[464].setRotationPoint(-48.0f, -4.5f, -9.875f);

		bodyModel[465] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[465].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[465].setRotationPoint(-47.5f, -4.5f, -6.875f);

		bodyModel[466] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[466].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[466].setRotationPoint(-47.5f, -4.5f, -9.875f);

		bodyModel[467] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[467].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[467].setRotationPoint(-48.25f, -2.25f, -4.875f);

		bodyModel[468] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[468].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[468].setRotationPoint(-48.25f, -2.375f, -4.875f);

		bodyModel[469] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[469].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[469].setRotationPoint(-48.25f, -2.375f, -4.875f);

		bodyModel[470] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[470].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[470].setRotationPoint(-48.25f, -2.25f, -10.25f);

		bodyModel[471] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[471].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[471].setRotationPoint(-48.25f, -2.375f, -10.25f);

		bodyModel[472] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[472].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[472].setRotationPoint(-48.25f, -2.375f, -10.25f);

		bodyModel[473] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[473].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[473].setRotationPoint(-47.0f, 0.0f, 5.875f);

		bodyModel[474] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[474].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[474].setRotationPoint(-48.0f, -1.0f, 4.875f);

		bodyModel[475] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[475].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[475].setRotationPoint(-48.0f, -1.5f, 7.875f);

		bodyModel[476] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[476].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[476].setRotationPoint(-48.0f, -1.5f, 4.875f);

		bodyModel[477] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[477].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[477].setRotationPoint(-48.0f, -4.5f, 4.875f);

		bodyModel[478] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[478].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[478].setRotationPoint(-47.5f, -4.5f, 7.875f);

		bodyModel[479] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[479].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[479].setRotationPoint(-47.5f, -4.5f, 4.875f);

		bodyModel[480] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[480].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[480].setRotationPoint(-48.25f, -2.25f, 9.875f);

		bodyModel[481] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[481].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[481].setRotationPoint(-48.25f, -2.375f, 9.875f);

		bodyModel[482] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[482].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[482].setRotationPoint(-48.25f, -2.375f, 9.875f);

		bodyModel[483] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[483].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[483].setRotationPoint(-48.25f, -2.25f, 4.5f);

		bodyModel[484] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[484].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[484].setRotationPoint(-48.25f, -2.375f, 4.5f);

		bodyModel[485] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[485].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[485].setRotationPoint(-48.25f, -2.375f, 4.5f);

		bodyModel[486] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[486].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[486].setRotationPoint(-40.5f, 0.0f, -8.875f);

		bodyModel[487] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[487].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[487].setRotationPoint(-41.5f, -1.0f, -9.875f);

		bodyModel[488] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[488].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[488].setRotationPoint(-41.5f, -1.5f, -6.875f);

		bodyModel[489] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[489].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[489].setRotationPoint(-41.5f, -1.5f, -9.875f);

		bodyModel[490] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[490].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[490].setRotationPoint(-41.5f, -4.5f, -9.875f);

		bodyModel[491] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[491].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[491].setRotationPoint(-41.0f, -4.5f, -6.875f);

		bodyModel[492] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[492].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[492].setRotationPoint(-41.0f, -4.5f, -9.875f);

		bodyModel[493] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[493].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[493].setRotationPoint(-41.75f, -2.25f, -4.875f);

		bodyModel[494] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[494].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[494].setRotationPoint(-41.75f, -2.375f, -4.875f);

		bodyModel[495] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[495].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[495].setRotationPoint(-41.75f, -2.375f, -4.875f);

		bodyModel[496] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[496].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[496].setRotationPoint(-41.75f, -2.25f, -10.25f);

		bodyModel[497] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[497].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[497].setRotationPoint(-41.75f, -2.375f, -10.25f);

		bodyModel[498] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[498].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[498].setRotationPoint(-41.75f, -2.375f, -10.25f);

		bodyModel[499] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[499].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[499].setRotationPoint(-40.5f, 0.0f, 5.875f);

	}

	private final void initGroup_bodyModel1(){
		bodyModel[500] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[500].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[500].setRotationPoint(-41.5f, -1.0f, 4.875f);

		bodyModel[501] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[501].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[501].setRotationPoint(-41.5f, -1.5f, 7.875f);

		bodyModel[502] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[502].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[502].setRotationPoint(-41.5f, -1.5f, 4.875f);

		bodyModel[503] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[503].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[503].setRotationPoint(-41.5f, -4.5f, 4.875f);

		bodyModel[504] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[504].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[504].setRotationPoint(-41.0f, -4.5f, 7.875f);

		bodyModel[505] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[505].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[505].setRotationPoint(-41.0f, -4.5f, 4.875f);

		bodyModel[506] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[506].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[506].setRotationPoint(-41.75f, -2.25f, 9.875f);

		bodyModel[507] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[507].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[507].setRotationPoint(-41.75f, -2.375f, 9.875f);

		bodyModel[508] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[508].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[508].setRotationPoint(-41.75f, -2.375f, 9.875f);

		bodyModel[509] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[509].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[509].setRotationPoint(-41.75f, -2.25f, 4.5f);

		bodyModel[510] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[510].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[510].setRotationPoint(-41.75f, -2.375f, 4.5f);

		bodyModel[511] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[511].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[511].setRotationPoint(-41.75f, -2.375f, 4.5f);

		bodyModel[512] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[512].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[512].setRotationPoint(-34.0f, 0.0f, -8.875f);

		bodyModel[513] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[513].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[513].setRotationPoint(-35.0f, -1.0f, -9.875f);

		bodyModel[514] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[514].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[514].setRotationPoint(-35.0f, -1.5f, -6.875f);

		bodyModel[515] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[515].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[515].setRotationPoint(-35.0f, -1.5f, -9.875f);

		bodyModel[516] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[516].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[516].setRotationPoint(-35.0f, -4.5f, -9.875f);

		bodyModel[517] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[517].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[517].setRotationPoint(-34.5f, -4.5f, -6.875f);

		bodyModel[518] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[518].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[518].setRotationPoint(-34.5f, -4.5f, -9.875f);

		bodyModel[519] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[519].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[519].setRotationPoint(-35.25f, -2.25f, -4.875f);

		bodyModel[520] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[520].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[520].setRotationPoint(-35.25f, -2.375f, -4.875f);

		bodyModel[521] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[521].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[521].setRotationPoint(-35.25f, -2.375f, -4.875f);

		bodyModel[522] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[522].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[522].setRotationPoint(-35.25f, -2.25f, -10.25f);

		bodyModel[523] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[523].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[523].setRotationPoint(-35.25f, -2.375f, -10.25f);

		bodyModel[524] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[524].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[524].setRotationPoint(-35.25f, -2.375f, -10.25f);

		bodyModel[525] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[525].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[525].setRotationPoint(-34.0f, 0.0f, 5.875f);

		bodyModel[526] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[526].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[526].setRotationPoint(-35.0f, -1.0f, 4.875f);

		bodyModel[527] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[527].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[527].setRotationPoint(-35.0f, -1.5f, 7.875f);

		bodyModel[528] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[528].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[528].setRotationPoint(-35.0f, -1.5f, 4.875f);

		bodyModel[529] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[529].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[529].setRotationPoint(-35.0f, -4.5f, 4.875f);

		bodyModel[530] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[530].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[530].setRotationPoint(-34.5f, -4.5f, 7.875f);

		bodyModel[531] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[531].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[531].setRotationPoint(-34.5f, -4.5f, 4.875f);

		bodyModel[532] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[532].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[532].setRotationPoint(-35.25f, -2.25f, 9.875f);

		bodyModel[533] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[533].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[533].setRotationPoint(-35.25f, -2.375f, 9.875f);

		bodyModel[534] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[534].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[534].setRotationPoint(-35.25f, -2.375f, 9.875f);

		bodyModel[535] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[535].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[535].setRotationPoint(-35.25f, -2.25f, 4.5f);

		bodyModel[536] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[536].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[536].setRotationPoint(-35.25f, -2.375f, 4.5f);

		bodyModel[537] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[537].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[537].setRotationPoint(-35.25f, -2.375f, 4.5f);

		bodyModel[538] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[538].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[538].setRotationPoint(-27.5f, 0.0f, -8.875f);

		bodyModel[539] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[539].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[539].setRotationPoint(-28.5f, -1.0f, -9.875f);

		bodyModel[540] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[540].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[540].setRotationPoint(-28.5f, -1.5f, -6.875f);

		bodyModel[541] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[541].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[541].setRotationPoint(-28.5f, -1.5f, -9.875f);

		bodyModel[542] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[542].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[542].setRotationPoint(-28.5f, -4.5f, -9.875f);

		bodyModel[543] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[543].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[543].setRotationPoint(-28.0f, -4.5f, -6.875f);

		bodyModel[544] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[544].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[544].setRotationPoint(-28.0f, -4.5f, -9.875f);

		bodyModel[545] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[545].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[545].setRotationPoint(-28.75f, -2.25f, -4.875f);

		bodyModel[546] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[546].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[546].setRotationPoint(-28.75f, -2.375f, -4.875f);

		bodyModel[547] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[547].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[547].setRotationPoint(-28.75f, -2.375f, -4.875f);

		bodyModel[548] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[548].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[548].setRotationPoint(-28.75f, -2.25f, -10.25f);

		bodyModel[549] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[549].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[549].setRotationPoint(-28.75f, -2.375f, -10.25f);

		bodyModel[550] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[550].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[550].setRotationPoint(-28.75f, -2.375f, -10.25f);

		bodyModel[551] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[551].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[551].setRotationPoint(-27.5f, 0.0f, 5.875f);

		bodyModel[552] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[552].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[552].setRotationPoint(-28.5f, -1.0f, 4.875f);

		bodyModel[553] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[553].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[553].setRotationPoint(-28.5f, -1.5f, 7.875f);

		bodyModel[554] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[554].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[554].setRotationPoint(-28.5f, -1.5f, 4.875f);

		bodyModel[555] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[555].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[555].setRotationPoint(-28.5f, -4.5f, 4.875f);

		bodyModel[556] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[556].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[556].setRotationPoint(-28.0f, -4.5f, 7.875f);

		bodyModel[557] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[557].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[557].setRotationPoint(-28.0f, -4.5f, 4.875f);

		bodyModel[558] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[558].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[558].setRotationPoint(-28.75f, -2.25f, 9.875f);

		bodyModel[559] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[559].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[559].setRotationPoint(-28.75f, -2.375f, 9.875f);

		bodyModel[560] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[560].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[560].setRotationPoint(-28.75f, -2.375f, 9.875f);

		bodyModel[561] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[561].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[561].setRotationPoint(-28.75f, -2.25f, 4.5f);

		bodyModel[562] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[562].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[562].setRotationPoint(-28.75f, -2.375f, 4.5f);

		bodyModel[563] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[563].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[563].setRotationPoint(-28.75f, -2.375f, 4.5f);

		bodyModel[564] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[564].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[564].setRotationPoint(-21.0f, 0.0f, -8.875f);

		bodyModel[565] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[565].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[565].setRotationPoint(-22.0f, -1.0f, -9.875f);

		bodyModel[566] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[566].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[566].setRotationPoint(-22.0f, -1.5f, -6.875f);

		bodyModel[567] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[567].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[567].setRotationPoint(-22.0f, -1.5f, -9.875f);

		bodyModel[568] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[568].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[568].setRotationPoint(-22.0f, -4.5f, -9.875f);

		bodyModel[569] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[569].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[569].setRotationPoint(-21.5f, -4.5f, -6.875f);

		bodyModel[570] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[570].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[570].setRotationPoint(-21.5f, -4.5f, -9.875f);

		bodyModel[571] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[571].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[571].setRotationPoint(-22.25f, -2.25f, -4.875f);

		bodyModel[572] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[572].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[572].setRotationPoint(-22.25f, -2.375f, -4.875f);

		bodyModel[573] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[573].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[573].setRotationPoint(-22.25f, -2.375f, -4.875f);

		bodyModel[574] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[574].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[574].setRotationPoint(-22.25f, -2.25f, -10.25f);

		bodyModel[575] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[575].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[575].setRotationPoint(-22.25f, -2.375f, -10.25f);

		bodyModel[576] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[576].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[576].setRotationPoint(-22.25f, -2.375f, -10.25f);

		bodyModel[577] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[577].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[577].setRotationPoint(-21.0f, 0.0f, 5.875f);

		bodyModel[578] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[578].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[578].setRotationPoint(-22.0f, -1.0f, 4.875f);

		bodyModel[579] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[579].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[579].setRotationPoint(-22.0f, -1.5f, 7.875f);

		bodyModel[580] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[580].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[580].setRotationPoint(-22.0f, -1.5f, 4.875f);

		bodyModel[581] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[581].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[581].setRotationPoint(-22.0f, -4.5f, 4.875f);

		bodyModel[582] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[582].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[582].setRotationPoint(-21.5f, -4.5f, 7.875f);

		bodyModel[583] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[583].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[583].setRotationPoint(-21.5f, -4.5f, 4.875f);

		bodyModel[584] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[584].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[584].setRotationPoint(-22.25f, -2.25f, 9.875f);

		bodyModel[585] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[585].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[585].setRotationPoint(-22.25f, -2.375f, 9.875f);

		bodyModel[586] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[586].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[586].setRotationPoint(-22.25f, -2.375f, 9.875f);

		bodyModel[587] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[587].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[587].setRotationPoint(-22.25f, -2.25f, 4.5f);

		bodyModel[588] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[588].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[588].setRotationPoint(-22.25f, -2.375f, 4.5f);

		bodyModel[589] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[589].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[589].setRotationPoint(-22.25f, -2.375f, 4.5f);

		bodyModel[590] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[590].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[590].setRotationPoint(-14.5f, 0.0f, -8.875f);

		bodyModel[591] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[591].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[591].setRotationPoint(-15.5f, -1.0f, -9.875f);

		bodyModel[592] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[592].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[592].setRotationPoint(-15.5f, -1.5f, -6.875f);

		bodyModel[593] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[593].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[593].setRotationPoint(-15.5f, -1.5f, -9.875f);

		bodyModel[594] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[594].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[594].setRotationPoint(-15.5f, -4.5f, -9.875f);

		bodyModel[595] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[595].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[595].setRotationPoint(-15.0f, -4.5f, -6.875f);

		bodyModel[596] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[596].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[596].setRotationPoint(-15.0f, -4.5f, -9.875f);

		bodyModel[597] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[597].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[597].setRotationPoint(-15.75f, -2.25f, -4.875f);

		bodyModel[598] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[598].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[598].setRotationPoint(-15.75f, -2.375f, -4.875f);

		bodyModel[599] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[599].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[599].setRotationPoint(-15.75f, -2.375f, -4.875f);

		bodyModel[600] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[600].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[600].setRotationPoint(-15.75f, -2.25f, -10.25f);

		bodyModel[601] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[601].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[601].setRotationPoint(-15.75f, -2.375f, -10.25f);

		bodyModel[602] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[602].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[602].setRotationPoint(-15.75f, -2.375f, -10.25f);

		bodyModel[603] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[603].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[603].setRotationPoint(-14.5f, 0.0f, 5.875f);

		bodyModel[604] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[604].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[604].setRotationPoint(-15.5f, -1.0f, 4.875f);

		bodyModel[605] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[605].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[605].setRotationPoint(-15.5f, -1.5f, 7.875f);

		bodyModel[606] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[606].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[606].setRotationPoint(-15.5f, -1.5f, 4.875f);

		bodyModel[607] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[607].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[607].setRotationPoint(-15.5f, -4.5f, 4.875f);

		bodyModel[608] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[608].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[608].setRotationPoint(-15.0f, -4.5f, 7.875f);

		bodyModel[609] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[609].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[609].setRotationPoint(-15.0f, -4.5f, 4.875f);

		bodyModel[610] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[610].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[610].setRotationPoint(-15.75f, -2.25f, 9.875f);

		bodyModel[611] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[611].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[611].setRotationPoint(-15.75f, -2.375f, 9.875f);

		bodyModel[612] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[612].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[612].setRotationPoint(-15.75f, -2.375f, 9.875f);

		bodyModel[613] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[613].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[613].setRotationPoint(-15.75f, -2.25f, 4.5f);

		bodyModel[614] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[614].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[614].setRotationPoint(-15.75f, -2.375f, 4.5f);

		bodyModel[615] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[615].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[615].setRotationPoint(-15.75f, -2.375f, 4.5f);

		bodyModel[616] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[616].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[616].setRotationPoint(-8.0f, 0.0f, -8.875f);

		bodyModel[617] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[617].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[617].setRotationPoint(-9.0f, -1.0f, -9.875f);

		bodyModel[618] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[618].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[618].setRotationPoint(-9.0f, -1.5f, -6.875f);

		bodyModel[619] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[619].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[619].setRotationPoint(-9.0f, -1.5f, -9.875f);

		bodyModel[620] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[620].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[620].setRotationPoint(-9.0f, -4.5f, -9.875f);

		bodyModel[621] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[621].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[621].setRotationPoint(-8.5f, -4.5f, -6.875f);

		bodyModel[622] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[622].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[622].setRotationPoint(-8.5f, -4.5f, -9.875f);

		bodyModel[623] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[623].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[623].setRotationPoint(-9.25f, -2.25f, -4.875f);

		bodyModel[624] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[624].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[624].setRotationPoint(-9.25f, -2.375f, -4.875f);

		bodyModel[625] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[625].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[625].setRotationPoint(-9.25f, -2.375f, -4.875f);

		bodyModel[626] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[626].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[626].setRotationPoint(-9.25f, -2.25f, -10.25f);

		bodyModel[627] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[627].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[627].setRotationPoint(-9.25f, -2.375f, -10.25f);

		bodyModel[628] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[628].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[628].setRotationPoint(-9.25f, -2.375f, -10.25f);

		bodyModel[629] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[629].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[629].setRotationPoint(-8.0f, 0.0f, 5.875f);

		bodyModel[630] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[630].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[630].setRotationPoint(-9.0f, -1.0f, 4.875f);

		bodyModel[631] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[631].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[631].setRotationPoint(-9.0f, -1.5f, 7.875f);

		bodyModel[632] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[632].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[632].setRotationPoint(-9.0f, -1.5f, 4.875f);

		bodyModel[633] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[633].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[633].setRotationPoint(-9.0f, -4.5f, 4.875f);

		bodyModel[634] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[634].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[634].setRotationPoint(-8.5f, -4.5f, 7.875f);

		bodyModel[635] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[635].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[635].setRotationPoint(-8.5f, -4.5f, 4.875f);

		bodyModel[636] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[636].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[636].setRotationPoint(-9.25f, -2.25f, 9.875f);

		bodyModel[637] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[637].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[637].setRotationPoint(-9.25f, -2.375f, 9.875f);

		bodyModel[638] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[638].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[638].setRotationPoint(-9.25f, -2.375f, 9.875f);

		bodyModel[639] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[639].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[639].setRotationPoint(-9.25f, -2.25f, 4.5f);

		bodyModel[640] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[640].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[640].setRotationPoint(-9.25f, -2.375f, 4.5f);

		bodyModel[641] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[641].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[641].setRotationPoint(-9.25f, -2.375f, 4.5f);

		bodyModel[642] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[642].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[642].setRotationPoint(-1.5f, 0.0f, -8.875f);

		bodyModel[643] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[643].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[643].setRotationPoint(-2.5f, -1.0f, -9.875f);

		bodyModel[644] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[644].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[644].setRotationPoint(-2.5f, -1.5f, -6.875f);

		bodyModel[645] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[645].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[645].setRotationPoint(-2.5f, -1.5f, -9.875f);

		bodyModel[646] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[646].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[646].setRotationPoint(-2.5f, -4.5f, -9.875f);

		bodyModel[647] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[647].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[647].setRotationPoint(-2.0f, -4.5f, -6.875f);

		bodyModel[648] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[648].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[648].setRotationPoint(-2.0f, -4.5f, -9.875f);

		bodyModel[649] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[649].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[649].setRotationPoint(-2.75f, -2.25f, -4.875f);

		bodyModel[650] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[650].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[650].setRotationPoint(-2.75f, -2.375f, -4.875f);

		bodyModel[651] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[651].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[651].setRotationPoint(-2.75f, -2.375f, -4.875f);

		bodyModel[652] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[652].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[652].setRotationPoint(-2.75f, -2.25f, -10.25f);

		bodyModel[653] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[653].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[653].setRotationPoint(-2.75f, -2.375f, -10.25f);

		bodyModel[654] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[654].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[654].setRotationPoint(-2.75f, -2.375f, -10.25f);

		bodyModel[655] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[655].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[655].setRotationPoint(-1.5f, 0.0f, 5.875f);

		bodyModel[656] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[656].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[656].setRotationPoint(-2.5f, -1.0f, 4.875f);

		bodyModel[657] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[657].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[657].setRotationPoint(-2.5f, -1.5f, 7.875f);

		bodyModel[658] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[658].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[658].setRotationPoint(-2.5f, -1.5f, 4.875f);

		bodyModel[659] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[659].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[659].setRotationPoint(-2.5f, -4.5f, 4.875f);

		bodyModel[660] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[660].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[660].setRotationPoint(-2.0f, -4.5f, 7.875f);

		bodyModel[661] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[661].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[661].setRotationPoint(-2.0f, -4.5f, 4.875f);

		bodyModel[662] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[662].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[662].setRotationPoint(-2.75f, -2.25f, 9.875f);

		bodyModel[663] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[663].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[663].setRotationPoint(-2.75f, -2.375f, 9.875f);

		bodyModel[664] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[664].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[664].setRotationPoint(-2.75f, -2.375f, 9.875f);

		bodyModel[665] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[665].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[665].setRotationPoint(-2.75f, -2.25f, 4.5f);

		bodyModel[666] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[666].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[666].setRotationPoint(-2.75f, -2.375f, 4.5f);

		bodyModel[667] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[667].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[667].setRotationPoint(-2.75f, -2.375f, 4.5f);

		bodyModel[668] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[668].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[668].setRotationPoint(5.0f, 0.0f, -8.875f);

		bodyModel[669] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[669].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[669].setRotationPoint(4.0f, -1.0f, -9.875f);

		bodyModel[670] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[670].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[670].setRotationPoint(4.0f, -1.5f, -6.875f);

		bodyModel[671] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[671].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[671].setRotationPoint(4.0f, -1.5f, -9.875f);

		bodyModel[672] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[672].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[672].setRotationPoint(4.0f, -4.5f, -9.875f);

		bodyModel[673] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[673].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[673].setRotationPoint(4.5f, -4.5f, -6.875f);

		bodyModel[674] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[674].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[674].setRotationPoint(4.5f, -4.5f, -9.875f);

		bodyModel[675] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[675].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[675].setRotationPoint(3.75f, -2.25f, -4.875f);

		bodyModel[676] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[676].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[676].setRotationPoint(3.75f, -2.375f, -4.875f);

		bodyModel[677] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[677].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[677].setRotationPoint(3.75f, -2.375f, -4.875f);

		bodyModel[678] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[678].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[678].setRotationPoint(3.75f, -2.25f, -10.25f);

		bodyModel[679] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[679].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[679].setRotationPoint(3.75f, -2.375f, -10.25f);

		bodyModel[680] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[680].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[680].setRotationPoint(3.75f, -2.375f, -10.25f);

		bodyModel[681] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[681].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[681].setRotationPoint(5.0f, 0.0f, 5.875f);

		bodyModel[682] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[682].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[682].setRotationPoint(4.0f, -1.0f, 4.875f);

		bodyModel[683] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[683].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[683].setRotationPoint(4.0f, -1.5f, 7.875f);

		bodyModel[684] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[684].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[684].setRotationPoint(4.0f, -1.5f, 4.875f);

		bodyModel[685] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[685].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[685].setRotationPoint(4.0f, -4.5f, 4.875f);

		bodyModel[686] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[686].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[686].setRotationPoint(4.5f, -4.5f, 7.875f);

		bodyModel[687] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[687].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[687].setRotationPoint(4.5f, -4.5f, 4.875f);

		bodyModel[688] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[688].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[688].setRotationPoint(3.75f, -2.25f, 9.875f);

		bodyModel[689] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[689].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[689].setRotationPoint(3.75f, -2.375f, 9.875f);

		bodyModel[690] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[690].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[690].setRotationPoint(3.75f, -2.375f, 9.875f);

		bodyModel[691] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[691].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[691].setRotationPoint(3.75f, -2.25f, 4.5f);

		bodyModel[692] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[692].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[692].setRotationPoint(3.75f, -2.375f, 4.5f);

		bodyModel[693] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[693].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[693].setRotationPoint(3.75f, -2.375f, 4.5f);

		bodyModel[694] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[694].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[694].setRotationPoint(11.5f, 0.0f, -8.875f);

		bodyModel[695] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[695].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[695].setRotationPoint(10.5f, -1.0f, -9.875f);

		bodyModel[696] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[696].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[696].setRotationPoint(10.5f, -1.5f, -6.875f);

		bodyModel[697] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[697].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[697].setRotationPoint(10.5f, -1.5f, -9.875f);

		bodyModel[698] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[698].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[698].setRotationPoint(10.5f, -4.5f, -9.875f);

		bodyModel[699] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[699].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[699].setRotationPoint(11.0f, -4.5f, -6.875f);

		bodyModel[700] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[700].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[700].setRotationPoint(11.0f, -4.5f, -9.875f);

		bodyModel[701] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[701].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[701].setRotationPoint(10.25f, -2.25f, -4.875f);

		bodyModel[702] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[702].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[702].setRotationPoint(10.25f, -2.375f, -4.875f);

		bodyModel[703] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[703].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[703].setRotationPoint(10.25f, -2.375f, -4.875f);

		bodyModel[704] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[704].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[704].setRotationPoint(10.25f, -2.25f, -10.25f);

		bodyModel[705] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[705].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[705].setRotationPoint(10.25f, -2.375f, -10.25f);

		bodyModel[706] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[706].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[706].setRotationPoint(10.25f, -2.375f, -10.25f);

		bodyModel[707] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[707].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[707].setRotationPoint(11.5f, 0.0f, 5.875f);

		bodyModel[708] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[708].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[708].setRotationPoint(10.5f, -1.0f, 4.875f);

		bodyModel[709] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[709].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[709].setRotationPoint(10.5f, -1.5f, 7.875f);

		bodyModel[710] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[710].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[710].setRotationPoint(10.5f, -1.5f, 4.875f);

		bodyModel[711] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[711].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[711].setRotationPoint(10.5f, -4.5f, 4.875f);

		bodyModel[712] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[712].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[712].setRotationPoint(11.0f, -4.5f, 7.875f);

		bodyModel[713] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[713].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[713].setRotationPoint(11.0f, -4.5f, 4.875f);

		bodyModel[714] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[714].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[714].setRotationPoint(10.25f, -2.25f, 9.875f);

		bodyModel[715] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[715].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[715].setRotationPoint(10.25f, -2.375f, 9.875f);

		bodyModel[716] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[716].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[716].setRotationPoint(10.25f, -2.375f, 9.875f);

		bodyModel[717] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[717].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[717].setRotationPoint(10.25f, -2.25f, 4.5f);

		bodyModel[718] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[718].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[718].setRotationPoint(10.25f, -2.375f, 4.5f);

		bodyModel[719] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[719].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[719].setRotationPoint(10.25f, -2.375f, 4.5f);

		bodyModel[720] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[720].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[720].setRotationPoint(18.0f, 0.0f, -8.875f);

		bodyModel[721] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[721].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[721].setRotationPoint(17.0f, -1.0f, -9.875f);

		bodyModel[722] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[722].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[722].setRotationPoint(17.0f, -1.5f, -6.875f);

		bodyModel[723] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[723].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[723].setRotationPoint(17.0f, -1.5f, -9.875f);

		bodyModel[724] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[724].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[724].setRotationPoint(17.0f, -4.5f, -9.875f);

		bodyModel[725] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[725].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[725].setRotationPoint(17.5f, -4.5f, -6.875f);

		bodyModel[726] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[726].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[726].setRotationPoint(17.5f, -4.5f, -9.875f);

		bodyModel[727] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[727].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[727].setRotationPoint(16.75f, -2.25f, -4.875f);

		bodyModel[728] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[728].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[728].setRotationPoint(16.75f, -2.375f, -4.875f);

		bodyModel[729] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[729].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[729].setRotationPoint(16.75f, -2.375f, -4.875f);

		bodyModel[730] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[730].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[730].setRotationPoint(16.75f, -2.25f, -10.25f);

		bodyModel[731] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[731].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[731].setRotationPoint(16.75f, -2.375f, -10.25f);

		bodyModel[732] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[732].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[732].setRotationPoint(16.75f, -2.375f, -10.25f);

		bodyModel[733] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[733].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[733].setRotationPoint(18.0f, 0.0f, 5.875f);

		bodyModel[734] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[734].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[734].setRotationPoint(17.0f, -1.0f, 4.875f);

		bodyModel[735] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[735].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[735].setRotationPoint(17.0f, -1.5f, 7.875f);

		bodyModel[736] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[736].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[736].setRotationPoint(17.0f, -1.5f, 4.875f);

		bodyModel[737] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[737].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[737].setRotationPoint(17.0f, -4.5f, 4.875f);

		bodyModel[738] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[738].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[738].setRotationPoint(17.5f, -4.5f, 7.875f);

		bodyModel[739] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[739].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[739].setRotationPoint(17.5f, -4.5f, 4.875f);

		bodyModel[740] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[740].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[740].setRotationPoint(16.75f, -2.25f, 9.875f);

		bodyModel[741] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[741].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[741].setRotationPoint(16.75f, -2.375f, 9.875f);

		bodyModel[742] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[742].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[742].setRotationPoint(16.75f, -2.375f, 9.875f);

		bodyModel[743] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[743].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[743].setRotationPoint(16.75f, -2.25f, 4.5f);

		bodyModel[744] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[744].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[744].setRotationPoint(16.75f, -2.375f, 4.5f);

		bodyModel[745] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[745].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[745].setRotationPoint(16.75f, -2.375f, 4.5f);

		bodyModel[746] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[746].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[746].setRotationPoint(24.5f, 0.0f, -8.875f);

		bodyModel[747] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[747].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[747].setRotationPoint(23.5f, -1.0f, -9.875f);

		bodyModel[748] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[748].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[748].setRotationPoint(23.5f, -1.5f, -6.875f);

		bodyModel[749] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[749].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[749].setRotationPoint(23.5f, -1.5f, -9.875f);

		bodyModel[750] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[750].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[750].setRotationPoint(23.5f, -4.5f, -9.875f);

		bodyModel[751] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[751].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[751].setRotationPoint(24.0f, -4.5f, -6.875f);

		bodyModel[752] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[752].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[752].setRotationPoint(24.0f, -4.5f, -9.875f);

		bodyModel[753] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[753].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[753].setRotationPoint(23.25f, -2.25f, -4.875f);

		bodyModel[754] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[754].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[754].setRotationPoint(23.25f, -2.375f, -4.875f);

		bodyModel[755] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[755].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[755].setRotationPoint(23.25f, -2.375f, -4.875f);

		bodyModel[756] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[756].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[756].setRotationPoint(23.25f, -2.25f, -10.25f);

		bodyModel[757] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[757].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[757].setRotationPoint(23.25f, -2.375f, -10.25f);

		bodyModel[758] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[758].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[758].setRotationPoint(23.25f, -2.375f, -10.25f);

		bodyModel[759] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[759].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[759].setRotationPoint(24.5f, 0.0f, 5.875f);

		bodyModel[760] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[760].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[760].setRotationPoint(23.5f, -1.0f, 4.875f);

		bodyModel[761] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[761].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[761].setRotationPoint(23.5f, -1.5f, 7.875f);

		bodyModel[762] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[762].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[762].setRotationPoint(23.5f, -1.5f, 4.875f);

		bodyModel[763] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[763].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[763].setRotationPoint(23.5f, -4.5f, 4.875f);

		bodyModel[764] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[764].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[764].setRotationPoint(24.0f, -4.5f, 7.875f);

		bodyModel[765] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[765].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[765].setRotationPoint(24.0f, -4.5f, 4.875f);

		bodyModel[766] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[766].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[766].setRotationPoint(23.25f, -2.25f, 9.875f);

		bodyModel[767] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[767].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[767].setRotationPoint(23.25f, -2.375f, 9.875f);

		bodyModel[768] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[768].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[768].setRotationPoint(23.25f, -2.375f, 9.875f);

		bodyModel[769] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[769].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[769].setRotationPoint(23.25f, -2.25f, 4.5f);

		bodyModel[770] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[770].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[770].setRotationPoint(23.25f, -2.375f, 4.5f);

		bodyModel[771] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[771].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[771].setRotationPoint(23.25f, -2.375f, 4.5f);

		bodyModel[772] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[772].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[772].setRotationPoint(31.0f, 0.0f, -8.875f);

		bodyModel[773] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[773].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[773].setRotationPoint(30.0f, -1.0f, -9.875f);

		bodyModel[774] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[774].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[774].setRotationPoint(30.0f, -1.5f, -6.875f);

		bodyModel[775] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[775].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[775].setRotationPoint(30.0f, -1.5f, -9.875f);

		bodyModel[776] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[776].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[776].setRotationPoint(30.0f, -4.5f, -9.875f);

		bodyModel[777] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[777].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[777].setRotationPoint(30.5f, -4.5f, -6.875f);

		bodyModel[778] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[778].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[778].setRotationPoint(30.5f, -4.5f, -9.875f);

		bodyModel[779] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[779].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[779].setRotationPoint(29.75f, -2.25f, -4.875f);

		bodyModel[780] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[780].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[780].setRotationPoint(29.75f, -2.375f, -4.875f);

		bodyModel[781] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[781].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[781].setRotationPoint(29.75f, -2.375f, -4.875f);

		bodyModel[782] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[782].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[782].setRotationPoint(29.75f, -2.25f, -10.25f);

		bodyModel[783] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[783].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[783].setRotationPoint(29.75f, -2.375f, -10.25f);

		bodyModel[784] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[784].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[784].setRotationPoint(29.75f, -2.375f, -10.25f);

		bodyModel[785] = new ModelRendererTurbo(this, 376, 196, textureX, textureY);
		bodyModel[785].addShapeBox(0, 0, 0, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[785].setRotationPoint(31.0f, 0.0f, 5.875f);

		bodyModel[786] = new ModelRendererTurbo(this, 347, 156, textureX, textureY);
		bodyModel[786].addShapeBox(0, 0, 0, 4, 1, 5, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0);
		bodyModel[786].setRotationPoint(30.0f, -1.0f, 4.875f);

		bodyModel[787] = new ModelRendererTurbo(this, 49, 173, textureX, textureY);
		bodyModel[787].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -1, 0, 0, -1, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[787].setRotationPoint(30.0f, -1.5f, 7.875f);

		bodyModel[788] = new ModelRendererTurbo(this, 387, 172, textureX, textureY);
		bodyModel[788].addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, -0.9375f, -0.25f, 0, -0.9375f, -0.25f, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[788].setRotationPoint(30.0f, -1.5f, 4.875f);

		bodyModel[789] = new ModelRendererTurbo(this, 121, 183, textureX, textureY);
		bodyModel[789].addShapeBox(0, 0, 0, 1, 4, 5, 0, 1, 0, 0, -1.5f, 0, 0, -1.5f, 0, 0, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[789].setRotationPoint(30.0f, -4.5f, 4.875f);

		bodyModel[790] = new ModelRendererTurbo(this, 358, 214, textureX, textureY);
		bodyModel[790].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -2, 0, 0, -1.8125f, -0.125f, -0.25f, 1, 0, 0, 0, 0, 0, -1, 0, 0, -0.8125f, 0, -0.25f, 0, 0, 0);
		bodyModel[790].setRotationPoint(30.5f, -4.5f, 7.875f);

		bodyModel[791] = new ModelRendererTurbo(this, 330, 214, textureX, textureY);
		bodyModel[791].addShapeBox(0, 0, 0, 1, 4, 2, 0, 1, 0, 0, -1.8125f, -0.125f, -0.25f, -2, 0, 0, 1, 0, 0, 0, 0, 0, -0.8125f, 0, -0.25f, -1, 0, 0, 0, 0, 0);
		bodyModel[791].setRotationPoint(30.5f, -4.5f, 4.875f);

		bodyModel[792] = new ModelRendererTurbo(this, 300, 211, textureX, textureY);
		bodyModel[792].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[792].setRotationPoint(29.75f, -2.25f, 9.875f);

		bodyModel[793] = new ModelRendererTurbo(this, 291, 211, textureX, textureY);
		bodyModel[793].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[793].setRotationPoint(29.75f, -2.375f, 9.875f);

		bodyModel[794] = new ModelRendererTurbo(this, 266, 211, textureX, textureY);
		bodyModel[794].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[794].setRotationPoint(29.75f, -2.375f, 9.875f);

		bodyModel[795] = new ModelRendererTurbo(this, 257, 211, textureX, textureY);
		bodyModel[795].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.25f, 0, 0.25f, -0.25f, 0, 0.25f, -0.25f, -0.625f, 0, -0.25f, -0.625f);
		bodyModel[795].setRotationPoint(29.75f, -2.25f, 4.5f);

		bodyModel[796] = new ModelRendererTurbo(this, 248, 211, textureX, textureY);
		bodyModel[796].addShapeBox(0, 0, 0, 3, 1, 1, 0, -2, 0, 0, -0.625f, 0, 0, -0.625f, 0, -0.625f, -2, 0, -0.625f, 0, -0.875f, 0, 0, -0.875f, 0, 0, -0.875f, -0.625f, 0, -0.875f, -0.625f);
		bodyModel[796].setRotationPoint(29.75f, -2.375f, 4.5f);

		bodyModel[797] = new ModelRendererTurbo(this, 186, 211, textureX, textureY);
		bodyModel[797].addShapeBox(0, 0, 0, 3, 1, 1, 0, 0, -0.875f, 0, 0.25f, -0.875f, 0, 0.25f, -0.875f, -0.625f, 0, -0.875f, -0.625f, -0.625f, 0, 0, -1.875f, 0, 0, -1.875f, 0, -0.625f, -0.625f, 0, -0.625f);
		bodyModel[797].setRotationPoint(29.75f, -2.375f, 4.5f);

		bodyModel[798] = new ModelRendererTurbo(this, 0, 0, textureX, textureY);
		bodyModel[798].addShapeBox(0, 0, 0, 7, 2, 3, 0, 0, 0.6875f, -0.34375f, 2.875f, -2, -0.34375f, 2.875f, -2, -0.34375f, 0, 0.6875f, -0.34375f, 0, 0, -0.34375f, 2.875f, 0, -0.34375f, 2.875f, 0, -0.34375f, 0, 0, -0.34375f);
		bodyModel[798].setRotationPoint(-73.9375f, -15.9375f, -1.5f);

		bodyModel[799] = new ModelRendererTurbo(this, 425, 6, textureX, textureY);
		bodyModel[799].addShapeBox(0, 0, 0, 1, 1, 1, 0, -0.375f, -0.25f, -0.25f, -0.3125f, -0.25f, -0.25f, -0.3125f, -0.25f, -0.25f, -0.375f, -0.25f, -0.25f, -0.375f, -0.25f, -0.25f, -0.3125f, -0.25f, -0.25f, -0.3125f, -0.25f, -0.25f, -0.375f, -0.25f, -0.25f);
		bodyModel[799].setRotationPoint(-76.65625f, -16.796875f, -0.5f);

		bodyModel[800] = new ModelRendererTurbo(this, 0, 0, textureX, textureY);
		bodyModel[800].addShapeBox(0, 0, 0, 0, 1, 1, 0, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f, 0, -0.25f, -0.25f);
		bodyModel[800].setRotationPoint(-76.28125f, -17.296875f, -0.5f);

		bodyModel[801] = new ModelRendererTurbo(this, 0, 6, textureX, textureY);
		bodyModel[801].addShapeBox(0, 0, 0, 1, 0, 1, 0, -0.3125f, 0, -0.25f, -0.375f, 0, -0.25f, -0.375f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.3125f, 0, -0.25f, -0.375f, 0, -0.25f, -0.375f, 0, -0.25f, -0.3125f, 0, -0.25f);
		bodyModel[801].setRotationPoint(-76.59375f, -17.046875f, -0.5f);

	}

}
