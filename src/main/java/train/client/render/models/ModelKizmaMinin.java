//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 24.12.2021 - 14:48:48
// Last changed on: 24.12.2021 - 14:48:48

package train.client.render.models; //Path where the model is located


import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.client.render.RenderRollingStock;
import train.common.api.AbstractTrains;
import train.common.entity.rollingStock.PassengerPL42;
import train.common.library.Info;

public class ModelKizmaMinin extends ModelConverter
{
	int textureX = 512;
	int textureY = 128;

	public ModelKizmaMinin() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[114];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 14
		bodyModel[1] = new ModelRendererTurbo(this, 113, 1, textureX, textureY); // Box 15
		bodyModel[2] = new ModelRendererTurbo(this, 209, 1, textureX, textureY); // Box 16
		bodyModel[3] = new ModelRendererTurbo(this, 281, 1, textureX, textureY); // Box 17
		bodyModel[4] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Box 18
		bodyModel[5] = new ModelRendererTurbo(this, 257, 1, textureX, textureY); // Box 19
		bodyModel[6] = new ModelRendererTurbo(this, 273, 1, textureX, textureY); // Box 21
		bodyModel[7] = new ModelRendererTurbo(this, 409, 1, textureX, textureY); // Box 23
		bodyModel[8] = new ModelRendererTurbo(this, 425, 1, textureX, textureY); // Box 24
		bodyModel[9] = new ModelRendererTurbo(this, 465, 1, textureX, textureY); // Box 26
		bodyModel[10] = new ModelRendererTurbo(this, 465, 9, textureX, textureY); // Box 27
		bodyModel[11] = new ModelRendererTurbo(this, 129, 17, textureX, textureY); // Box 28
		bodyModel[12] = new ModelRendererTurbo(this, 161, 17, textureX, textureY); // Box 29
		bodyModel[13] = new ModelRendererTurbo(this, 425, 17, textureX, textureY); // Box 30
		bodyModel[14] = new ModelRendererTurbo(this, 193, 17, textureX, textureY); // Box 28
		bodyModel[15] = new ModelRendererTurbo(this, 449, 17, textureX, textureY); // Box 29
		bodyModel[16] = new ModelRendererTurbo(this, 257, 17, textureX, textureY); // Box 30
		bodyModel[17] = new ModelRendererTurbo(this, 281, 25, textureX, textureY); // Box 31
		bodyModel[18] = new ModelRendererTurbo(this, 257, 9, textureX, textureY); // Box 32
		bodyModel[19] = new ModelRendererTurbo(this, 409, 9, textureX, textureY); // Box 33
		bodyModel[20] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Box 35
		bodyModel[21] = new ModelRendererTurbo(this, 505, 1, textureX, textureY); // Box 36
		bodyModel[22] = new ModelRendererTurbo(this, 465, 17, textureX, textureY); // Box 39
		bodyModel[23] = new ModelRendererTurbo(this, 481, 17, textureX, textureY); // Box 40
		bodyModel[24] = new ModelRendererTurbo(this, 497, 9, textureX, textureY); // Box 45
		bodyModel[25] = new ModelRendererTurbo(this, 497, 17, textureX, textureY); // Box 46
		bodyModel[26] = new ModelRendererTurbo(this, 289, 1, textureX, textureY); // Box 47
		bodyModel[27] = new ModelRendererTurbo(this, 201, 17, textureX, textureY); // Box 48
		bodyModel[28] = new ModelRendererTurbo(this, 441, 17, textureX, textureY); // Box 49
		bodyModel[29] = new ModelRendererTurbo(this, 1, 25, textureX, textureY); // Box 50
		bodyModel[30] = new ModelRendererTurbo(this, 337, 25, textureX, textureY); // Box 44
		bodyModel[31] = new ModelRendererTurbo(this, 353, 25, textureX, textureY); // Box 45
		bodyModel[32] = new ModelRendererTurbo(this, 257, 1, textureX, textureY); // Box 46
		bodyModel[33] = new ModelRendererTurbo(this, 273, 1, textureX, textureY); // Box 47
		bodyModel[34] = new ModelRendererTurbo(this, 409, 1, textureX, textureY); // Box 49
		bodyModel[35] = new ModelRendererTurbo(this, 425, 1, textureX, textureY); // Box 50
		bodyModel[36] = new ModelRendererTurbo(this, 465, 1, textureX, textureY); // Box 51
		bodyModel[37] = new ModelRendererTurbo(this, 497, 1, textureX, textureY); // Box 52
		bodyModel[38] = new ModelRendererTurbo(this, 257, 9, textureX, textureY); // Box 53
		bodyModel[39] = new ModelRendererTurbo(this, 329, 25, textureX, textureY); // Box 54
		bodyModel[40] = new ModelRendererTurbo(this, 369, 25, textureX, textureY); // Box 55
		bodyModel[41] = new ModelRendererTurbo(this, 385, 25, textureX, textureY); // Box 56
		bodyModel[42] = new ModelRendererTurbo(this, 393, 25, textureX, textureY); // Box 57
		bodyModel[43] = new ModelRendererTurbo(this, 217, 17, textureX, textureY); // Box 179
		bodyModel[44] = new ModelRendererTurbo(this, 281, 17, textureX, textureY); // Box 59
		bodyModel[45] = new ModelRendererTurbo(this, 409, 17, textureX, textureY); // Box 60
		bodyModel[46] = new ModelRendererTurbo(this, 457, 17, textureX, textureY); // Box 61
		bodyModel[47] = new ModelRendererTurbo(this, 401, 25, textureX, textureY); // Box 62
		bodyModel[48] = new ModelRendererTurbo(this, 465, 25, textureX, textureY); // Box 63
		bodyModel[49] = new ModelRendererTurbo(this, 481, 25, textureX, textureY); // Box 64
		bodyModel[50] = new ModelRendererTurbo(this, 489, 25, textureX, textureY); // Box 65
		bodyModel[51] = new ModelRendererTurbo(this, 497, 25, textureX, textureY); // Box 66
		bodyModel[52] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Box 67
		bodyModel[53] = new ModelRendererTurbo(this, 9, 33, textureX, textureY); // Box 68
		bodyModel[54] = new ModelRendererTurbo(this, 209, 33, textureX, textureY); // Box 69
		bodyModel[55] = new ModelRendererTurbo(this, 217, 33, textureX, textureY); // Box 70
		bodyModel[56] = new ModelRendererTurbo(this, 225, 33, textureX, textureY); // Box 71
		bodyModel[57] = new ModelRendererTurbo(this, 233, 33, textureX, textureY); // Box 72
		bodyModel[58] = new ModelRendererTurbo(this, 241, 33, textureX, textureY); // Box 73
		bodyModel[59] = new ModelRendererTurbo(this, 257, 33, textureX, textureY); // Box 74
		bodyModel[60] = new ModelRendererTurbo(this, 337, 33, textureX, textureY); // Box 75
		bodyModel[61] = new ModelRendererTurbo(this, 353, 33, textureX, textureY); // Box 158
		bodyModel[62] = new ModelRendererTurbo(this, 369, 33, textureX, textureY); // Box 159
		bodyModel[63] = new ModelRendererTurbo(this, 193, 17, textureX, textureY); // Box 396
		bodyModel[64] = new ModelRendererTurbo(this, 425, 17, textureX, textureY); // Box 397
		bodyModel[65] = new ModelRendererTurbo(this, 281, 25, textureX, textureY); // Box 398
		bodyModel[66] = new ModelRendererTurbo(this, 385, 33, textureX, textureY); // Box 174
		bodyModel[67] = new ModelRendererTurbo(this, 401, 33, textureX, textureY); // Box 175
		bodyModel[68] = new ModelRendererTurbo(this, 409, 25, textureX, textureY); // Box 178
		bodyModel[69] = new ModelRendererTurbo(this, 473, 25, textureX, textureY); // Box 179
		bodyModel[70] = new ModelRendererTurbo(this, 241, 33, textureX, textureY); // Box 87
		bodyModel[71] = new ModelRendererTurbo(this, 321, 33, textureX, textureY); // Box 90
		bodyModel[72] = new ModelRendererTurbo(this, 465, 33, textureX, textureY); // Box 92
		bodyModel[73] = new ModelRendererTurbo(this, 353, 33, textureX, textureY); // Box 93
		bodyModel[74] = new ModelRendererTurbo(this, 489, 33, textureX, textureY); // Box 94
		bodyModel[75] = new ModelRendererTurbo(this, 153, 41, textureX, textureY); // Box 95
		bodyModel[76] = new ModelRendererTurbo(this, 177, 41, textureX, textureY); // Box 96
		bodyModel[77] = new ModelRendererTurbo(this, 201, 41, textureX, textureY); // Box 97
		bodyModel[78] = new ModelRendererTurbo(this, 385, 41, textureX, textureY); // Box 98
		bodyModel[79] = new ModelRendererTurbo(this, 449, 41, textureX, textureY); // Box 99
		bodyModel[80] = new ModelRendererTurbo(this, 473, 41, textureX, textureY); // Box 100
		bodyModel[81] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // Box 101
		bodyModel[82] = new ModelRendererTurbo(this, 97, 49, textureX, textureY); // Box 102
		bodyModel[83] = new ModelRendererTurbo(this, 225, 41, textureX, textureY); // Box 103
		bodyModel[84] = new ModelRendererTurbo(this, 353, 25, textureX, textureY); // Box 104
		bodyModel[85] = new ModelRendererTurbo(this, 417, 25, textureX, textureY); // Box 106
		bodyModel[86] = new ModelRendererTurbo(this, 177, 49, textureX, textureY); // Box 108
		bodyModel[87] = new ModelRendererTurbo(this, 241, 41, textureX, textureY); // Box 109
		bodyModel[88] = new ModelRendererTurbo(this, 305, 41, textureX, textureY); // Box 111
		bodyModel[89] = new ModelRendererTurbo(this, 505, 25, textureX, textureY); // Box 112
		bodyModel[90] = new ModelRendererTurbo(this, 337, 41, textureX, textureY); // Box 113
		bodyModel[91] = new ModelRendererTurbo(this, 369, 41, textureX, textureY); // Box 114
		bodyModel[92] = new ModelRendererTurbo(this, 417, 33, textureX, textureY); // Box 115
		bodyModel[93] = new ModelRendererTurbo(this, 409, 41, textureX, textureY); // Box 116
		bodyModel[94] = new ModelRendererTurbo(this, 225, 49, textureX, textureY); // Box 117
		bodyModel[95] = new ModelRendererTurbo(this, 273, 49, textureX, textureY); // Box 118
		bodyModel[96] = new ModelRendererTurbo(this, 417, 41, textureX, textureY); // Box 119
		bodyModel[97] = new ModelRendererTurbo(this, 497, 41, textureX, textureY); // Box 120
		bodyModel[98] = new ModelRendererTurbo(this, 161, 41, textureX, textureY); // Box 121
		bodyModel[99] = new ModelRendererTurbo(this, 345, 49, textureX, textureY); // Box 124
		bodyModel[100] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 128
		bodyModel[101] = new ModelRendererTurbo(this, 433, 49, textureX, textureY); // Box 171
		bodyModel[102] = new ModelRendererTurbo(this, 441, 49, textureX, textureY); // Box 180
		bodyModel[103] = new ModelRendererTurbo(this, 457, 49, textureX, textureY); // Box 181
		bodyModel[104] = new ModelRendererTurbo(this, 473, 49, textureX, textureY); // Box 182
		bodyModel[105] = new ModelRendererTurbo(this, 481, 49, textureX, textureY); // Box 183
		bodyModel[106] = new ModelRendererTurbo(this, 393, 33, textureX, textureY); // Box 184
		bodyModel[107] = new ModelRendererTurbo(this, 409, 33, textureX, textureY); // Box 185
		bodyModel[108] = new ModelRendererTurbo(this, 481, 49, textureX, textureY); // Box 136
		bodyModel[109] = new ModelRendererTurbo(this, 97, 57, textureX, textureY); // Box 139
		bodyModel[110] = new ModelRendererTurbo(this, 113, 57, textureX, textureY); // Box 140
		bodyModel[111] = new ModelRendererTurbo(this, 121, 57, textureX, textureY); // Box 141
		bodyModel[112] = new ModelRendererTurbo(this, 137, 57, textureX, textureY); // Box 181
		bodyModel[113] = new ModelRendererTurbo(this, 505, 49, textureX, textureY); // Box 176

		bodyModel[0].addShapeBox(0F, 0F, 0F, 53, 9, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 14
		bodyModel[0].setRotationPoint(-33F, -2F, 8.5F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 53, 9, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 15
		bodyModel[1].setRotationPoint(-33F, -2F, -9.5F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 14, 6, 19, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 16
		bodyModel[2].setRotationPoint(20F, 1.5F, -9.5F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 53, 2, 19, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 17
		bodyModel[3].setRotationPoint(-33F, -5F, -9.5F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 53, 7, 19, 0F,0F, 0F, -5.75F, 0F, 0F, -5.75F, 0F, 0F, -5.75F, 0F, 0F, -5.75F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 18
		bodyModel[4].setRotationPoint(-33F, -13F, -9.5F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F,0.25F, 0F, -0.25F, 0.25F, 0F, -0.25F, 0.25F, 0F, -0.25F, 0.25F, 0F, -0.25F, 0.25F, 0F, -0.25F, 0.25F, 0F, -0.25F, 0.25F, 0F, -0.25F, 0.25F, 0F, -0.25F); // Box 19
		bodyModel[5].setRotationPoint(-28F, -15F, -2.5F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 4, 6, 5, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.75F, 0F, -1F, -0.75F, 0F, -1F, -0.75F, 0F, -1F, -0.75F, 0F, -1F); // Box 21
		bodyModel[6].setRotationPoint(-29F, -21F, -2.5F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 4, 1, 5, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 23
		bodyModel[7].setRotationPoint(-29F, -22F, -2.5F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 15, 1, 7, 0F,0F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Box 24
		bodyModel[8].setRotationPoint(20F, -17F, -3.5F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 13, 1, 5, 0F,0F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, 0.5F, 0.75F, 0F, 0.5F, 0.75F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, -1F, 0.75F, 0F, -1F, 0.75F); // Box 26
		bodyModel[9].setRotationPoint(20F, -16F, -9.5F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 13, 1, 5, 0F,0F, 0.5F, 0.75F, 0F, 0.5F, 0.75F, 0F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, -1F, 0.75F, 0F, -1F, 0.75F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Box 27
		bodyModel[10].setRotationPoint(20F, -16F, 4.5F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 14, 17, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 28
		bodyModel[11].setRotationPoint(20F, -15F, -9.5F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 14, 17, 1, 0F,0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 29
		bodyModel[12].setRotationPoint(20F, -15F, 8.5F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 1, 18, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 30
		bodyModel[13].setRotationPoint(20F, -16F, -3.75F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 1, 18, 5, 0F,0F, -0.8F, 0F, 0F, -0.8F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 28
		bodyModel[14].setRotationPoint(20F, -16F, -8.5F);

		bodyModel[15].addShapeBox(0F, 0F, 0F, 1, 18, 5, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 29
		bodyModel[15].setRotationPoint(20F, -16F, 3.5F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 1, 8, 19, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F); // Box 30
		bodyModel[16].setRotationPoint(-34F, -2F, -9.5F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 19, 4, 5, 0F,0F, 0.5F, 0F, 0.25F, 0.5F, 0F, 0.25F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0F, 0F, 0F); // Box 31
		bodyModel[17].setRotationPoint(-20F, -17F, -2.5F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 3, 4, 5, 0F,-0.25F, 0.5F, 0F, 0.5F, -2F, 0F, 0.5F, -2F, 0F, -0.25F, 0.5F, 0F, -0.25F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.25F, 0F, 0F); // Box 32
		bodyModel[18].setRotationPoint(-1F, -17F, -2.5F);

		bodyModel[19].addShapeBox(0F, 0F, 0F, 3, 2, 3, 0F,-0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.25F, 0.5F, 0F); // Box 33
		bodyModel[19].setRotationPoint(-15.4F, -20F, -1.5F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 2, 2, 4, 0F,0F, 0.5F, -1.5F, 0.25F, 0.5F, 0F, 0.25F, 0.5F, 0F, 0F, 0.5F, -1.5F, 0F, 0F, -1.5F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0F, 0F, -1.5F); // Box 35
		bodyModel[20].setRotationPoint(17.75F, -15F, -2F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 1, 5, 1, 0F,-1F, 0.75F, -0.5F, 0.25F, 0.75F, -0.5F, 0.25F, 0.75F, 0F, -1F, 0.75F, 0F, -1F, 0F, -0.5F, 0.25F, 0F, -0.5F, 0.25F, 0F, 0F, -1F, 0F, 0F); // Box 36
		bodyModel[21].setRotationPoint(18.75F, -18F, -3F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F,0F, 0.5F, 1.75F, 0F, 0.5F, 1.75F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -1F, 1.75F, 0F, -1F, 1.75F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 39
		bodyModel[22].setRotationPoint(33F, -16F, 5.5F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 2, 1, 4, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 1.75F, 0F, 0.5F, 1.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 1.75F, 0F, -1F, 1.75F); // Box 40
		bodyModel[23].setRotationPoint(33F, -16F, -9.5F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F,0F, 0.2F, -0.75F, 0F, 0.2F, -0.75F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 45
		bodyModel[24].setRotationPoint(31F, 2F, -10.5F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 3, 5, 1, 0F,0F, 0.2F, -0.75F, 0F, 0.2F, -0.75F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 46
		bodyModel[25].setRotationPoint(33F, -16F, -10.5F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0.45F, -0.75F, 0F, 0.45F, -0.75F, 0F, 0.45F, 0F, 0F, 0.45F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F, 0F, -0.25F, 0F, 0F, -0.25F, 0F); // Box 47
		bodyModel[26].setRotationPoint(31F, 1F, -10.5F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0.45F, 0F, 0F, 0.45F, 0F, 0F, 0.45F, -0.75F, 0F, 0.45F, -0.75F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -0.75F, 0F, -0.25F, -0.75F); // Box 48
		bodyModel[27].setRotationPoint(31F, 1F, 9.5F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 5, 3, 1, 0F,0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, -0.75F, 0F, 0.2F, -0.75F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F); // Box 49
		bodyModel[28].setRotationPoint(31F, 2F, 9.5F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 3, 5, 1, 0F,0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, -0.75F, 0F, 0.2F, -0.75F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F); // Box 50
		bodyModel[29].setRotationPoint(33F, -16F, 9.5F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F,-0.25F, 0F, -0.25F, -0.25F, 0F, -1.75F, -0.25F, 0F, -1.75F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -1.75F, -0.25F, 0F, -1.75F, -0.25F, 0F, -0.25F); // Box 44
		bodyModel[30].setRotationPoint(-26F, -15F, -2.5F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 2, 2, 5, 0F,-0.25F, 0F, -1.75F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -1.75F, -0.25F, 0F, -1.75F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -1.75F); // Box 45
		bodyModel[31].setRotationPoint(-30F, -15F, -2.5F);

		bodyModel[32].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.25F, 0F, -0.75F, 0.25F, 0F, -0.75F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0.25F, -0.75F, 0.25F, 0.25F, -0.75F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F); // Box 46
		bodyModel[32].setRotationPoint(-7F, 0F, -10.5F);

		bodyModel[33].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.25F, 0F, -0.75F, 0.25F, 0F, -0.75F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0.25F, -0.75F, 0.25F, 0.25F, -0.75F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F); // Box 47
		bodyModel[33].setRotationPoint(-7F, 6F, -10.5F);

		bodyModel[34].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.25F, 0F, -0.75F, 0.25F, 0F, -0.75F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0.25F, -0.75F, 0.25F, 0.25F, -0.75F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F); // Box 49
		bodyModel[34].setRotationPoint(3F, 0F, -10.5F);

		bodyModel[35].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, -0.75F, 0.25F, 0F, -0.75F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, -0.75F, 0.25F, 0.25F, -0.75F); // Box 50
		bodyModel[35].setRotationPoint(-7F, 0F, 9.5F);

		bodyModel[36].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, -0.75F, 0.25F, 0F, -0.75F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, -0.75F, 0.25F, 0.25F, -0.75F); // Box 51
		bodyModel[36].setRotationPoint(-7F, 6F, 9.5F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, -0.75F, 0.25F, 0F, -0.75F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, -0.75F, 0.25F, 0.25F, -0.75F); // Box 52
		bodyModel[37].setRotationPoint(3F, 0F, 9.5F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, -0.75F, 0.25F, 0F, -0.75F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, -0.75F, 0.25F, 0.25F, -0.75F); // Box 53
		bodyModel[38].setRotationPoint(3F, 6F, 9.5F);

		bodyModel[39].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F,0.25F, 0.25F, -3.27F, 0.25F, 0.25F, -3.27F, 0.25F, 0F, 2.58F, 0.25F, 0F, 2.58F, 0.25F, 0F, -1F, 0.25F, 0F, -1F, 0.25F, 0.25F, 0.25F, 0.25F, 0.25F, 0.25F); // Box 54
		bodyModel[39].setRotationPoint(-17F, -10F, -9.5F);

		bodyModel[40].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F,0.25F, 0F, 2.58F, 0.25F, 0F, 2.58F, 0.25F, 0.25F, -3.27F, 0.25F, 0.25F, -3.27F, 0.25F, 0.25F, 0.25F, 0.25F, 0.25F, 0.25F, 0.25F, 0F, -1F, 0.25F, 0F, -1F); // Box 55
		bodyModel[40].setRotationPoint(-17F, -10F, 8.5F);

		bodyModel[41].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F,0.25F, 0F, -0.75F, 0.5F, 0F, -0.75F, 0.5F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0.25F, -0.75F, 0.5F, 0.25F, -0.75F, 0.5F, 0.25F, 0F, 0.25F, 0.25F, 0F); // Box 56
		bodyModel[41].setRotationPoint(-21F, 1.5F, -10.5F);

		bodyModel[42].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F,0.25F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.75F, 0.25F, 0F, -0.75F, 0.25F, 0.25F, 0F, 0.5F, 0.25F, 0F, 0.5F, 0.25F, -0.75F, 0.25F, 0.25F, -0.75F); // Box 57
		bodyModel[42].setRotationPoint(-21F, 1.5F, 9.5F);

		bodyModel[43].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 179
		bodyModel[43].setRotationPoint(14F, -1F, -9.76F);

		bodyModel[44].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 59
		bodyModel[44].setRotationPoint(14F, 2.5F, -9.76F);

		bodyModel[45].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 60
		bodyModel[45].setRotationPoint(14F, 6F, -9.76F);

		bodyModel[46].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 61
		bodyModel[46].setRotationPoint(14F, -1F, 8.75F);

		bodyModel[47].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 62
		bodyModel[47].setRotationPoint(14F, 2.5F, 8.75F);

		bodyModel[48].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 63
		bodyModel[48].setRotationPoint(14F, 6F, 8.75F);

		bodyModel[49].addShapeBox(0F, 0F, 0F, 3, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 64
		bodyModel[49].setRotationPoint(10F, -9F, -6.85F);

		bodyModel[50].addShapeBox(0F, 0F, 0F, 3, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 65
		bodyModel[50].setRotationPoint(14.5F, -3F, -9.76F);

		bodyModel[51].addShapeBox(0F, 0F, 0F, 3, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 66
		bodyModel[51].setRotationPoint(10F, -9F, 5.85F);

		bodyModel[52].addShapeBox(0F, 0F, 0F, 3, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 67
		bodyModel[52].setRotationPoint(14.5F, -3F, 8.75F);

		bodyModel[53].addShapeBox(0F, 0F, 0F, 3, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 68
		bodyModel[53].setRotationPoint(-17F, -1F, -9.76F);

		bodyModel[54].addShapeBox(0F, 0F, 0F, 3, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 69
		bodyModel[54].setRotationPoint(-17F, 2.5F, -9.76F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 3, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 70
		bodyModel[55].setRotationPoint(-17F, 6F, -9.76F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 3, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 71
		bodyModel[56].setRotationPoint(-12.5F, -4F, -9.76F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 3, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 72
		bodyModel[57].setRotationPoint(-12.5F, -4F, 8.75F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 73
		bodyModel[58].setRotationPoint(-17F, -1F, 8.75F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 74
		bodyModel[59].setRotationPoint(-17F, 2.5F, 8.75F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 75
		bodyModel[60].setRotationPoint(-17F, 6F, 8.75F);

		bodyModel[61].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 158
		bodyModel[61].setRotationPoint(-37F, 1F, -8.5F);

		bodyModel[62].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 159
		bodyModel[62].setRotationPoint(-37F, 1F, 7.5F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1.25F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1.25F, 0F, -0.25F); // Box 396
		bodyModel[63].setRotationPoint(-35.03F, 2F, -0.5F);

		bodyModel[64].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,1F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 397
		bodyModel[64].setRotationPoint(-36.28F, 3.01F, -0.5F);

		bodyModel[65].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 398
		bodyModel[65].setRotationPoint(-37.28F, 2.01F, -0.5F);

		bodyModel[66].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 174
		bodyModel[66].setRotationPoint(-37F, 3F, -4.5F);

		bodyModel[67].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 175
		bodyModel[67].setRotationPoint(-37F, 3F, 3.5F);

		bodyModel[68].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F); // Box 178
		bodyModel[68].setRotationPoint(-38F, 2F, 2.5F);

		bodyModel[69].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F); // Box 179
		bodyModel[69].setRotationPoint(-38F, 2F, -5.5F);

		bodyModel[70].addShapeBox(0F, 0F, 0F, 1, 1, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 87
		bodyModel[70].setRotationPoint(-7F, 6.5F, -6F);

		bodyModel[71].addShapeBox(0F, 0F, 0F, 1, 1, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 90
		bodyModel[71].setRotationPoint(3F, 6.5F, -6F);

		bodyModel[72].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 92
		bodyModel[72].setRotationPoint(9.5F, 4F, -7F);

		bodyModel[73].addShapeBox(0F, 0F, 0F, 1, 1, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 93
		bodyModel[73].setRotationPoint(13F, 6.5F, -6F);

		bodyModel[74].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 94
		bodyModel[74].setRotationPoint(9.5F, 4F, 6F);

		bodyModel[75].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 95
		bodyModel[75].setRotationPoint(-0.5F, 4F, 6F);

		bodyModel[76].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 96
		bodyModel[76].setRotationPoint(-0.5F, 4F, -7F);

		bodyModel[77].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 97
		bodyModel[77].setRotationPoint(-10.5F, 4F, 6F);

		bodyModel[78].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 98
		bodyModel[78].setRotationPoint(-10.5F, 4F, -7F);

		bodyModel[79].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 99
		bodyModel[79].setRotationPoint(-20.5F, 4F, 6F);

		bodyModel[80].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 100
		bodyModel[80].setRotationPoint(-20.5F, 4F, -7F);

		bodyModel[81].addShapeBox(0F, 0F, 0F, 42, 1, 4, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 101
		bodyModel[81].setRotationPoint(-22F, 2.5F, -8F);

		bodyModel[82].addShapeBox(0F, 0F, 0F, 35, 1, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, 0F, -0.25F, 0F); // Box 102
		bodyModel[82].setRotationPoint(-19F, 8.5F, -7F);

		bodyModel[83].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.05F, 0F, 0F, -0.05F); // Box 103
		bodyModel[83].setRotationPoint(-22F, 3.5F, -7F);

		bodyModel[84].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.05F, 0F, 0F, -0.05F); // Box 104
		bodyModel[84].setRotationPoint(-20F, 4.5F, -7F);

		bodyModel[85].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0F, 0F, -0.25F, 0F); // Box 106
		bodyModel[85].setRotationPoint(-21F, 4.5F, -7.5F);

		bodyModel[86].addShapeBox(0F, 0F, 0F, 21, 1, 1, 0F,0F, 3.75F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 3.75F, 0F, 0F, -4.25F, -0.5F, -0.2F, -0.25F, -0.5F, -0.2F, -0.25F, 0F, 0F, -4.25F, 0F); // Box 108
		bodyModel[86].setRotationPoint(-19F, 8.5F, -7.5F);

		bodyModel[87].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, -0.5F, 0F, 0.75F, -0.5F, 0F, 0.75F, 0F, 0F, 0F, 0F, -0.2F, -0.25F, -0.5F, 0.2F, -1F, -0.5F, 0.2F, -1F, 0F, -0.2F, -0.25F, 0F); // Box 109
		bodyModel[87].setRotationPoint(0F, 8.15F, -8F);

		bodyModel[88].addShapeBox(0F, 0F, 0F, 10, 1, 1, 0F,0.5F, 0F, -0.5F, 0F, -2.75F, -0.5F, 0F, -2.75F, 0F, 0.5F, 0F, 0F, 0.5F, -0.25F, -0.5F, -0.2F, 2.75F, -0.5F, -0.2F, 2.75F, 0F, 0.5F, -0.25F, 0F); // Box 111
		bodyModel[88].setRotationPoint(-6.5F, 4.5F, -8.5F);

		bodyModel[89].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.05F, 0F, 0F, -0.05F); // Box 112
		bodyModel[89].setRotationPoint(-8F, 4.5F, -8F);

		bodyModel[90].addShapeBox(0F, 0F, 0F, 10, 1, 1, 0F,0.5F, 0F, 0F, 0F, -2.75F, 0F, 0F, -2.75F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, -0.25F, 0F, -0.2F, 2.75F, 0F, -0.2F, 2.75F, -0.5F, 0.5F, -0.25F, -0.5F); // Box 113
		bodyModel[90].setRotationPoint(-6.5F, 4.5F, 7.5F);

		bodyModel[91].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 114
		bodyModel[91].setRotationPoint(-22F, 3.5F, 6F);

		bodyModel[92].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -0.5F, 0F, -0.25F, -0.5F); // Box 115
		bodyModel[92].setRotationPoint(-21F, 4.5F, 6.5F);

		bodyModel[93].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 116
		bodyModel[93].setRotationPoint(-20F, 4.5F, 6F);

		bodyModel[94].addShapeBox(0F, 0F, 0F, 21, 1, 1, 0F,0F, 3.75F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 3.75F, -0.5F, 0F, -4.25F, 0F, -0.2F, -0.25F, 0F, -0.2F, -0.25F, -0.5F, 0F, -4.25F, -0.5F); // Box 117
		bodyModel[94].setRotationPoint(-19F, 8.5F, 6.5F);

		bodyModel[95].addShapeBox(0F, 0F, 0F, 35, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -0.5F, 0F, -0.25F, -0.5F); // Box 118
		bodyModel[95].setRotationPoint(-19F, 8.5F, 6F);

		bodyModel[96].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 119
		bodyModel[96].setRotationPoint(-8F, 4.5F, 7F);

		bodyModel[97].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, 0F, 0F, 0.75F, 0F, 0F, 0.75F, -0.5F, 0F, 0F, -0.5F, -0.2F, -0.25F, 0F, 0.2F, -1F, 0F, 0.2F, -1F, -0.5F, -0.2F, -0.25F, -0.5F); // Box 120
		bodyModel[97].setRotationPoint(0F, 8.15F, 7F);

		bodyModel[98].addShapeBox(0F, 0F, 0F, 1, 1, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 121
		bodyModel[98].setRotationPoint(-17F, 6.5F, -6F);

		bodyModel[99].addShapeBox(0F, 0F, 0F, 39, 8, 8, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 124
		bodyModel[99].setRotationPoint(-21F, -2F, -4F);

		bodyModel[100].addShapeBox(0F, 0F, 0F, 42, 1, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 128
		bodyModel[100].setRotationPoint(-22F, 2.5F, 4F);

		bodyModel[101].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1.25F, 0F, -0.25F, 1.25F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 171
		bodyModel[101].setRotationPoint(34F, 2F, -0.5F);

		bodyModel[102].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 180
		bodyModel[102].setRotationPoint(34F, 3F, 3.5F);

		bodyModel[103].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 181
		bodyModel[103].setRotationPoint(34F, 3F, -4.5F);

		bodyModel[104].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 1F, 0F, -0.25F, 1F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F); // Box 182
		bodyModel[104].setRotationPoint(35.28F, 3.01F, -0.5F);

		bodyModel[105].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0F, -1F, -0.25F, 0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 183
		bodyModel[105].setRotationPoint(36.28F, 2.01F, -0.5F);

		bodyModel[106].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 184
		bodyModel[106].setRotationPoint(37F, 2F, 2.5F);

		bodyModel[107].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 185
		bodyModel[107].setRotationPoint(37F, 2F, -5.5F);

		bodyModel[108].addShapeBox(0F, 0F, 0F, 2, 13, 12, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 136
		bodyModel[108].setRotationPoint(21F, -11.5F, -6F);

		bodyModel[109].addShapeBox(0F, 0F, 0F, 3, 4, 1, 0F,0F, 0.2F, -0.75F, 0F, 0.2F, -0.75F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 139
		bodyModel[109].setRotationPoint(27F, -13F, -10.5F);

		bodyModel[110].addShapeBox(0F, 0F, 0F, 3, 4, 1, 0F,0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, -0.75F, 0F, 0.2F, -0.75F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F); // Box 140
		bodyModel[110].setRotationPoint(27F, -13F, 9.5F);

		bodyModel[111].addShapeBox(0F, 0F, 0F, 1, 3, 7, 0F,-0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 1.5F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, -0.75F, 1.5F, 0F); // Box 141
		bodyModel[111].setRotationPoint(-34F, -9F, -3.5F);

		bodyModel[112].addShapeBox(0F, 0F, 0F, 2, 0, 4, 0F,-1F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -1F, 0F, 0F); // Box 181
		bodyModel[112].setRotationPoint(33F, 1.5F, -2F);

		bodyModel[113].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.25F, 0F, -0.75F, 0.25F, 0F, -0.75F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0.25F, -0.75F, 0.25F, 0.25F, -0.75F, 0.25F, 0.25F, 0F, 0.25F, 0.25F, 0F); // Box 176
		bodyModel[113].setRotationPoint(3F, 6F, -10.5F);
	}
}