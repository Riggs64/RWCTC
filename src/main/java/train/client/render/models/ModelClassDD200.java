//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2023 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: Class DD200
// Model Creator: Mecha Max
// Created on: 19.07.2022 - 23:23:30
// Last changed on: 19.07.2022 - 23:23:30

package train.client.render.models; //Path where the model is located

import tmt.ModelConverter;
import tmt.ModelRendererTurbo;

public class ModelClassDD200 extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 512;

	public ModelClassDD200() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[100];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 185, 1, textureX, textureY); // Box 3
		bodyModel[2] = new ModelRendererTurbo(this, 225, 1, textureX, textureY); // Box 4
		bodyModel[3] = new ModelRendererTurbo(this, 265, 1, textureX, textureY); // Box 6
		bodyModel[4] = new ModelRendererTurbo(this, 289, 1, textureX, textureY); // Box 59
		bodyModel[5] = new ModelRendererTurbo(this, 241, 1, textureX, textureY); // Box 31
		bodyModel[6] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 163
		bodyModel[7] = new ModelRendererTurbo(this, 9, 1, textureX, textureY); // Box 171
		bodyModel[8] = new ModelRendererTurbo(this, 305, 1, textureX, textureY); // Box 177
		bodyModel[9] = new ModelRendererTurbo(this, 313, 1, textureX, textureY); // Box 182
		bodyModel[10] = new ModelRendererTurbo(this, 321, 1, textureX, textureY); // Box 183
		bodyModel[11] = new ModelRendererTurbo(this, 329, 1, textureX, textureY); // Box 126
		bodyModel[12] = new ModelRendererTurbo(this, 321, 1, textureX, textureY); // Box 126
		bodyModel[13] = new ModelRendererTurbo(this, 337, 1, textureX, textureY); // Box 126
		bodyModel[14] = new ModelRendererTurbo(this, 345, 1, textureX, textureY); // Box 177
		bodyModel[15] = new ModelRendererTurbo(this, 361, 1, textureX, textureY); // Box 6
		bodyModel[16] = new ModelRendererTurbo(this, 384, 1, textureX, textureY); // Box 1
		bodyModel[17] = new ModelRendererTurbo(this, 433, 1, textureX, textureY); // Box 26
		bodyModel[18] = new ModelRendererTurbo(this, 465, 9, textureX, textureY); // Box 33
		bodyModel[19] = new ModelRendererTurbo(this, 265, 33, textureX, textureY); // Box 126
		bodyModel[20] = new ModelRendererTurbo(this, 433, 1, textureX, textureY); // Box 152
		bodyModel[21] = new ModelRendererTurbo(this, 151, 79, textureX, textureY); // Box 31
		bodyModel[22] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // Box 126
		bodyModel[23] = new ModelRendererTurbo(this, 465, 1, textureX, textureY); // Box 126
		bodyModel[24] = new ModelRendererTurbo(this, 481, 1, textureX, textureY); // Box 126
		bodyModel[25] = new ModelRendererTurbo(this, 337, 9, textureX, textureY); // Box 147
		bodyModel[26] = new ModelRendererTurbo(this, 505, 1, textureX, textureY); // Box 147
		bodyModel[27] = new ModelRendererTurbo(this, 305, 9, textureX, textureY); // Box 171
		bodyModel[28] = new ModelRendererTurbo(this, 209, 25, textureX, textureY); // Box 31
		bodyModel[29] = new ModelRendererTurbo(this, 433, 25, textureX, textureY); // Box 126
		bodyModel[30] = new ModelRendererTurbo(this, 289, 17, textureX, textureY); // Box 126
		bodyModel[31] = new ModelRendererTurbo(this, 313, 9, textureX, textureY); // Box 147
		bodyModel[32] = new ModelRendererTurbo(this, 185, 17, textureX, textureY); // Box 126
		bodyModel[33] = new ModelRendererTurbo(this, 393, 9, textureX, textureY); // Box 182
		bodyModel[34] = new ModelRendererTurbo(this, 17, 9, textureX, textureY); // Box 183
		bodyModel[35] = new ModelRendererTurbo(this, 425, 41, textureX, textureY); // Box 31
		bodyModel[36] = new ModelRendererTurbo(this, 89, 57, textureX, textureY); // Box 142
		bodyModel[37] = new ModelRendererTurbo(this, 209, 17, textureX, textureY); // Box 142
		bodyModel[38] = new ModelRendererTurbo(this, 345, 17, textureX, textureY); // Box 126
		bodyModel[39] = new ModelRendererTurbo(this, 449, 41, textureX, textureY); // Box 126
		bodyModel[40] = new ModelRendererTurbo(this, 225, 17, textureX, textureY); // Box 138
		bodyModel[41] = new ModelRendererTurbo(this, 449, 17, textureX, textureY); // Box 138
		bodyModel[42] = new ModelRendererTurbo(this, 481, 41, textureX, textureY); // Box 126
		bodyModel[43] = new ModelRendererTurbo(this, 489, 25, textureX, textureY); // Box 138
		bodyModel[44] = new ModelRendererTurbo(this, 209, 33, textureX, textureY); // Box 138
		bodyModel[45] = new ModelRendererTurbo(this, 257, 57, textureX, textureY); // Box 142
		bodyModel[46] = new ModelRendererTurbo(this, 257, 41, textureX, textureY); // Box 142
		bodyModel[47] = new ModelRendererTurbo(this, 425, 41, textureX, textureY); // Box 142
		bodyModel[48] = new ModelRendererTurbo(this, 473, 41, textureX, textureY); // Box 142
		bodyModel[49] = new ModelRendererTurbo(this, 161, 57, textureX, textureY); // Box 152
		bodyModel[50] = new ModelRendererTurbo(this, 505, 9, textureX, textureY); // Box 163
		bodyModel[51] = new ModelRendererTurbo(this, 289, 17, textureX, textureY); // Box 163
		bodyModel[52] = new ModelRendererTurbo(this, 329, 57, textureX, textureY); // Box 152
		bodyModel[53] = new ModelRendererTurbo(this, 313, 17, textureX, textureY); // Box 163
		bodyModel[54] = new ModelRendererTurbo(this, 449, 41, textureX, textureY); // Box 152
		bodyModel[55] = new ModelRendererTurbo(this, 353, 57, textureX, textureY); // Box 10
		bodyModel[56] = new ModelRendererTurbo(this, 177, 57, textureX, textureY); // Box 31
		bodyModel[57] = new ModelRendererTurbo(this, 329, 9, textureX, textureY); // Box 171
		bodyModel[58] = new ModelRendererTurbo(this, 465, 57, textureX, textureY); // Box 147
		bodyModel[59] = new ModelRendererTurbo(this, 161, 65, textureX, textureY); // Box 147
		bodyModel[60] = new ModelRendererTurbo(this, 489, 57, textureX, textureY); // Box 147
		bodyModel[61] = new ModelRendererTurbo(this, 209, 65, textureX, textureY); // Box 152
		bodyModel[62] = new ModelRendererTurbo(this, 329, 65, textureX, textureY); // Box 152
		bodyModel[63] = new ModelRendererTurbo(this, 233, 25, textureX, textureY); // Box 163
		bodyModel[64] = new ModelRendererTurbo(this, 449, 49, textureX, textureY); // Box 152
		bodyModel[65] = new ModelRendererTurbo(this, 233, 65, textureX, textureY); // Box 126
		bodyModel[66] = new ModelRendererTurbo(this, 409, 65, textureX, textureY); // Box 126
		bodyModel[67] = new ModelRendererTurbo(this, 465, 65, textureX, textureY); // Box 126
		bodyModel[68] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 138
		bodyModel[69] = new ModelRendererTurbo(this, 409, 57, textureX, textureY); // Box 138
		bodyModel[70] = new ModelRendererTurbo(this, 89, 73, textureX, textureY); // Box 126
		bodyModel[71] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 138
		bodyModel[72] = new ModelRendererTurbo(this, 409, 65, textureX, textureY); // Box 138
		bodyModel[73] = new ModelRendererTurbo(this, 113, 73, textureX, textureY); // Box 152
		bodyModel[74] = new ModelRendererTurbo(this, 505, 25, textureX, textureY); // Box 163
		bodyModel[75] = new ModelRendererTurbo(this, 233, 33, textureX, textureY); // Box 163
		bodyModel[76] = new ModelRendererTurbo(this, 265, 73, textureX, textureY); // Box 152
		bodyModel[77] = new ModelRendererTurbo(this, 425, 33, textureX, textureY); // Box 163
		bodyModel[78] = new ModelRendererTurbo(this, 449, 65, textureX, textureY); // Box 152
		bodyModel[79] = new ModelRendererTurbo(this, 145, 73, textureX, textureY); // Box 152
		bodyModel[80] = new ModelRendererTurbo(this, 305, 73, textureX, textureY); // Box 152
		bodyModel[81] = new ModelRendererTurbo(this, 441, 65, textureX, textureY); // Box 168
		bodyModel[82] = new ModelRendererTurbo(this, 433, 9, textureX, textureY); // Box 171
		bodyModel[83] = new ModelRendererTurbo(this, 241, 17, textureX, textureY); // Box 177
		bodyModel[84] = new ModelRendererTurbo(this, 505, 33, textureX, textureY); // Box 182
		bodyModel[85] = new ModelRendererTurbo(this, 465, 9, textureX, textureY); // Box 183
		bodyModel[86] = new ModelRendererTurbo(this, 505, 41, textureX, textureY); // Box 126
		bodyModel[87] = new ModelRendererTurbo(this, 209, 73, textureX, textureY); // Box 126
		bodyModel[88] = new ModelRendererTurbo(this, 505, 49, textureX, textureY); // Box 126
		bodyModel[89] = new ModelRendererTurbo(this, 465, 65, textureX, textureY); // Box 177
		bodyModel[90] = new ModelRendererTurbo(this, 249, 17, textureX, textureY); // Box 147
		bodyModel[91] = new ModelRendererTurbo(this, 265, 17, textureX, textureY); // Box 147
		bodyModel[92] = new ModelRendererTurbo(this, 489, 65, textureX, textureY); // Box 182
		bodyModel[93] = new ModelRendererTurbo(this, 273, 17, textureX, textureY); // Box 183
		bodyModel[94] = new ModelRendererTurbo(this, 329, 73, textureX, textureY); // Box 31
		bodyModel[95] = new ModelRendererTurbo(this, 465, 17, textureX, textureY); // Box 171
		bodyModel[96] = new ModelRendererTurbo(this, 281, 73, textureX, textureY); // Box 168
		bodyModel[97] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 31
		bodyModel[98] = new ModelRendererTurbo(this, 289, 1, textureX, textureY); // Box 59
		bodyModel[99] = new ModelRendererTurbo(this, 105, 81, textureX, textureY); // Box 31

		bodyModel[0].addBox(0F, 3F, 0F, 79, 30, 22, 0F); // Box 0
		bodyModel[0].setRotationPoint(-39F, -28F, -11F);

		bodyModel[1].addBox(-9F, -5F, -9F, 15, 12, 1, 0F); // Box 3
		bodyModel[1].setRotationPoint(12F, -10F, -1.25F);

		bodyModel[2].addBox(-9F, -5F, -9F, 15, 12, 1, 0F); // Box 4
		bodyModel[2].setRotationPoint(12F, -10F, 17.75F);

		bodyModel[3].addBox(-9F, -12F, -9F, 15, 7, 1, 0F); // Box 6
		bodyModel[3].setRotationPoint(12F, -10F, 17.75F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 1, 1, 12, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -1F, 0F, -0.25F); // Box 59
		bodyModel[4].setRotationPoint(33.15F, -15.5F, -6.25F);

		bodyModel[5].addBox(0F, 0F, 0F, 1, 19, 20, 0F); // Box 31
		bodyModel[5].setRotationPoint(18F, -22F, -10.25F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F); // Box 163
		bodyModel[6].setRotationPoint(17F, 0F, 7.25F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 171
		bodyModel[7].setRotationPoint(33.5F, 1.5F, 8.75F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 177
		bodyModel[8].setRotationPoint(36.6F, -2.5F, -1.2F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 182
		bodyModel[9].setRotationPoint(37.65F, -2.5F, 4.75F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 183
		bodyModel[10].setRotationPoint(36.6F, -2.5F, 4.75F);

		bodyModel[11].addBox(0F, 0F, 0F, 1, 3, 2, 0F); // Box 126
		bodyModel[11].setRotationPoint(36.6F, -10.5F, 2.5F);

		bodyModel[12].addBox(0F, 0F, 0F, 0, 8, 18, 0F); // Box 126
		bodyModel[12].setRotationPoint(36.55F, -11F, -9F);

		bodyModel[13].addBox(0F, 0F, 0F, 1, 3, 2, 0F); // Box 126
		bodyModel[13].setRotationPoint(36.6F, -10.5F, -4.5F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 177
		bodyModel[14].setRotationPoint(37.65F, -2.5F, -1.2F);

		bodyModel[15].addBox(-9F, -12F, -9F, 15, 7, 1, 0F); // Box 6
		bodyModel[15].setRotationPoint(12F, -10F, -1.25F);

		bodyModel[16].addBox(-47F, 0.95F, -9F, 15, 13, 14, 0F); // Box 1
		bodyModel[16].setRotationPoint(66.1F, -17F, 1.75F);

		bodyModel[17].addBox(0F, 0F, 0F, 10, 1, 11, 0F); // Box 26
		bodyModel[17].setRotationPoint(-26F, -17F, -5.75F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 12, 1, 11, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 33
		bodyModel[18].setRotationPoint(5F, -25F, -5.75F);

		bodyModel[19].addBox(0F, 0F, 0F, 68, 2, 20, 0F); // Box 126
		bodyModel[19].setRotationPoint(-33F, -3F, -10.25F);

		bodyModel[20].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // Box 152
		bodyModel[20].setRotationPoint(23F, 0F, 6.25F);

		bodyModel[21].addShapeBox(0F, 0.95F, 0F, 6, 13, 12, 0F,0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		bodyModel[21].setRotationPoint(3.1F, -17F, -7.25F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 4, 6, 4, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 126
		bodyModel[22].setRotationPoint(-2.5F, -23F, -2.25F);

		bodyModel[23].addBox(0F, 0F, 0F, 7, 2, 3, 0F); // Box 126
		bodyModel[23].setRotationPoint(19F, -5F, -10.25F);

		bodyModel[24].addBox(0F, 0F, 0F, 6, 1, 6, 0F); // Box 126
		bodyModel[24].setRotationPoint(-4.1F, -17F, -3.25F);

		bodyModel[25].addBox(0F, 0F, 0F, 10, 4, 3, 0F); // Box 147
		bodyModel[25].setRotationPoint(4F, -1F, 6.75F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F); // Box 147
		bodyModel[26].setRotationPoint(36.6F, -7.5F, 2.5F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 171
		bodyModel[27].setRotationPoint(-37.05F, -10.5F, -0.8F);

		bodyModel[28].addBox(0F, 0F, 0F, 1, 19, 20, 0F); // Box 31
		bodyModel[28].setRotationPoint(2F, -22F, -10.25F);

		bodyModel[29].addBox(0F, 0F, 0F, 22, 4, 11, 0F); // Box 126
		bodyModel[29].setRotationPoint(-12F, -1F, -5.25F);

		bodyModel[30].addBox(0F, 0F, 0F, 8, 1, 7, 0F); // Box 126
		bodyModel[30].setRotationPoint(21F, -1F, -3.75F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F); // Box 147
		bodyModel[31].setRotationPoint(36.6F, -7.5F, -4.5F);

		bodyModel[32].addBox(0F, 0F, 0F, 7, 2, 3, 0F); // Box 126
		bodyModel[32].setRotationPoint(19F, -5F, 6.75F);

		bodyModel[33].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 182
		bodyModel[33].setRotationPoint(37.65F, -2.5F, -5.75F);

		bodyModel[34].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 183
		bodyModel[34].setRotationPoint(36.6F, -2.5F, -5.75F);

		bodyModel[35].addShapeBox(0F, 0F, 0F, 1, 3, 18, 0F,0F, 0F, 0F, 0F, 0F, -9F, 0F, 0F, -9F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -9F, 1F, 0F, -9F, 0F, 0F, 0F); // Box 31
		bodyModel[35].setRotationPoint(36.55F, 1F, -9F);

		bodyModel[36].addBox(0F, 0F, 0F, 35, 11, 0, 0F); // Box 142
		bodyModel[36].setRotationPoint(-33F, -14F, -10.25F);

		bodyModel[37].addBox(0F, 0F, 0F, 7, 9, 0, 0F); // Box 142
		bodyModel[37].setRotationPoint(19F, -14F, -10.25F);

		bodyModel[38].addBox(0F, 0F, 0F, 12, 2, 13, 0F); // Box 126
		bodyModel[38].setRotationPoint(19F, 0F, -6.75F);

		bodyModel[39].addBox(0F, 0F, 0F, 1, 1, 14, 0F); // Box 126
		bodyModel[39].setRotationPoint(19F, 2F, -7.25F);

		bodyModel[40].addBox(0F, 0F, 0F, 5, 5, 0, 0F); // Box 138
		bodyModel[40].setRotationPoint(17F, 0F, -7.35F);

		bodyModel[41].addBox(0F, 0F, 0F, 5, 5, 0, 0F); // Box 138
		bodyModel[41].setRotationPoint(17F, 0F, 6.8F);

		bodyModel[42].addBox(0F, 0F, 0F, 1, 1, 14, 0F); // Box 126
		bodyModel[42].setRotationPoint(30F, 2F, -7.25F);

		bodyModel[43].addBox(0F, 0F, 0F, 5, 5, 0, 0F); // Box 138
		bodyModel[43].setRotationPoint(28F, 0F, -7.35F);

		bodyModel[44].addBox(0F, 0F, 0F, 5, 5, 0, 0F); // Box 138
		bodyModel[44].setRotationPoint(28F, 0F, 6.8F);

		bodyModel[45].addBox(0F, 0F, 0F, 35, 11, 0, 0F); // Box 142
		bodyModel[45].setRotationPoint(-33F, -14F, 9.75F);

		bodyModel[46].addBox(0F, 0F, 0F, 9, 11, 0, 0F); // Box 142
		bodyModel[46].setRotationPoint(26F, -14F, -10.25F);

		bodyModel[47].addBox(0F, 0F, 0F, 7, 9, 0, 0F); // Box 142
		bodyModel[47].setRotationPoint(19F, -14F, 9.75F);

		bodyModel[48].addBox(0F, 0F, 0F, 9, 11, 0, 0F); // Box 142
		bodyModel[48].setRotationPoint(26F, -14F, 9.75F);

		bodyModel[49].addBox(0F, 0F, 0F, 14, 3, 1, 0F); // Box 152
		bodyModel[49].setRotationPoint(18F, 0F, 7.25F);

		bodyModel[50].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 163
		bodyModel[50].setRotationPoint(32F, 0F, 7.25F);

		bodyModel[51].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F); // Box 163
		bodyModel[51].setRotationPoint(17F, 0F, -8.75F);

		bodyModel[52].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 152
		bodyModel[52].setRotationPoint(17F, -1F, 7.25F);

		bodyModel[53].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 163
		bodyModel[53].setRotationPoint(32F, 0F, -8.75F);

		bodyModel[54].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // Box 152
		bodyModel[54].setRotationPoint(23F, 0F, -7.75F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 17, 2, 14, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 3F); // Box 10
		bodyModel[55].setRotationPoint(2F, -24F, -7.25F);

		bodyModel[56].addBox(0F, 0F, 0F, 3, 5, 18, 0F); // Box 31
		bodyModel[56].setRotationPoint(33.5F, -1F, -9F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 171
		bodyModel[57].setRotationPoint(33.5F, 1.5F, -10.05F);

		bodyModel[58].addBox(0F, 0F, 0F, 6, 4, 3, 0F); // Box 147
		bodyModel[58].setRotationPoint(-12F, -1F, 6.75F);

		bodyModel[59].addBox(0F, 0F, 0F, 10, 4, 3, 0F); // Box 147
		bodyModel[59].setRotationPoint(4F, -1F, -10.25F);

		bodyModel[60].addBox(0F, 0F, 0F, 6, 4, 3, 0F); // Box 147
		bodyModel[60].setRotationPoint(-12F, -1F, -10.25F);

		bodyModel[61].addBox(0F, 0F, 0F, 14, 3, 1, 0F); // Box 152
		bodyModel[61].setRotationPoint(18F, 0F, -8.75F);

		bodyModel[62].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 152
		bodyModel[62].setRotationPoint(17F, -1F, -8.75F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F); // Box 163
		bodyModel[63].setRotationPoint(-30F, 0F, 7.25F);

		bodyModel[64].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // Box 152
		bodyModel[64].setRotationPoint(-24F, 0F, 6.25F);

		bodyModel[65].addBox(0F, 0F, 0F, 8, 1, 7, 0F); // Box 126
		bodyModel[65].setRotationPoint(-26F, -1F, -3.75F);

		bodyModel[66].addBox(0F, 0F, 0F, 12, 2, 13, 0F); // Box 126
		bodyModel[66].setRotationPoint(-28F, 0F, -6.75F);

		bodyModel[67].addBox(0F, 0F, 0F, 1, 1, 14, 0F); // Box 126
		bodyModel[67].setRotationPoint(-28F, 2F, -7.25F);

		bodyModel[68].addBox(0F, 0F, 0F, 5, 5, 0, 0F); // Box 138
		bodyModel[68].setRotationPoint(-30F, 0F, -7.35F);

		bodyModel[69].addBox(0F, 0F, 0F, 5, 5, 0, 0F); // Box 138
		bodyModel[69].setRotationPoint(-30F, 0F, 6.8F);

		bodyModel[70].addBox(0F, 0F, 0F, 1, 1, 14, 0F); // Box 126
		bodyModel[70].setRotationPoint(-17F, 2F, -7.25F);

		bodyModel[71].addBox(0F, 0F, 0F, 5, 5, 0, 0F); // Box 138
		bodyModel[71].setRotationPoint(-19F, 0F, -7.35F);

		bodyModel[72].addBox(0F, 0F, 0F, 5, 5, 0, 0F); // Box 138
		bodyModel[72].setRotationPoint(-19F, 0F, 6.8F);

		bodyModel[73].addBox(0F, 0F, 0F, 14, 3, 1, 0F); // Box 152
		bodyModel[73].setRotationPoint(-29F, 0F, 7.25F);

		bodyModel[74].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 163
		bodyModel[74].setRotationPoint(-15F, 0F, 7.25F);

		bodyModel[75].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F); // Box 163
		bodyModel[75].setRotationPoint(-30F, 0F, -8.75F);

		bodyModel[76].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 152
		bodyModel[76].setRotationPoint(-30F, -1F, 7.25F);

		bodyModel[77].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F); // Box 163
		bodyModel[77].setRotationPoint(-15F, 0F, -8.75F);

		bodyModel[78].addBox(0F, 0F, 0F, 4, 4, 1, 0F); // Box 152
		bodyModel[78].setRotationPoint(-24F, 0F, -7.75F);

		bodyModel[79].addBox(0F, 0F, 0F, 14, 3, 1, 0F); // Box 152
		bodyModel[79].setRotationPoint(-29F, 0F, -8.75F);

		bodyModel[80].addShapeBox(0F, 0F, 0F, 16, 1, 1, 0F,-1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 152
		bodyModel[80].setRotationPoint(-30F, -1F, -8.75F);

		bodyModel[81].addBox(-47F, 0F, -9F, 0, 3, 19, 0F); // Box 168
		bodyModel[81].setRotationPoint(83.55F, -3F, -0.5F);

		bodyModel[82].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 171
		bodyModel[82].setRotationPoint(-36.05F, 1.5F, -9.75F);

		bodyModel[83].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 177
		bodyModel[83].setRotationPoint(-37.1F, -2.5F, -0.800000000000001F);

		bodyModel[84].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 182
		bodyModel[84].setRotationPoint(-38.15F, -2.5F, -5.75F);

		bodyModel[85].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 183
		bodyModel[85].setRotationPoint(-37.1F, -2.5F, -5.75F);

		bodyModel[86].addBox(0F, 0F, 0F, 1, 3, 2, 0F); // Box 126
		bodyModel[86].setRotationPoint(-37.05F, -10.5F, -4.5F);

		bodyModel[87].addBox(0F, 0F, 0F, 0, 8, 18, 0F); // Box 126
		bodyModel[87].setRotationPoint(-35.95F, -11F, -9F);

		bodyModel[88].addBox(0F, 0F, 0F, 1, 3, 2, 0F); // Box 126
		bodyModel[88].setRotationPoint(-37.05F, -10.5F, 2.5F);

		bodyModel[89].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 177
		bodyModel[89].setRotationPoint(-39.15F, -2.5F, -0.8F);

		bodyModel[90].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F); // Box 147
		bodyModel[90].setRotationPoint(-37.05F, -7.5F, -4.5F);

		bodyModel[91].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F); // Box 147
		bodyModel[91].setRotationPoint(-37.05F, -7.5F, 2.5F);

		bodyModel[92].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 182
		bodyModel[92].setRotationPoint(-38.15F, -2.5F, 4.75F);

		bodyModel[93].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 183
		bodyModel[93].setRotationPoint(-37.1F, -2.5F, 4.75F);

		bodyModel[94].addShapeBox(0F, 0F, 0F, 1, 3, 18, 0F,0F, 0F, -9F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -9F, 1F, 0F, -9F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -9F); // Box 31
		bodyModel[94].setRotationPoint(-37.1F, 1F, -9F);

		bodyModel[95].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 171
		bodyModel[95].setRotationPoint(-36.05F, 1.5F, 9.05F);

		bodyModel[96].addBox(-47F, 0F, -9F, 0, 3, 19, 0F); // Box 168
		bodyModel[96].setRotationPoint(10.95F, -3F, -0.5F);

		bodyModel[97].addBox(0F, 0.95F, 0F, 35, 13, 14, 0F); // Box 31
		bodyModel[97].setRotationPoint(-33.1F, -17F, -7.25F);

		bodyModel[98].addShapeBox(0F, 0F, 0F, 1, 1, 12, 0F,-1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, -1F, 0F, -0.25F); // Box 59
		bodyModel[98].setRotationPoint(-34.15F, -15.5F, -6.25F);

		bodyModel[99].addBox(0F, 0F, 0F, 4, 5, 18, 0F); // Box 31
		bodyModel[99].setRotationPoint(-36F, -1F, -9F);
	}
	public float[] getTrans() {
		return new float[]{ -1.5f, -0.13f, 0.0f };
	}
}