//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 23.12.2021 - 08:38:53
// Last changed on: 23.12.2021 - 08:38:53

package train.client.render.models; //Path where the model is located


import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.client.render.RenderRollingStock;
import train.common.api.AbstractTrains;
import train.common.entity.rollingStock.PassengerPL42;
import train.common.library.Info;

public class ModelPL42 extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 128;

	public ModelPL42() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[93];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}
	private ModelPL42Bogie trucks = new ModelPL42Bogie();

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		if (entity instanceof PassengerPL42) {
			Tessellator.bindTexture(RenderRollingStock.getTexture(entity));
			for (int i = 0; i < 93; i++) {
				if (i == 33 || i == 31 || i == 32 || i == 29 || i == 43 || i == 42 || i == 44 || i == 45 || i == 38 || i == 39 || i == 37 || i == 41 || i == 40 || i == 30 || i == 34 || i == 35 || i == 36) {
					if (entity.riddenByEntity != null) {
						bodyModel[i].setRotationAngle(0, -entity.riddenByEntity.getRotationYawHead() + entity.rotationYaw+90, 0);
						((PassengerPL42) entity).gunRot = (-entity.riddenByEntity.getRotationYawHead()+ entity.rotationYaw+90);
					} else {
						bodyModel[i].setRotationAngle(0, ((PassengerPL42) entity).gunRot, 0);
					}
				}
				bodyModel[i].render(f5);
			}
		}

		Tessellator.bindTexture(new ResourceLocation(Info.resourceLocation, "textures/trains/PL-42 Bogie Texture.png"));
		GL11.glPushMatrix();
		GL11.glTranslated(2,0,0);
		trucks.render(entity,f,f1,f2,f3,f4,f5);
		GL11.glTranslated(-4,0,0);
		trucks.render(entity,f,f1,f2,f3,f4,f5);
		GL11.glPopMatrix();
	}
	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 217, 1, textureX, textureY); // Box 1
		bodyModel[2] = new ModelRendererTurbo(this, 217, 9, textureX, textureY); // Box 2
		bodyModel[3] = new ModelRendererTurbo(this, 393, 1, textureX, textureY); // Box 3
		bodyModel[4] = new ModelRendererTurbo(this, 441, 1, textureX, textureY); // Box 4
		bodyModel[5] = new ModelRendererTurbo(this, 233, 17, textureX, textureY); // Box 5
		bodyModel[6] = new ModelRendererTurbo(this, 417, 1, textureX, textureY); // Box 8
		bodyModel[7] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 396
		bodyModel[8] = new ModelRendererTurbo(this, 9, 1, textureX, textureY); // Box 397
		bodyModel[9] = new ModelRendererTurbo(this, 417, 1, textureX, textureY); // Box 398
		bodyModel[10] = new ModelRendererTurbo(this, 433, 1, textureX, textureY); // Box 174
		bodyModel[11] = new ModelRendererTurbo(this, 441, 1, textureX, textureY); // Box 178
		bodyModel[12] = new ModelRendererTurbo(this, 465, 1, textureX, textureY); // Box 179
		bodyModel[13] = new ModelRendererTurbo(this, 481, 1, textureX, textureY); // Box 158
		bodyModel[14] = new ModelRendererTurbo(this, 497, 1, textureX, textureY); // Box 159
		bodyModel[15] = new ModelRendererTurbo(this, 449, 1, textureX, textureY); // Box 17
		bodyModel[16] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // Box 18
		bodyModel[17] = new ModelRendererTurbo(this, 441, 9, textureX, textureY); // Box 19
		bodyModel[18] = new ModelRendererTurbo(this, 465, 9, textureX, textureY); // Box 20
		bodyModel[19] = new ModelRendererTurbo(this, 481, 9, textureX, textureY); // Box 21
		bodyModel[20] = new ModelRendererTurbo(this, 497, 9, textureX, textureY); // Box 22
		bodyModel[21] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Box 24
		bodyModel[22] = new ModelRendererTurbo(this, 473, 1, textureX, textureY); // Box 25
		bodyModel[23] = new ModelRendererTurbo(this, 217, 17, textureX, textureY); // Box 26
		bodyModel[24] = new ModelRendererTurbo(this, 233, 17, textureX, textureY); // Box 27
		bodyModel[25] = new ModelRendererTurbo(this, 9, 9, textureX, textureY); // Box 28
		bodyModel[26] = new ModelRendererTurbo(this, 449, 9, textureX, textureY); // Box 29
		bodyModel[27] = new ModelRendererTurbo(this, 345, 17, textureX, textureY); // Box 30
		bodyModel[28] = new ModelRendererTurbo(this, 353, 17, textureX, textureY); // Box 31
		bodyModel[29] = new ModelRendererTurbo(this, 353, 17, textureX, textureY); // Box 32
		bodyModel[30] = new ModelRendererTurbo(this, 377, 17, textureX, textureY); // Box 34
		bodyModel[31] = new ModelRendererTurbo(this, 473, 17, textureX, textureY); // Box 35
		bodyModel[32] = new ModelRendererTurbo(this, 425, 17, textureX, textureY); // Box 36
		bodyModel[33] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Box 37
		bodyModel[34] = new ModelRendererTurbo(this, 497, 17, textureX, textureY); // Box 43
		bodyModel[35] = new ModelRendererTurbo(this, 345, 25, textureX, textureY); // Box 44
		bodyModel[36] = new ModelRendererTurbo(this, 33, 33, textureX, textureY); // Box 45
		bodyModel[37] = new ModelRendererTurbo(this, 57, 33, textureX, textureY); // Box 46
		bodyModel[38] = new ModelRendererTurbo(this, 233, 25, textureX, textureY); // Box 48
		bodyModel[39] = new ModelRendererTurbo(this, 393, 17, textureX, textureY); // Box 49
		bodyModel[40] = new ModelRendererTurbo(this, 401, 17, textureX, textureY); // Box 50
		bodyModel[41] = new ModelRendererTurbo(this, 417, 17, textureX, textureY); // Box 51
		bodyModel[42] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Box 53
		bodyModel[43] = new ModelRendererTurbo(this, 81, 33, textureX, textureY); // Box 54
		bodyModel[44] = new ModelRendererTurbo(this, 361, 17, textureX, textureY); // Box 48
		bodyModel[45] = new ModelRendererTurbo(this, 377, 17, textureX, textureY); // Box 49
		bodyModel[46] = new ModelRendererTurbo(this, 89, 33, textureX, textureY); // Box 50
		bodyModel[47] = new ModelRendererTurbo(this, 129, 33, textureX, textureY); // Box 69
		bodyModel[48] = new ModelRendererTurbo(this, 145, 33, textureX, textureY); // Box 70
		bodyModel[49] = new ModelRendererTurbo(this, 193, 33, textureX, textureY); // Box 71
		bodyModel[50] = new ModelRendererTurbo(this, 65, 33, textureX, textureY); // Box 72
		bodyModel[51] = new ModelRendererTurbo(this, 225, 33, textureX, textureY); // Box 73
		bodyModel[52] = new ModelRendererTurbo(this, 177, 33, textureX, textureY); // Box 74
		bodyModel[53] = new ModelRendererTurbo(this, 217, 33, textureX, textureY); // Box 75
		bodyModel[54] = new ModelRendererTurbo(this, 425, 17, textureX, textureY); // Box 76
		bodyModel[55] = new ModelRendererTurbo(this, 441, 17, textureX, textureY); // Box 77
		bodyModel[56] = new ModelRendererTurbo(this, 505, 25, textureX, textureY); // Box 78
		bodyModel[57] = new ModelRendererTurbo(this, 449, 17, textureX, textureY); // Box 79
		bodyModel[58] = new ModelRendererTurbo(this, 465, 17, textureX, textureY); // Box 80
		bodyModel[59] = new ModelRendererTurbo(this, 473, 17, textureX, textureY); // Box 81
		bodyModel[60] = new ModelRendererTurbo(this, 249, 33, textureX, textureY); // Box 82
		bodyModel[61] = new ModelRendererTurbo(this, 265, 33, textureX, textureY); // Box 83
		bodyModel[62] = new ModelRendererTurbo(this, 193, 33, textureX, textureY); // Box 84
		bodyModel[63] = new ModelRendererTurbo(this, 305, 33, textureX, textureY); // Box 85
		bodyModel[64] = new ModelRendererTurbo(this, 321, 33, textureX, textureY); // Box 86
		bodyModel[65] = new ModelRendererTurbo(this, 337, 33, textureX, textureY); // Box 87
		bodyModel[66] = new ModelRendererTurbo(this, 393, 33, textureX, textureY); // Box 90
		bodyModel[67] = new ModelRendererTurbo(this, 409, 33, textureX, textureY); // Box 91
		bodyModel[68] = new ModelRendererTurbo(this, 425, 33, textureX, textureY); // Box 92
		bodyModel[69] = new ModelRendererTurbo(this, 265, 33, textureX, textureY); // Box 93
		bodyModel[70] = new ModelRendererTurbo(this, 385, 33, textureX, textureY); // Box 94
		bodyModel[71] = new ModelRendererTurbo(this, 441, 33, textureX, textureY); // Box 95
		bodyModel[72] = new ModelRendererTurbo(this, 449, 33, textureX, textureY); // Box 96
		bodyModel[73] = new ModelRendererTurbo(this, 457, 33, textureX, textureY); // Box 186
		bodyModel[74] = new ModelRendererTurbo(this, 473, 33, textureX, textureY); // Box 93
		bodyModel[75] = new ModelRendererTurbo(this, 489, 33, textureX, textureY); // Box 94
		bodyModel[76] = new ModelRendererTurbo(this, 1, 41, textureX, textureY); // Box 95
		bodyModel[77] = new ModelRendererTurbo(this, 33, 41, textureX, textureY); // Box 99
		bodyModel[78] = new ModelRendererTurbo(this, 49, 41, textureX, textureY); // Box 100
		bodyModel[79] = new ModelRendererTurbo(this, 129, 41, textureX, textureY); // Box 101
		bodyModel[80] = new ModelRendererTurbo(this, 145, 41, textureX, textureY); // Box 102
		bodyModel[81] = new ModelRendererTurbo(this, 65, 41, textureX, textureY); // Box 99
		bodyModel[82] = new ModelRendererTurbo(this, 177, 41, textureX, textureY); // Box 101
		bodyModel[83] = new ModelRendererTurbo(this, 193, 41, textureX, textureY); // Box 102
		bodyModel[84] = new ModelRendererTurbo(this, 217, 41, textureX, textureY); // Box 103
		bodyModel[85] = new ModelRendererTurbo(this, 305, 41, textureX, textureY); // Box 104
		bodyModel[86] = new ModelRendererTurbo(this, 321, 41, textureX, textureY); // Box 105
		bodyModel[87] = new ModelRendererTurbo(this, 337, 41, textureX, textureY); // Box 106
		bodyModel[88] = new ModelRendererTurbo(this, 353, 41, textureX, textureY); // Box 107
		bodyModel[89] = new ModelRendererTurbo(this, 465, 33, textureX, textureY); // Box 110
		bodyModel[90] = new ModelRendererTurbo(this, 481, 33, textureX, textureY); // Box 111
		bodyModel[91] = new ModelRendererTurbo(this, 497, 33, textureX, textureY); // Box 112
		bodyModel[92] = new ModelRendererTurbo(this, 41, 41, textureX, textureY); // Box 113

		bodyModel[0].addShapeBox(0F, 0F, 0F, 95, 10, 19, 0F,-2.5F, 0F, -3F, -2.5F, 0F, -3F, -2.5F, 0F, -3F, -2.5F, 0F, -3F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 0
		bodyModel[0].setRotationPoint(-47.5F, -8F, -9.5F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 93, 5, 1, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		bodyModel[1].setRotationPoint(-46.5F, 2F, -9.5F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 93, 5, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 2
		bodyModel[2].setRotationPoint(-46.5F, 2F, 8.5F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 1, 5, 19, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -1F, 0F, -0.5F); // Box 3
		bodyModel[3].setRotationPoint(-47.5F, 2F, -9.5F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 1, 5, 19, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, -1F, 0F, -0.5F, -1F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 4
		bodyModel[4].setRotationPoint(46.5F, 2F, -9.5F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 46, 1, 13, 0F,-0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		bodyModel[5].setRotationPoint(-23F, -9F, -6.5F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 5, 4, 5, 0F,-0.75F, 0F, -0.75F, -0.75F, 0F, -0.75F, -0.75F, 0F, -0.75F, -0.75F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		bodyModel[6].setRotationPoint(-4.5F, -13F, 0F);
		bodyModel[6].rotateAngleY = -0.78539816F;

		bodyModel[7].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0.5F, 0F, -0.25F, 0.5F, 0F, -0.25F, 0F, 0F, -0.25F, 1.25F, 0F, -0.25F, 0.68F, 0F, -0.25F, 0.68F, 0F, -0.25F, 1.25F, 0F, -0.25F); // Box 396
		bodyModel[7].setRotationPoint(-49F, 2F, -0.5F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,1F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 397
		bodyModel[8].setRotationPoint(-50.25F, 3.01F, -0.5F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 398
		bodyModel[9].setRotationPoint(-51.25F, 2.01F, -0.5F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, 0F, 0.65F, 0F, 0F, 0.65F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.85F, 0F, 0F, 0.85F, 0F, 0F, 0F, 0F, 0F); // Box 174
		bodyModel[10].setRotationPoint(-50.97F, 3F, -4.5F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F); // Box 178
		bodyModel[11].setRotationPoint(-51.97F, 2F, 2.5F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F); // Box 179
		bodyModel[12].setRotationPoint(-51.97F, 2F, -5.5F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 3, 5, 1, 0F,0F, 0F, -1F, 0.5F, 0F, -1F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 1.5F, 0F, -1F, 1.5F, 0F, 0F, 0F, 0F, 0F); // Box 158
		bodyModel[13].setRotationPoint(-51F, 2F, -8.5F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 3, 5, 1, 0F,0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 1.5F, 0F, 0F, 1.5F, 0F, -1F, 0F, 0F, -1F); // Box 159
		bodyModel[14].setRotationPoint(-51F, 2F, 7.5F);

		bodyModel[15].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, -1F, 0.75F, 0F, -1F, 0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0.5F, 0F, -1F, 0.5F, 0F, 0F, 0F, 0F, 0F); // Box 17
		bodyModel[15].setRotationPoint(-51F, 1F, -8.5F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, 0F, 0.75F, 0F, 0F, 0.75F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -1F, 0F, 0F, -1F); // Box 18
		bodyModel[16].setRotationPoint(-51F, 1F, 7.5F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0.75F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0.75F, 0F, 0F, 0.5F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0.5F, 0F, 0F); // Box 19
		bodyModel[17].setRotationPoint(48F, 1F, -8.5F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 3, 5, 1, 0F,0.5F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0.5F, 0F, 0F, 1.5F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 1.5F, 0F, 0F); // Box 20
		bodyModel[18].setRotationPoint(48F, 2F, -8.5F);

		bodyModel[19].addShapeBox(0F, 0F, 0F, 3, 5, 1, 0F,0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0.5F, 0F, -1F, 1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 1.5F, 0F, -1F); // Box 21
		bodyModel[19].setRotationPoint(48F, 2F, 7.5F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0.75F, 0F, -1F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0.5F, 0F, -1F); // Box 22
		bodyModel[20].setRotationPoint(48F, 1F, 7.5F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0F, 0F, 0F, 0.65F, 0F, 0F, 0.65F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.85F, 0F, 0F, 0.85F, 0F, 0F, 0F, 0F, 0F); // Box 24
		bodyModel[21].setRotationPoint(-50.97F, 3F, 3.5F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.5F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0.5F, 0F, -0.25F, 0.68F, 0F, -0.25F, 1.25F, 0F, -0.25F, 1.25F, 0F, -0.25F, 0.68F, 0F, -0.25F); // Box 25
		bodyModel[22].setRotationPoint(48F, 2F, -0.5F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0.65F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.65F, 0F, 0F, 0.85F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.85F, 0F, 0F); // Box 26
		bodyModel[23].setRotationPoint(48.03F, 3F, -4.5F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 3, 1, 1, 0F,0.65F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.65F, 0F, 0F, 0.85F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.85F, 0F, 0F); // Box 27
		bodyModel[24].setRotationPoint(48.03F, 3F, 3.5F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 28
		bodyModel[25].setRotationPoint(51.03F, 2F, 2.5F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 29
		bodyModel[26].setRotationPoint(51.03F, 2F, -5.5F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 1F, 0F, -0.25F, 1F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F); // Box 30
		bodyModel[27].setRotationPoint(49.25F, 3.01F, -0.5F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0F, -1F, -0.25F, 0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 31
		bodyModel[28].setRotationPoint(50.25F, 2.01F, -0.5F);

		bodyModel[29].addShapeBox(-4.63F, 0F, -6.5F, 3, 4, 13, 0F,0F, 0F, -3.75F, 0F, 0.25F, -3F, 0F, 0.25F, -3F, 0F, 0F, -3.75F, 0F, 0F, -2.25F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -2.25F); // Box 32
		bodyModel[29].setRotationPoint(33.63F, -12.5F, 0F);

		bodyModel[30].addShapeBox(-6.63F, 0F, -2.5F, 2, 3, 5, 0F,0.5F, -1.25F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0.5F, -1.25F, -0.25F, 0.5F, 0F, -0.25F, -1.75F, 1F, -0.25F, -1.75F, 1F, -0.25F, 0.5F, 0F, -0.25F); // Box 34
		bodyModel[30].setRotationPoint(33.63F, -12.5F, 0F);

		bodyModel[31].addShapeBox(0.37F, 0F, -5.5F, 3, 4, 11, 0F,0F, 0.25F, -1.5F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.75F, 0F, 0.25F, -1.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 35
		bodyModel[31].setRotationPoint(33.63F, -12.5F, 0F);

		bodyModel[32].addShapeBox(-1.63F, 0F, -5.5F, 2, 4, 11, 0F,0F, 0.25F, -2F, 0F, 0.25F, -1.5F, 0F, 0.25F, -1.5F, 0F, 0.25F, -2F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F); // Box 36
		bodyModel[32].setRotationPoint(33.63F, -12.5F, 0F);

		bodyModel[33].addShapeBox(3.37F, 0F, -6.5F, 7, 4, 13, 0F,0F, 0.25F, -2.75F, -2.5F, 0F, -4.25F, -2.5F, 0F, -4.25F, 0F, 0.25F, -2.75F, 0F, 0F, -0.5F, 0F, 0F, -4.25F, 0F, 0F, -4.25F, 0F, 0F, -0.5F); // Box 37
		bodyModel[33].setRotationPoint(33.63F, -12.5F, 0);

		bodyModel[34].addShapeBox(-9.63F, 0F, -1.5F, 2, 2, 3, 0F,0F, -0.25F, 0F, 0.5F, -0.25F, 0F, 0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F); // Box 43
		bodyModel[34].setRotationPoint(33.63F, -11.5F, 0F);

		bodyModel[35].addShapeBox(-9.63F, 0F, -1.5F, 4, 1, 3, 0F,-0.5F, 0F, -0.25F, 1F, 0F, 0F, 1F, 0F, 0F, -0.5F, 0F, -0.25F, 0F, 0.25F, 0F, -1.5F, 0.25F, 0F, -1.5F, 0.25F, 0F, 0F, 0.25F, 0F); // Box 44
		bodyModel[35].setRotationPoint(33.63F, -12.5F, 0F);

		bodyModel[36].addShapeBox(-21.63F, 0F, -0.5F, 12, 1, 1, 0F,0.75F, -0.25F, 0F, 0.4F, -0.25F, 0F, 0.4F, -0.25F, 0F, 0.75F, -0.25F, 0F, 0.75F, 0.25F, 0F, -0.1F, 0.5F, 0F, -0.1F, 0.5F, 0F, 0.75F, 0.25F, 0F); // Box 45
		bodyModel[36].setRotationPoint(33.63F, -12.5F, 0F);

		bodyModel[37].addShapeBox(0.37F, 0F, -1.5F, 5, 1, 4, 0F,-0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F); // Box 46
		bodyModel[37].setRotationPoint(33.63F, -13.5F, 0F);

		bodyModel[38].addShapeBox(-3.63F, 0F, -2.5F, 2, 2, 1, 0F,-0.75F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.75F, -0.5F, 0F, -0.75F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.75F, 0F, 0F); // Box 48
		bodyModel[38].setRotationPoint(33.63F, -14.5F, 0F);

		bodyModel[39].addShapeBox(-1.63F, 0F, 3.5F, 2, 1, 1, 0F,-0.75F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.75F, -0.25F, 0F); // Box 49
		bodyModel[39].setRotationPoint(33.63F, -13.5F, 0F);

		bodyModel[40].addShapeBox(-0.63F, 0F, -0.5F, 2, 1, 1, 0F,-0.5F, -0.75F, -0.25F, 0F, -0.9F, -0.25F, 0F, -0.9F, -0.25F, -0.5F, -0.75F, -0.25F, -0.5F, 0.25F, -0.25F, 0F, 0.25F, -0.25F, 0F, 0.25F, -0.25F, -0.5F, 0.25F, -0.25F); // Box 50
		bodyModel[40].setRotationPoint(33.63F, -14.5F, 0F);

		bodyModel[41].addShapeBox(-0.63F, 0F, 1.5F, 2, 1, 1, 0F,-1F, -0.75F, -0.25F, 0F, -0.9F, -0.25F, 0F, -0.9F, -0.25F, -1F, -0.75F, -0.25F, -1F, 0.25F, -0.25F, 0F, 0.25F, -0.25F, 0F, 0.25F, -0.25F, -1F, 0.25F, -0.25F); // Box 51
		bodyModel[41].setRotationPoint(33.63F, -14.5F, 0F);

		bodyModel[42].addShapeBox(-6.63F, 0F, -4.5F, 2, 3, 2, 0F,-1.5F, -0.75F, -2F, 0F, 0F, -1.75F, 0F, 0F, 0.25F, -1.5F, -0.75F, 0.25F, -0.25F, 1F, -1.5F, 0F, 1F, -0.25F, 0F, 1F, 0.25F, -0.25F, 1F, 0.25F); // Box 53
		bodyModel[42].setRotationPoint(33.63F, -12.5F, 0F);

		bodyModel[43].addShapeBox(-6.63F, 0F, 2.5F, 2, 3, 2, 0F,-1.5F, -0.75F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, -1.75F, -1.5F, -0.75F, -2F, -0.25F, 1F, 0.25F, 0F, 1F, 0.25F, 0F, 1F, -0.25F, -0.25F, 1F, -1.5F); // Box 54
		bodyModel[43].setRotationPoint(33.63F, -12.5F, 0F);

		bodyModel[44].addShapeBox(-0.63F, 0F, -5.5F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.4F, 0F, 0F, 0.65F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.1F, 0F, 0F, 0.1F); // Box 48
		bodyModel[44].setRotationPoint(33.63F, -12.5F, 0F);

		bodyModel[45].addShapeBox(-0.63F, 0F, 4.5F, 1, 1, 1, 0F,0F, 0F, 0.65F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.1F, 0F, 0F, -0.1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 49
		bodyModel[45].setRotationPoint(33.63F, -12.5F, 0F);

		bodyModel[46].addShapeBox(0F, 0F, 0F, 13, 1, 7, 0F,-0.25F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.25F, -0.5F, -0.5F, -0.25F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.25F, 0F, -0.5F); // Box 50
		bodyModel[46].setRotationPoint(27F, -9F, -3.5F);

		bodyModel[47].addShapeBox(0F, 0F, 0F, 12, 1, 1, 0F,0.4F, -0.25F, 0F, 0.75F, -0.25F, 0F, 0.75F, -0.25F, 0F, 0.4F, -0.25F, 0F, -0.1F, 0.5F, 0F, 0.75F, 0.25F, 0F, 0.75F, 0.25F, 0F, -0.1F, 0.5F, 0F); // Box 69
		bodyModel[47].setRotationPoint(-24F, -12.5F, -0.5F);

		bodyModel[48].addShapeBox(0F, 0F, 0F, 7, 4, 13, 0F,-2.5F, 0F, -4.25F, 0F, 0.25F, -2.75F, 0F, 0.25F, -2.75F, -2.5F, 0F, -4.25F, 0F, 0F, -4.25F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -4.25F); // Box 70
		bodyModel[48].setRotationPoint(-44F, -12.5F, -6.5F);

		bodyModel[49].addShapeBox(0F, 0F, 0F, 3, 4, 11, 0F,0F, 0.25F, -1.75F, 0F, 0.25F, -1.5F, 0F, 0.25F, -1.5F, 0F, 0.25F, -1.75F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // Box 71
		bodyModel[49].setRotationPoint(-37F, -12.5F, -5.5F);

		bodyModel[50].addShapeBox(0F, 0F, 0F, 2, 4, 11, 0F,0F, 0.25F, -1.5F, 0F, 0.25F, -2F, 0F, 0.25F, -2F, 0F, 0.25F, -1.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F); // Box 72
		bodyModel[50].setRotationPoint(-34F, -12.5F, -5.5F);

		bodyModel[51].addShapeBox(0F, 0F, 0F, 3, 4, 13, 0F,0F, 0.25F, -3F, 0F, 0F, -3.75F, 0F, 0F, -3.75F, 0F, 0.25F, -3F, 0F, 0F, -1F, 0F, 0F, -2.25F, 0F, 0F, -2.25F, 0F, 0F, -1F); // Box 73
		bodyModel[51].setRotationPoint(-32F, -12.5F, -6.5F);

		bodyModel[52].addShapeBox(0F, 0F, 0F, 4, 1, 3, 0F,1F, 0F, 0F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 1F, 0F, 0F, -1.5F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, -1.5F, 0.25F, 0F); // Box 74
		bodyModel[52].setRotationPoint(-28F, -12.5F, -1.5F);

		bodyModel[53].addShapeBox(0F, 0F, 0F, 5, 1, 4, 0F,0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, -0.25F, 0F, 0F, -0.25F, 0F); // Box 75
		bodyModel[53].setRotationPoint(-39F, -13.5F, -2.5F);

		bodyModel[54].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, -0.9F, -0.25F, -0.5F, -0.75F, -0.25F, -0.5F, -0.75F, -0.25F, 0F, -0.9F, -0.25F, 0F, 0.25F, -0.25F, -0.5F, 0.25F, -0.25F, -0.5F, 0.25F, -0.25F, 0F, 0.25F, -0.25F); // Box 76
		bodyModel[54].setRotationPoint(-35F, -14.5F, -0.5F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, -0.9F, -0.25F, -1F, -0.75F, -0.25F, -1F, -0.75F, -0.25F, 0F, -0.9F, -0.25F, 0F, 0.25F, -0.25F, -1F, 0.25F, -0.25F, -1F, 0.25F, -0.25F, 0F, 0.25F, -0.25F); // Box 77
		bodyModel[55].setRotationPoint(-35F, -14.5F, -2.5F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0F, -0.5F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.25F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, -0.25F, 0F); // Box 78
		bodyModel[56].setRotationPoint(-32F, -14.5F, 1.5F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0.4F, 0F, 0F, 0.65F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.1F, 0F, 0F, 0.1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 79
		bodyModel[57].setRotationPoint(-34F, -12.5F, 4.5F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, -0.5F, 0F, -0.75F, -0.5F, 0F, -0.75F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.25F, 0F, -0.75F, -0.25F, 0F, -0.75F, -0.25F, 0F, 0F, -0.25F, 0F); // Box 80
		bodyModel[58].setRotationPoint(-34F, -13.5F, -4.5F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.65F, 0F, 0F, 0.4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.1F, 0F, 0F, -0.1F); // Box 81
		bodyModel[59].setRotationPoint(-34F, -12.5F, -5.5F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 2, 3, 5, 0F,0F, 0F, -0.25F, 0.5F, -1.25F, -0.25F, 0.5F, -1.25F, -0.25F, 0F, 0F, -0.25F, -1.75F, 1F, -0.25F, 0.5F, 0F, -0.25F, 0.5F, 0F, -0.25F, -1.75F, 1F, -0.25F); // Box 82
		bodyModel[60].setRotationPoint(-29F, -12.5F, -2.5F);

		bodyModel[61].addShapeBox(0F, 0F, 0F, 13, 1, 7, 0F,0F, -0.5F, 0F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, 0F, 0F, 0F, 0F, -0.25F, 0F, -0.5F, -0.25F, 0F, -0.5F, 0F, 0F, 0F); // Box 83
		bodyModel[61].setRotationPoint(-40F, -9F, -3.5F);

		bodyModel[62].addShapeBox(0F, 0F, 0F, 2, 2, 3, 0F,0.5F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0.5F, -0.25F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F); // Box 84
		bodyModel[62].setRotationPoint(-26F, -11.5F, -1.5F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F,0F, 0F, -1.75F, -1.5F, -0.75F, -2F, -1.5F, -0.75F, 0.25F, 0F, 0F, 0.25F, 0F, 1F, -0.25F, -0.25F, 1F, -1.5F, -0.25F, 1F, 0.25F, 0F, 1F, 0.25F); // Box 85
		bodyModel[63].setRotationPoint(-29F, -12.5F, -4.5F);

		bodyModel[64].addShapeBox(0F, 0F, 0F, 2, 3, 2, 0F,0F, 0F, 0.25F, -1.5F, -0.75F, 0.25F, -1.5F, -0.75F, -2F, 0F, 0F, -1.75F, 0F, 1F, 0.25F, -0.25F, 1F, 0.25F, -0.25F, 1F, -1.5F, 0F, 1F, -0.25F); // Box 86
		bodyModel[64].setRotationPoint(-29F, -12.5F, 2.5F);

		bodyModel[65].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 1.3F, 0F, 0F, 1.3F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Box 87
		bodyModel[65].setRotationPoint(-13F, -6F, -9.5F);

		bodyModel[66].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 1.3F, 0F, 0F, 1.3F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Box 90
		bodyModel[66].setRotationPoint(10F, -6F, -9.5F);

		bodyModel[67].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F,0F, 0F, 1.3F, 0F, 0F, 1.3F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 91
		bodyModel[67].setRotationPoint(-13F, -6F, 8.5F);

		bodyModel[68].addShapeBox(0F, 0F, 0F, 3, 3, 1, 0F,0F, 0F, 1.3F, 0F, 0F, 1.3F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 92
		bodyModel[68].setRotationPoint(10F, -6F, 8.5F);

		bodyModel[69].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,-0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0.2F, -0.25F, -0.25F, 0.2F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, -0.15F, -0.25F, -0.25F, -0.15F); // Box 93
		bodyModel[69].setRotationPoint(11F, -5F, -10F);

		bodyModel[70].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,-0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0.2F, -0.25F, -0.25F, 0.2F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, -0.15F, -0.25F, -0.25F, -0.15F); // Box 94
		bodyModel[70].setRotationPoint(-12F, -5F, -10F);

		bodyModel[71].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,-0.25F, -0.25F, 0.2F, -0.25F, -0.25F, 0.2F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, -0.15F, -0.25F, -0.25F, -0.15F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F); // Box 95
		bodyModel[71].setRotationPoint(-12F, -5F, 8F);

		bodyModel[72].addShapeBox(0F, 0F, 0F, 1, 1, 2, 0F,-0.25F, -0.25F, 0.2F, -0.25F, -0.25F, 0.2F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, -0.15F, -0.25F, -0.25F, -0.15F, -0.25F, -0.25F, 0F, -0.25F, -0.25F, 0F); // Box 96
		bodyModel[72].setRotationPoint(11F, -5F, 8F);

		bodyModel[73].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Box 186
		bodyModel[73].setRotationPoint(-42.5F, 4F, -10.5F);

		bodyModel[74].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Box 93
		bodyModel[74].setRotationPoint(-29F, 4F, -10.5F);

		bodyModel[75].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Box 94
		bodyModel[75].setRotationPoint(26F, 4F, -10.5F);

		bodyModel[76].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.25F, 0F, 0F, 0.25F); // Box 95
		bodyModel[76].setRotationPoint(39.5F, 4F, -10.5F);

		bodyModel[77].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 99
		bodyModel[77].setRotationPoint(-42.5F, 4F, 9.5F);

		bodyModel[78].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 100
		bodyModel[78].setRotationPoint(-29F, 4F, 9.5F);

		bodyModel[79].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 101
		bodyModel[79].setRotationPoint(26F, 4F, 9.5F);

		bodyModel[80].addShapeBox(0F, 0F, 0F, 3, 2, 1, 0F,0F, 0F, -0.15F, 0F, 0F, -0.15F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 102
		bodyModel[80].setRotationPoint(39.5F, 4F, 9.5F);

		bodyModel[81].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 99
		bodyModel[81].setRotationPoint(-2F, 0F, -11F);

		bodyModel[82].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 101
		bodyModel[82].setRotationPoint(-2F, -7F, -8.55F);

		bodyModel[83].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 102
		bodyModel[83].setRotationPoint(-2F, 4F, -11.3F);

		bodyModel[84].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 103
		bodyModel[84].setRotationPoint(-2F, -3.5F, -9.76F);

		bodyModel[85].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 104
		bodyModel[85].setRotationPoint(-2F, 4F, 10.3F);

		bodyModel[86].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 105
		bodyModel[86].setRotationPoint(-2F, -7F, 7.55F);

		bodyModel[87].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 106
		bodyModel[87].setRotationPoint(-2F, -3.5F, 8.79F);

		bodyModel[88].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 107
		bodyModel[88].setRotationPoint(-2F, 0F, 10F);

		bodyModel[89].addShapeBox(0F, 0F, 0F, 1, 0, 4, 0F,-0.05F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.05F, 0F, 0F); // Box 110
		bodyModel[89].setRotationPoint(-46.5F, -6F, -2F);

		bodyModel[90].addShapeBox(0F, 0F, 0F, 1, 0, 4, 0F,-0.05F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.05F, 0F, 0F); // Box 111
		bodyModel[90].setRotationPoint(-47.75F, -1F, -2F);

		bodyModel[91].addShapeBox(0F, 0F, 0F, 1, 0, 4, 0F,0F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0F, 0F, 0F); // Box 112
		bodyModel[91].setRotationPoint(46.75F, -1F, -2F);

		bodyModel[92].addShapeBox(0F, 0F, 0F, 1, 0, 4, 0F,0F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.05F, 0F, 0F, -0.05F, 0F, 0F, 0F, 0F, 0F); // Box 113
		bodyModel[92].setRotationPoint(45.5F, -6F, -2F);
	}
}