//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 07.11.2022 - 10:30:25
// Last changed on: 07.11.2022 - 10:30:25

package train.client.render.models; //Path where the model is located

import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.common.library.Info;

public class ModelBTRHopper extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 512;

	public ModelBTRHopper() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[99];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 33, 1, textureX, textureY); // Box 1
		bodyModel[2] = new ModelRendererTurbo(this, 201, 1, textureX, textureY); // Box 3
		bodyModel[3] = new ModelRendererTurbo(this, 225, 1, textureX, textureY); // Box 5
		bodyModel[4] = new ModelRendererTurbo(this, 393, 1, textureX, textureY); // Box 8
		bodyModel[5] = new ModelRendererTurbo(this, 441, 1, textureX, textureY); // Box 9
		bodyModel[6] = new ModelRendererTurbo(this, 33, 9, textureX, textureY); // Box 10
		bodyModel[7] = new ModelRendererTurbo(this, 225, 9, textureX, textureY); // Box 11
		bodyModel[8] = new ModelRendererTurbo(this, 81, 33, textureX, textureY); // Box 12
		bodyModel[9] = new ModelRendererTurbo(this, 233, 25, textureX, textureY); // Box 14
		bodyModel[10] = new ModelRendererTurbo(this, 81, 9, textureX, textureY); // Box 17
		bodyModel[11] = new ModelRendererTurbo(this, 129, 9, textureX, textureY); // Box 18
		bodyModel[12] = new ModelRendererTurbo(this, 177, 9, textureX, textureY); // Box 19
		bodyModel[13] = new ModelRendererTurbo(this, 417, 9, textureX, textureY); // Box 20
		bodyModel[14] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 22
		bodyModel[15] = new ModelRendererTurbo(this, 9, 1, textureX, textureY); // Box 23
		bodyModel[16] = new ModelRendererTurbo(this, 417, 1, textureX, textureY); // Box 24
		bodyModel[17] = new ModelRendererTurbo(this, 425, 1, textureX, textureY); // Box 21
		bodyModel[18] = new ModelRendererTurbo(this, 465, 1, textureX, textureY); // Box 22
		bodyModel[19] = new ModelRendererTurbo(this, 489, 1, textureX, textureY); // Box 23
		bodyModel[20] = new ModelRendererTurbo(this, 57, 9, textureX, textureY); // Box 24
		bodyModel[21] = new ModelRendererTurbo(this, 105, 9, textureX, textureY); // Box 25
		bodyModel[22] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // Box 26
		bodyModel[23] = new ModelRendererTurbo(this, 273, 33, textureX, textureY); // Box 30
		bodyModel[24] = new ModelRendererTurbo(this, 297, 41, textureX, textureY); // Box 31
		bodyModel[25] = new ModelRendererTurbo(this, 481, 1, textureX, textureY); // Box 33
		bodyModel[26] = new ModelRendererTurbo(this, 33, 9, textureX, textureY); // Box 34
		bodyModel[27] = new ModelRendererTurbo(this, 73, 9, textureX, textureY); // Box 35
		bodyModel[28] = new ModelRendererTurbo(this, 121, 9, textureX, textureY); // Box 36
		bodyModel[29] = new ModelRendererTurbo(this, 449, 1, textureX, textureY); // Box 37
		bodyModel[30] = new ModelRendererTurbo(this, 489, 17, textureX, textureY); // Box 31
		bodyModel[31] = new ModelRendererTurbo(this, 497, 17, textureX, textureY); // Box 35
		bodyModel[32] = new ModelRendererTurbo(this, 505, 17, textureX, textureY); // Box 36
		bodyModel[33] = new ModelRendererTurbo(this, 1, 25, textureX, textureY); // Box 37
		bodyModel[34] = new ModelRendererTurbo(this, 9, 25, textureX, textureY); // Box 39
		bodyModel[35] = new ModelRendererTurbo(this, 17, 25, textureX, textureY); // Box 40
		bodyModel[36] = new ModelRendererTurbo(this, 25, 25, textureX, textureY); // Box 41
		bodyModel[37] = new ModelRendererTurbo(this, 465, 33, textureX, textureY); // Box 42
		bodyModel[38] = new ModelRendererTurbo(this, 473, 33, textureX, textureY); // Box 43
		bodyModel[39] = new ModelRendererTurbo(this, 481, 33, textureX, textureY); // Box 44
		bodyModel[40] = new ModelRendererTurbo(this, 33, 41, textureX, textureY); // Box 45
		bodyModel[41] = new ModelRendererTurbo(this, 41, 41, textureX, textureY); // Box 46
		bodyModel[42] = new ModelRendererTurbo(this, 49, 41, textureX, textureY); // Box 47
		bodyModel[43] = new ModelRendererTurbo(this, 57, 41, textureX, textureY); // Box 48
		bodyModel[44] = new ModelRendererTurbo(this, 65, 41, textureX, textureY); // Box 49
		bodyModel[45] = new ModelRendererTurbo(this, 73, 41, textureX, textureY); // Box 50
		bodyModel[46] = new ModelRendererTurbo(this, 489, 41, textureX, textureY); // Box 51
		bodyModel[47] = new ModelRendererTurbo(this, 497, 41, textureX, textureY); // Box 52
		bodyModel[48] = new ModelRendererTurbo(this, 505, 41, textureX, textureY); // Box 53
		bodyModel[49] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // Box 54
		bodyModel[50] = new ModelRendererTurbo(this, 9, 49, textureX, textureY); // Box 55
		bodyModel[51] = new ModelRendererTurbo(this, 17, 49, textureX, textureY); // Box 56
		bodyModel[52] = new ModelRendererTurbo(this, 25, 49, textureX, textureY); // Box 57
		bodyModel[53] = new ModelRendererTurbo(this, 81, 49, textureX, textureY); // Box 58
		bodyModel[54] = new ModelRendererTurbo(this, 89, 49, textureX, textureY); // Box 59
		bodyModel[55] = new ModelRendererTurbo(this, 97, 49, textureX, textureY); // Box 60
		bodyModel[56] = new ModelRendererTurbo(this, 105, 57, textureX, textureY); // Box 61
		bodyModel[57] = new ModelRendererTurbo(this, 289, 57, textureX, textureY); // Box 62
		bodyModel[58] = new ModelRendererTurbo(this, 105, 65, textureX, textureY); // Box 63
		bodyModel[59] = new ModelRendererTurbo(this, 297, 65, textureX, textureY); // Box 64
		bodyModel[60] = new ModelRendererTurbo(this, 89, 9, textureX, textureY); // Box 65
		bodyModel[61] = new ModelRendererTurbo(this, 33, 65, textureX, textureY); // Box 71
		bodyModel[62] = new ModelRendererTurbo(this, 465, 1, textureX, textureY); // Box 83
		bodyModel[63] = new ModelRendererTurbo(this, 505, 1, textureX, textureY); // Box 86
		bodyModel[64] = new ModelRendererTurbo(this, 25, 73, textureX, textureY); // Box 87
		bodyModel[65] = new ModelRendererTurbo(this, 97, 73, textureX, textureY); // Box 82
		bodyModel[66] = new ModelRendererTurbo(this, 465, 65, textureX, textureY); // Box 83
		bodyModel[67] = new ModelRendererTurbo(this, 137, 9, textureX, textureY); // Box 84
		bodyModel[68] = new ModelRendererTurbo(this, 153, 9, textureX, textureY); // Box 85
		bodyModel[69] = new ModelRendererTurbo(this, 161, 9, textureX, textureY); // Box 86
		bodyModel[70] = new ModelRendererTurbo(this, 73, 65, textureX, textureY); // Box 87
		bodyModel[71] = new ModelRendererTurbo(this, 169, 9, textureX, textureY); // Box 88
		bodyModel[72] = new ModelRendererTurbo(this, 185, 9, textureX, textureY); // Box 89
		bodyModel[73] = new ModelRendererTurbo(this, 201, 9, textureX, textureY); // Box 90
		bodyModel[74] = new ModelRendererTurbo(this, 393, 9, textureX, textureY); // Box 91
		bodyModel[75] = new ModelRendererTurbo(this, 417, 9, textureX, textureY); // Box 92
		bodyModel[76] = new ModelRendererTurbo(this, 441, 9, textureX, textureY); // Box 93
		bodyModel[77] = new ModelRendererTurbo(this, 81, 17, textureX, textureY); // Box 94
		bodyModel[78] = new ModelRendererTurbo(this, 1, 73, textureX, textureY); // Box 95
		bodyModel[79] = new ModelRendererTurbo(this, 9, 73, textureX, textureY); // Box 96
		bodyModel[80] = new ModelRendererTurbo(this, 169, 17, textureX, textureY); // Box 97
		bodyModel[81] = new ModelRendererTurbo(this, 185, 17, textureX, textureY); // Box 98
		bodyModel[82] = new ModelRendererTurbo(this, 241, 25, textureX, textureY); // Box 99
		bodyModel[83] = new ModelRendererTurbo(this, 497, 65, textureX, textureY); // Box 100
		bodyModel[84] = new ModelRendererTurbo(this, 505, 65, textureX, textureY); // Box 101
		bodyModel[85] = new ModelRendererTurbo(this, 17, 73, textureX, textureY); // Box 102
		bodyModel[86] = new ModelRendererTurbo(this, 273, 25, textureX, textureY); // Box 103
		bodyModel[87] = new ModelRendererTurbo(this, 289, 25, textureX, textureY); // Box 104
		bodyModel[88] = new ModelRendererTurbo(this, 305, 25, textureX, textureY); // Box 105
		bodyModel[89] = new ModelRendererTurbo(this, 57, 9, textureX, textureY); // Box 109
		bodyModel[90] = new ModelRendererTurbo(this, 105, 9, textureX, textureY); // Box 110
		bodyModel[91] = new ModelRendererTurbo(this, 177, 9, textureX, textureY); // Box 111
		bodyModel[92] = new ModelRendererTurbo(this, 209, 9, textureX, textureY); // Box 112
		bodyModel[93] = new ModelRendererTurbo(this, 129, 17, textureX, textureY); // Box 113
		bodyModel[94] = new ModelRendererTurbo(this, 345, 25, textureX, textureY); // Box 117
		bodyModel[95] = new ModelRendererTurbo(this, 401, 9, textureX, textureY); // Box 118
		bodyModel[96] = new ModelRendererTurbo(this, 353, 25, textureX, textureY); // Box 119
		bodyModel[97] = new ModelRendererTurbo(this, 425, 9, textureX, textureY); // Box 120
		bodyModel[98] = new ModelRendererTurbo(this, 129, 17, textureX, textureY); // Box 121

		bodyModel[0].addBox(0F, 0F, 0F, 3, 1, 20, 0F); // Box 0
		bodyModel[0].setRotationPoint(-44F, -5F, -10F);

		bodyModel[1].addBox(0F, 0F, 0F, 92, 6, 1, 0F); // Box 1
		bodyModel[1].setRotationPoint(-46F, -26F, -11F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 1, 6, 20, 0F,5F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 3
		bodyModel[2].setRotationPoint(-41F, -20F, -10F);

		bodyModel[3].addBox(0F, 0F, 0F, 92, 6, 1, 0F); // Box 5
		bodyModel[3].setRotationPoint(-46F, -26F, 10F);

		bodyModel[4].addBox(0F, 0F, 0F, 1, 6, 20, 0F); // Box 8
		bodyModel[4].setRotationPoint(-46F, -26F, -10F);

		bodyModel[5].addBox(0F, 0F, 0F, 1, 6, 20, 0F); // Box 9
		bodyModel[5].setRotationPoint(45F, -26F, -10F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 1, 6, 20, 0F,-5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		bodyModel[6].setRotationPoint(40F, -20F, -10F);

		bodyModel[7].addBox(0F, 0F, 0F, 82, 9, 1, 0F); // Box 11
		bodyModel[7].setRotationPoint(-41F, -14F, -11F);

		bodyModel[8].addBox(0F, 0F, 0F, 82, 9, 1, 0F); // Box 12
		bodyModel[8].setRotationPoint(-41F, -14F, 10F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 8, 10, 20, 0F,0F, 0F, 0F, -7F, 0F, 0F, -7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 18F, 0F, 0F, 18F, 0F, 0F, 0F, 0F, 0F); // Box 14
		bodyModel[9].setRotationPoint(-41F, -14F, -10F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 1, 3, 20, 0F,5F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 17
		bodyModel[10].setRotationPoint(-11F, -4F, -10F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 1, 3, 20, 0F,-5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 18
		bodyModel[11].setRotationPoint(-11F, -4F, -10F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 1, 3, 20, 0F,-5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		bodyModel[12].setRotationPoint(10F, -4F, -10F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 1, 3, 20, 0F,5F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 20
		bodyModel[13].setRotationPoint(10F, -4F, -10F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 22
		bodyModel[14].setRotationPoint(10F, -4F, -11F);

		bodyModel[15].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		bodyModel[15].setRotationPoint(10F, -4F, 10F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		bodyModel[16].setRotationPoint(-11F, -4F, 10F);

		bodyModel[17].addBox(0F, 0F, 0F, 7, 2, 2, 0F); // Box 21
		bodyModel[17].setRotationPoint(-50F, -4F, -1F);

		bodyModel[18].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // Box 22
		bodyModel[18].setRotationPoint(-45F, -4F, 1F);

		bodyModel[19].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // Box 23
		bodyModel[19].setRotationPoint(-45F, -4F, -10F);

		bodyModel[20].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // Box 24
		bodyModel[20].setRotationPoint(43F, -4F, -10F);

		bodyModel[21].addBox(0F, 0F, 0F, 2, 2, 9, 0F); // Box 25
		bodyModel[21].setRotationPoint(43F, -4F, 1F);

		bodyModel[22].addBox(0F, 0F, 0F, 7, 2, 2, 0F); // Box 26
		bodyModel[22].setRotationPoint(43F, -4F, -1F);

		bodyModel[23].addBox(0F, 0F, 0F, 82, 6, 1, 0F); // Box 30
		bodyModel[23].setRotationPoint(-41F, -20F, -11F);

		bodyModel[24].addBox(0F, 0F, 0F, 82, 6, 1, 0F); // Box 31
		bodyModel[24].setRotationPoint(-41F, -20F, 10F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 5, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F); // Box 33
		bodyModel[25].setRotationPoint(-46F, -20F, -11F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 5, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F); // Box 34
		bodyModel[26].setRotationPoint(41F, -20F, -11F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 5, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F); // Box 35
		bodyModel[27].setRotationPoint(41F, -20F, 10F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 5, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -5F, 0F, 0F); // Box 36
		bodyModel[28].setRotationPoint(-46F, -20F, 10F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 37
		bodyModel[29].setRotationPoint(-11F, -4F, -11F);

		bodyModel[30].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 31
		bodyModel[30].setRotationPoint(-40F, -25F, 10.5F);

		bodyModel[31].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 35
		bodyModel[31].setRotationPoint(-19F, -25F, 10.5F);

		bodyModel[32].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 36
		bodyModel[32].setRotationPoint(-26F, -25F, 10.5F);

		bodyModel[33].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 37
		bodyModel[33].setRotationPoint(-33F, -25F, 10.5F);

		bodyModel[34].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 39
		bodyModel[34].setRotationPoint(-12F, -25F, 10.5F);

		bodyModel[35].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 40
		bodyModel[35].setRotationPoint(-5F, -25F, 10.5F);

		bodyModel[36].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 41
		bodyModel[36].setRotationPoint(2F, -25F, 10.5F);

		bodyModel[37].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 42
		bodyModel[37].setRotationPoint(9F, -25F, 10.5F);

		bodyModel[38].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 43
		bodyModel[38].setRotationPoint(16F, -25F, 10.5F);

		bodyModel[39].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 44
		bodyModel[39].setRotationPoint(23F, -25F, 10.5F);

		bodyModel[40].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 45
		bodyModel[40].setRotationPoint(29F, -25F, 10.5F);

		bodyModel[41].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 46
		bodyModel[41].setRotationPoint(35F, -25F, 10.5F);

		bodyModel[42].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 47
		bodyModel[42].setRotationPoint(40F, -25F, 10.5F);

		bodyModel[43].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 48
		bodyModel[43].setRotationPoint(35F, -25F, -11.5F);

		bodyModel[44].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 49
		bodyModel[44].setRotationPoint(-40F, -25F, -11.5F);

		bodyModel[45].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 50
		bodyModel[45].setRotationPoint(-33F, -25F, -11.5F);

		bodyModel[46].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 51
		bodyModel[46].setRotationPoint(-26F, -25F, -11.5F);

		bodyModel[47].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 52
		bodyModel[47].setRotationPoint(-19F, -25F, -11.5F);

		bodyModel[48].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 53
		bodyModel[48].setRotationPoint(-5F, -25F, -11.5F);

		bodyModel[49].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 54
		bodyModel[49].setRotationPoint(-12F, -25F, -11.5F);

		bodyModel[50].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 55
		bodyModel[50].setRotationPoint(2F, -25F, -11.5F);

		bodyModel[51].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 56
		bodyModel[51].setRotationPoint(9F, -25F, -11.5F);

		bodyModel[52].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 57
		bodyModel[52].setRotationPoint(23F, -25F, -11.5F);

		bodyModel[53].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 58
		bodyModel[53].setRotationPoint(16F, -25F, -11.5F);

		bodyModel[54].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 59
		bodyModel[54].setRotationPoint(29F, -25F, -11.5F);

		bodyModel[55].addBox(0F, 0F, 0F, 1, 20, 1, 0F); // Box 60
		bodyModel[55].setRotationPoint(40F, -25F, -11.5F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 88, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 61
		bodyModel[56].setRotationPoint(-44F, -5F, -11.5F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 88, 1, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 62
		bodyModel[57].setRotationPoint(-44F, -5F, 10F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 92, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 63
		bodyModel[58].setRotationPoint(-46F, -26F, 11F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 92, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 64
		bodyModel[59].setRotationPoint(-46F, -26F, -11.5F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 65
		bodyModel[60].setRotationPoint(-46.65F, -25.75F, 5F);

		bodyModel[61].addShapeBox(0F, 0F, 0F, 1, 23, 1, 0F,-0.25F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F); // Box 71
		bodyModel[61].setRotationPoint(-47F, -26F, 4F);

		bodyModel[62].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 83
		bodyModel[62].setRotationPoint(-44F, -8F, 10F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		bodyModel[63].setRotationPoint(43F, -8F, -11.5F);

		bodyModel[64].addShapeBox(0F, 0F, 0F, 12, 5, 20, 0F,-6F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, -6F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 87
		bodyModel[64].setRotationPoint(-6F, -9F, -10F);

		bodyModel[65].addShapeBox(0F, 0F, 0F, 8, 10, 20, 0F,-7F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -7F, 0F, 0F, 18F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 18F, 0F, 0F); // Box 82
		bodyModel[65].setRotationPoint(33F, -14F, -10F);

		bodyModel[66].addBox(0F, 0F, 0F, 3, 1, 20, 0F); // Box 83
		bodyModel[66].setRotationPoint(41F, -5F, -10F);

		bodyModel[67].addShapeBox(0F, 0F, 0F, 1, 13, 1, 0F,0F, -0.3F, 0F, 0F, -1.55F, 0F, 0F, -1.55F, 0F, 0F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 84
		bodyModel[67].setRotationPoint(-44F, -18F, 8.5F);

		bodyModel[68].addShapeBox(0F, 0F, 0F, 1, 13, 1, 0F,0F, -0.3F, 0F, 0F, -1.55F, 0F, 0F, -1.55F, 0F, 0F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 85
		bodyModel[68].setRotationPoint(-44F, -18F, -0.5F);

		bodyModel[69].addShapeBox(0F, 0F, 0F, 1, 13, 1, 0F,0F, -0.3F, 0F, 0F, -1.55F, 0F, 0F, -1.55F, 0F, 0F, -0.3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 86
		bodyModel[69].setRotationPoint(-44F, -18F, -9.5F);

		bodyModel[70].addShapeBox(0F, 0F, 0F, 1, 23, 1, 0F,-0.25F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F); // Box 87
		bodyModel[70].setRotationPoint(-47F, -26F, 8.5F);

		bodyModel[71].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 88
		bodyModel[71].setRotationPoint(-46.65F, -22.75F, 5F);

		bodyModel[72].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 89
		bodyModel[72].setRotationPoint(-46.65F, -19.75F, 5F);

		bodyModel[73].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 90
		bodyModel[73].setRotationPoint(-46.65F, -16.75F, 5F);

		bodyModel[74].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 91
		bodyModel[74].setRotationPoint(-46.65F, -13.75F, 5F);

		bodyModel[75].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 92
		bodyModel[75].setRotationPoint(-46.65F, -10.75F, 5F);

		bodyModel[76].addShapeBox(0F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 93
		bodyModel[76].setRotationPoint(-46.65F, -7.75F, 5F);

		bodyModel[77].addShapeBox(-2F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 94
		bodyModel[77].setRotationPoint(48.35F, -16.75F, -9F);

		bodyModel[78].addShapeBox(-2F, 0F, 0F, 1, 23, 1, 0F,-0.25F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F); // Box 95
		bodyModel[78].setRotationPoint(48F, -26F, -10F);

		bodyModel[79].addShapeBox(-2F, 0F, 0F, 1, 23, 1, 0F,-0.25F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.25F, 0F, 0F); // Box 96
		bodyModel[79].setRotationPoint(48F, -26F, -5.5F);

		bodyModel[80].addShapeBox(-2F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 97
		bodyModel[80].setRotationPoint(48.35F, -19.75F, -9F);

		bodyModel[81].addShapeBox(-2F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 98
		bodyModel[81].setRotationPoint(48.35F, -22.75F, -9F);

		bodyModel[82].addShapeBox(-2F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 99
		bodyModel[82].setRotationPoint(48.35F, -25.75F, -9F);

		bodyModel[83].addShapeBox(0F, 0F, 0F, 1, 13, 1, 0F,0F, -1.55F, 0F, 0F, -0.3F, 0F, 0F, -0.3F, 0F, 0F, -1.55F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 100
		bodyModel[83].setRotationPoint(43F, -18F, -0.5F);

		bodyModel[84].addShapeBox(0F, 0F, 0F, 1, 13, 1, 0F,0F, -1.55F, 0F, 0F, -0.3F, 0F, 0F, -0.3F, 0F, 0F, -1.55F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 101
		bodyModel[84].setRotationPoint(43F, -18F, -9.5F);

		bodyModel[85].addShapeBox(0F, 0F, 0F, 1, 13, 1, 0F,0F, -1.55F, 0F, 0F, -0.3F, 0F, 0F, -0.3F, 0F, 0F, -1.55F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 102
		bodyModel[85].setRotationPoint(43F, -18F, 8.5F);

		bodyModel[86].addShapeBox(-2F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 103
		bodyModel[86].setRotationPoint(48.35F, -7.75F, -9F);

		bodyModel[87].addShapeBox(-2F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 104
		bodyModel[87].setRotationPoint(48.35F, -10.75F, -9F);

		bodyModel[88].addShapeBox(-2F, 0F, 0F, 1, 1, 4, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 105
		bodyModel[88].setRotationPoint(48.35F, -13.75F, -9F);

		bodyModel[89].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 109
		bodyModel[89].setRotationPoint(-45F, -9F, 11F);

		bodyModel[90].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 110
		bodyModel[90].setRotationPoint(-42.5F, -9F, 11F);

		bodyModel[91].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 111
		bodyModel[91].setRotationPoint(-44.5F, -9F, 11F);

		bodyModel[92].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 112
		bodyModel[92].setRotationPoint(-44.5F, -6.5F, 11F);

		bodyModel[93].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,-0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -1.75F, -0.5F, 0F, 1.25F, -0.5F, 0F, 1.25F, -0.5F, -0.5F, -1.75F, -0.5F, -0.5F); // Box 113
		bodyModel[93].setRotationPoint(-44.75F, -9F, 11F);

		bodyModel[94].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 117
		bodyModel[94].setRotationPoint(44.5F, -9F, -11.5F);

		bodyModel[95].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 118
		bodyModel[95].setRotationPoint(42.5F, -6.5F, -11.5F);

		bodyModel[96].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 119
		bodyModel[96].setRotationPoint(42F, -9F, -11.5F);

		bodyModel[97].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 120
		bodyModel[97].setRotationPoint(42.5F, -9F, -11.5F);

		bodyModel[98].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,-0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -1.75F, -0.5F, 0F, 1.25F, -0.5F, 0F, 1.25F, -0.5F, -0.5F, -1.75F, -0.5F, -0.5F); // Box 121
		bodyModel[98].setRotationPoint(42.25F, -9F, -11.5F);
	}

	private ModelFreightTruckM trucks = new ModelFreightTruckM();

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		for(int i = 0; i < 99; i++)
		{
			bodyModel[i].render(f5);
		}
		Tessellator.bindTexture(new ResourceLocation(Info.resourceLocation, "textures/trains/freighttruckm.png"));

		GL11.glPushMatrix();
		GL11.glTranslated(-2.5,-0.43,-0.2);
		trucks.render(entity,f,f1,f2,f3,f4,f5);

		GL11.glTranslated(3.5,0,0.00);
		trucks.render(entity,f,f1,f2,f3,f4,f5);
		GL11.glPopMatrix();
	}

	public float[] getTrans() { return new float[]{-0F, -0.25F, 0F}; }
}