//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 27.12.2021 - 16:12:03
// Last changed on: 27.12.2021 - 16:12:03

package train.client.render.models;

import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.common.library.Info;

public class ModelKozmaMininTender extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 128;

	public ModelKozmaMininTender() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[64];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}
	private ModelKozmaMininTenderBogie trucks = new ModelKozmaMininTenderBogie();

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity,f,f1,f2,f3,f4,f5);
		Tessellator.bindTexture(new ResourceLocation(Info.resourceLocation, "textures/trains/Kozma_Minin_Tender_Bogie_texture.png"));
		GL11.glPushMatrix();
		GL11.glTranslated(0.9375,0,0);
		trucks.render(entity,f,f1,f2,f3,f4,f5);
		GL11.glTranslated(-1.8125,0,0);
		trucks.render(entity,f,f1,f2,f3,f4,f5);
		GL11.glPopMatrix();
	}
	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 48
		bodyModel[1] = new ModelRendererTurbo(this, 9, 1, textureX, textureY); // Box 148
		bodyModel[2] = new ModelRendererTurbo(this, 17, 1, textureX, textureY); // Box 149
		bodyModel[3] = new ModelRendererTurbo(this, 25, 1, textureX, textureY); // Box 150
		bodyModel[4] = new ModelRendererTurbo(this, 33, 1, textureX, textureY); // Box 151
		bodyModel[5] = new ModelRendererTurbo(this, 41, 1, textureX, textureY); // Box 152
		bodyModel[6] = new ModelRendererTurbo(this, 49, 1, textureX, textureY); // Box 153
		bodyModel[7] = new ModelRendererTurbo(this, 57, 1, textureX, textureY); // Box 154
		bodyModel[8] = new ModelRendererTurbo(this, 65, 1, textureX, textureY); // Box 155
		bodyModel[9] = new ModelRendererTurbo(this, 137, 1, textureX, textureY); // Box 157
		bodyModel[10] = new ModelRendererTurbo(this, 233, 1, textureX, textureY); // Box 158
		bodyModel[11] = new ModelRendererTurbo(this, 281, 1, textureX, textureY); // Box 160
		bodyModel[12] = new ModelRendererTurbo(this, 305, 1, textureX, textureY); // Box 161
		bodyModel[13] = new ModelRendererTurbo(this, 401, 1, textureX, textureY); // Box 162
		bodyModel[14] = new ModelRendererTurbo(this, 257, 1, textureX, textureY); // Box 163
		bodyModel[15] = new ModelRendererTurbo(this, 281, 1, textureX, textureY); // Box 164
		bodyModel[16] = new ModelRendererTurbo(this, 441, 1, textureX, textureY); // Box 165
		bodyModel[17] = new ModelRendererTurbo(this, 457, 1, textureX, textureY); // Box 166
		bodyModel[18] = new ModelRendererTurbo(this, 465, 1, textureX, textureY); // Box 167
		bodyModel[19] = new ModelRendererTurbo(this, 489, 1, textureX, textureY); // Box 168
		bodyModel[20] = new ModelRendererTurbo(this, 1, 9, textureX, textureY); // Box 169
		bodyModel[21] = new ModelRendererTurbo(this, 145, 9, textureX, textureY); // Box 170
		bodyModel[22] = new ModelRendererTurbo(this, 121, 9, textureX, textureY); // Box 171
		bodyModel[23] = new ModelRendererTurbo(this, 481, 1, textureX, textureY); // Box 172
		bodyModel[24] = new ModelRendererTurbo(this, 209, 9, textureX, textureY); // Box 173
		bodyModel[25] = new ModelRendererTurbo(this, 225, 9, textureX, textureY); // Box 174
		bodyModel[26] = new ModelRendererTurbo(this, 241, 9, textureX, textureY); // Box 175
		bodyModel[27] = new ModelRendererTurbo(this, 305, 9, textureX, textureY); // Box 176
		bodyModel[28] = new ModelRendererTurbo(this, 313, 9, textureX, textureY); // Box 177
		bodyModel[29] = new ModelRendererTurbo(this, 321, 9, textureX, textureY); // Box 178
		bodyModel[30] = new ModelRendererTurbo(this, 329, 9, textureX, textureY); // Box 179
		bodyModel[31] = new ModelRendererTurbo(this, 217, 9, textureX, textureY); // Box 180
		bodyModel[32] = new ModelRendererTurbo(this, 321, 9, textureX, textureY); // Box 183
		bodyModel[33] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Box 184
		bodyModel[34] = new ModelRendererTurbo(this, 361, 9, textureX, textureY); // Box 185
		bodyModel[35] = new ModelRendererTurbo(this, 377, 9, textureX, textureY); // Box 186
		bodyModel[36] = new ModelRendererTurbo(this, 153, 25, textureX, textureY); // Box 187
		bodyModel[37] = new ModelRendererTurbo(this, 377, 25, textureX, textureY); // Box 188
		bodyModel[38] = new ModelRendererTurbo(this, 393, 9, textureX, textureY); // Box 189
		bodyModel[39] = new ModelRendererTurbo(this, 401, 9, textureX, textureY); // Box 190
		bodyModel[40] = new ModelRendererTurbo(this, 409, 9, textureX, textureY); // Box 155
		bodyModel[41] = new ModelRendererTurbo(this, 441, 9, textureX, textureY); // Box 157
		bodyModel[42] = new ModelRendererTurbo(this, 65, 1, textureX, textureY); // Box 171
		bodyModel[43] = new ModelRendererTurbo(this, 1, 17, textureX, textureY); // Box 180
		bodyModel[44] = new ModelRendererTurbo(this, 49, 17, textureX, textureY); // Box 181
		bodyModel[45] = new ModelRendererTurbo(this, 505, 1, textureX, textureY); // Box 182
		bodyModel[46] = new ModelRendererTurbo(this, 457, 9, textureX, textureY); // Box 183
		bodyModel[47] = new ModelRendererTurbo(this, 233, 9, textureX, textureY); // Box 184
		bodyModel[48] = new ModelRendererTurbo(this, 9, 17, textureX, textureY); // Box 185
		bodyModel[49] = new ModelRendererTurbo(this, 489, 1, textureX, textureY); // Box 175
		bodyModel[50] = new ModelRendererTurbo(this, 145, 17, textureX, textureY); // Box 176
		bodyModel[51] = new ModelRendererTurbo(this, 153, 17, textureX, textureY); // Box 177
		bodyModel[52] = new ModelRendererTurbo(this, 161, 17, textureX, textureY); // Box 178
		bodyModel[53] = new ModelRendererTurbo(this, 457, 17, textureX, textureY); // Box 179
		bodyModel[54] = new ModelRendererTurbo(this, 465, 17, textureX, textureY); // Box 180
		bodyModel[55] = new ModelRendererTurbo(this, 473, 17, textureX, textureY); // Box 181
		bodyModel[56] = new ModelRendererTurbo(this, 169, 17, textureX, textureY); // Box 173
		bodyModel[57] = new ModelRendererTurbo(this, 177, 17, textureX, textureY); // Box 174
		bodyModel[58] = new ModelRendererTurbo(this, 185, 17, textureX, textureY); // Box 175
		bodyModel[59] = new ModelRendererTurbo(this, 193, 17, textureX, textureY); // Box 177
		bodyModel[60] = new ModelRendererTurbo(this, 201, 17, textureX, textureY); // Box 178
		bodyModel[61] = new ModelRendererTurbo(this, 209, 17, textureX, textureY); // Box 179
		bodyModel[62] = new ModelRendererTurbo(this, 217, 17, textureX, textureY); // Box 180
		bodyModel[63] = new ModelRendererTurbo(this, 65, 25, textureX, textureY); // Box 197

		bodyModel[0].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0.25F, 0F, -0.75F, 0.5F, 0F, -0.75F, 0.5F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, -0.75F, 0.5F, 0F, -0.75F, 0.5F, 0F, 0F, 0.25F, 0F, 0F); // Box 48
		bodyModel[0].setRotationPoint(-18.5F, 5F, -10.5F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1.25F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1.25F, 0F, -0.25F); // Box 148
		bodyModel[1].setRotationPoint(-28.53F, 2F, -0.5F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,1F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F, -1F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 149
		bodyModel[2].setRotationPoint(-29.78F, 3.01F, -0.5F);

		bodyModel[3].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 150
		bodyModel[3].setRotationPoint(-30.5F, 3F, -4.5F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F); // Box 151
		bodyModel[4].setRotationPoint(-31.5F, 2F, -5.5F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 152
		bodyModel[5].setRotationPoint(-30.78F, 2.01F, -0.5F);

		bodyModel[6].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 153
		bodyModel[6].setRotationPoint(-30.5F, 3F, 3.5F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,-0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F); // Box 154
		bodyModel[7].setRotationPoint(-31.5F, 2F, 2.5F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 29, 14, 7, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -6.5F, 0F, 0F, -6.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 155
		bodyModel[8].setRotationPoint(-11.5F, -12F, -9.5F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 54, 5, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 157
		bodyModel[9].setRotationPoint(-26.5F, 2F, -9.5F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 1, 6, 19, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 158
		bodyModel[10].setRotationPoint(-27.5F, 1.5F, -9.5F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 1, 5, 19, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 160
		bodyModel[11].setRotationPoint(27.5F, 2.5F, -9.5F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 54, 5, 1, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 161
		bodyModel[12].setRotationPoint(-26.5F, 2F, 8.5F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 8, 1, 19, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 162
		bodyModel[13].setRotationPoint(-27.5F, -16.5F, -9.5F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 8, 17, 1, 0F,0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 163
		bodyModel[14].setRotationPoint(-27.5F, -15.5F, -9.5F);

		bodyModel[15].addShapeBox(0F, 0F, 0F, 8, 17, 1, 0F,0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 164
		bodyModel[15].setRotationPoint(-27.5F, -15.5F, 8.5F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F,0F, 0.2F, -0.75F, 0F, 0.2F, -0.75F, 0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 165
		bodyModel[16].setRotationPoint(-29.5F, 2F, -10.5F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F,0F, 0.2F, 0F, 0F, 0.2F, 0F, 0F, 0.2F, -0.75F, 0F, 0.2F, -0.75F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.75F, 0F, -0.5F, -0.75F); // Box 166
		bodyModel[17].setRotationPoint(-29.5F, 2F, 9.5F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 4, 3, 7, 0F,0F, 0.25F, -1.5F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F); // Box 167
		bodyModel[18].setRotationPoint(-19.5F, -19.5F, -3.5F);

		bodyModel[19].addShapeBox(0F, 0F, 0F, 4, 3, 7, 0F,0F, 0.25F, 0F, 0F, 0.25F, -1.5F, 0F, 0.25F, -1.5F, 0F, 0.25F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F); // Box 168
		bodyModel[19].setRotationPoint(-15.5F, -19.5F, -3.5F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 30, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 169
		bodyModel[20].setRotationPoint(-11.5F, -14.5F, -9.5F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 30, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 170
		bodyModel[21].setRotationPoint(-11.5F, -14.5F, 8.5F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 1, 3, 17, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 171
		bodyModel[22].setRotationPoint(17.5F, -14.5F, -8.5F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 172
		bodyModel[23].setRotationPoint(-25.5F, 2.5F, -9.76F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 173
		bodyModel[24].setRotationPoint(-25.5F, 5.5F, -9.76F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 174
		bodyModel[25].setRotationPoint(-25.5F, 2.5F, 8.75F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 4, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 175
		bodyModel[26].setRotationPoint(-25.5F, 5.5F, 8.75F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 3, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 176
		bodyModel[27].setRotationPoint(23.5F, 2.5F, 8.75F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 3, 0, 1, 0F,0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F); // Box 177
		bodyModel[28].setRotationPoint(23.5F, 5.5F, 8.75F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 3, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 178
		bodyModel[29].setRotationPoint(23.5F, 2.5F, -9.76F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 3, 0, 1, 0F,0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -0.75F, 0F, 0F, -0.75F); // Box 179
		bodyModel[30].setRotationPoint(23.5F, 5.5F, -9.76F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 2, 0, 4, 0F,0.7F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0.7F, 0F, 0F, 0.7F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0.7F, 0F, 0F); // Box 180
		bodyModel[31].setRotationPoint(-28.5F, 1.5F, -2F);

		bodyModel[32].addShapeBox(0F, 0F, 0F, 8, 19, 19, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 183
		bodyModel[32].setRotationPoint(-19.5F, -17F, -9.5F);

		bodyModel[33].addShapeBox(0F, 0F, 0F, 11, 14, 19, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 184
		bodyModel[33].setRotationPoint(17.5F, -12F, -9.5F);

		bodyModel[34].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F,-0.25F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.25F, 0.5F, 0F); // Box 185
		bodyModel[34].setRotationPoint(21.1F, -13F, -6.5F);

		bodyModel[35].addShapeBox(0F, 0F, 0F, 3, 1, 3, 0F,-0.25F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.25F, 0.5F, 0F); // Box 186
		bodyModel[35].setRotationPoint(21.1F, -13F, 2.5F);

		bodyModel[36].addShapeBox(0F, 0F, 0F, 29, 14, 7, 0F,0F, -6.5F, 0F, 0F, -6.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 187
		bodyModel[36].setRotationPoint(-11.5F, -12F, 2.5F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 29, 8, 5, 0F,0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F); // Box 188
		bodyModel[37].setRotationPoint(-11.5F, -6F, -2.5F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F,0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 189
		bodyModel[38].setRotationPoint(-26.5F, -7F, -10.5F);

		bodyModel[39].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F,0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 190
		bodyModel[39].setRotationPoint(23.5F, -6F, -10.5F);

		bodyModel[40].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 155
		bodyModel[40].setRotationPoint(28.5F, 1F, 7.5F);

		bodyModel[41].addShapeBox(0F, 0F, 0F, 3, 6, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 157
		bodyModel[41].setRotationPoint(28.5F, 1F, -8.5F);

		bodyModel[42].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 1.25F, 0F, -0.25F, 1.25F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 171
		bodyModel[42].setRotationPoint(28.5F, 2F, -0.5F);

		bodyModel[43].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 180
		bodyModel[43].setRotationPoint(28.5F, 3F, 3.5F);

		bodyModel[44].addBox(0F, 0F, 0F, 3, 1, 1, 0F); // Box 181
		bodyModel[44].setRotationPoint(28.5F, 3F, -4.5F);

		bodyModel[45].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 1F, 0F, -0.25F, 1F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, -1F, 0F, -0.25F); // Box 182
		bodyModel[45].setRotationPoint(29.78F, 3.01F, -0.5F);

		bodyModel[46].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, -0.25F, 0F, -1F, -0.25F, 0F, -1F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F, 0F, 0F, -0.25F); // Box 183
		bodyModel[46].setRotationPoint(30.78F, 2.01F, -0.5F);

		bodyModel[47].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 184
		bodyModel[47].setRotationPoint(31.5F, 2F, 2.5F);

		bodyModel[48].addShapeBox(0F, 0F, 0F, 1, 3, 3, 0F,0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 185
		bodyModel[48].setRotationPoint(31.5F, 2F, -5.5F);

		bodyModel[49].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,-0.25F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, 0.5F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0F, -0.25F, 0.5F, 0F); // Box 175
		bodyModel[49].setRotationPoint(28.25F, -8F, -1.5F);

		bodyModel[50].addShapeBox(0F, 0F, 0F, 1, 2, 2, 0F,-0.5F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, 0.5F, 0F, -0.25F, 0.5F, 0F, -0.25F, 0.5F, 0F, -0.5F, 0.5F, 0F); // Box 176
		bodyModel[50].setRotationPoint(-20.25F, -8F, -1.5F);

		bodyModel[51].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F,0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 177
		bodyModel[51].setRotationPoint(-26.5F, -7F, 9.5F);

		bodyModel[52].addShapeBox(0F, 0F, 0F, 1, 6, 1, 0F,0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 178
		bodyModel[52].setRotationPoint(23.5F, -6F, 9.5F);

		bodyModel[53].addShapeBox(0F, 0F, 0F, 1, 16, 1, 0F,0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 179
		bodyModel[53].setRotationPoint(-28.5F, -14.5F, -8.5F);

		bodyModel[54].addShapeBox(0F, 0F, 0F, 1, 16, 1, 0F,0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 180
		bodyModel[54].setRotationPoint(-28.5F, -14.5F, 7.5F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 1, 1, 17, 0F,0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 181
		bodyModel[55].setRotationPoint(-28.5F, -15.5F, -8.5F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0.25F, 0F, -0.75F, 0.5F, 0F, -0.75F, 0.5F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, -0.75F, 0.5F, 0F, -0.75F, 0.5F, 0F, 0F, 0.25F, 0F, 0F); // Box 173
		bodyModel[56].setRotationPoint(-7.5F, 5F, -10.5F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0.25F, 0F, -0.75F, 0.5F, 0F, -0.75F, 0.5F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, -0.75F, 0.5F, 0F, -0.75F, 0.5F, 0F, 0F, 0.25F, 0F, 0F); // Box 174
		bodyModel[57].setRotationPoint(20.5F, 5F, -10.5F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0.25F, 0F, -0.75F, 0.5F, 0F, -0.75F, 0.5F, 0F, 0F, 0.25F, 0F, 0F, 0.25F, 0F, -0.75F, 0.5F, 0F, -0.75F, 0.5F, 0F, 0F, 0.25F, 0F, 0F); // Box 175
		bodyModel[58].setRotationPoint(9.5F, 5F, -10.5F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0.25F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.75F, 0.25F, 0F, -0.75F, 0.25F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.75F, 0.25F, 0F, -0.75F); // Box 177
		bodyModel[59].setRotationPoint(-7.5F, 5F, 9.5F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0.25F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.75F, 0.25F, 0F, -0.75F, 0.25F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.75F, 0.25F, 0F, -0.75F); // Box 178
		bodyModel[60].setRotationPoint(-18.5F, 5F, 9.5F);

		bodyModel[61].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0.25F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.75F, 0.25F, 0F, -0.75F, 0.25F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.75F, 0.25F, 0F, -0.75F); // Box 179
		bodyModel[61].setRotationPoint(9.5F, 5F, 9.5F);

		bodyModel[62].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0.25F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.75F, 0.25F, 0F, -0.75F, 0.25F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, -0.75F, 0.25F, 0F, -0.75F); // Box 180
		bodyModel[62].setRotationPoint(20.5F, 5F, 9.5F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 7, 1, 19, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 197
		bodyModel[63].setRotationPoint(-26.5F, 1.5F, -9.5F);
	}
}