package train.client.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import train.common.Traincraft;
import train.common.api.EntityRollingStock;
import train.common.core.network.PacketSetTrainLockedToClient;
import train.common.library.Info;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author 02skaplan
 * <p>Lock and Trusted Players Menu</p>
 * <p>Allows players to lock and unlock a piece of rolling stock and add & remove trusted individuals from using the rolling stock.</p>
 */
@SuppressWarnings("unchecked")
@SideOnly(Side.CLIENT)
public class GuiLockMenu extends GuiScreen {
    private final EntityPlayer editingPlayer;
    private final EntityRollingStock rollingStock;
    final private int MENU_TEXTURE_WIDTH = 176;
    final private int MENU_TEXTURE_HEIGHT = 222;
    /**
     * X-coordinate of top left of GUI.
     */
    private int GUI_ANCHOR_X;
    /**
     * X-coordinate of top center of GUI.
     */
    private int GUI_ANCHOR_MID_X;
    /**
     * Y-coordinate of top left of GUI.
     */
    private int GUI_ANCHOR_Y;
    private GuiButtonLockMenu lockUnlockButton;
    private GuiButtonLockMenu closeButton;
    private final int MAX_TRUSTEES;
    private final int textBoxWidth;
    private final int textBoxHeight;
    private final int textBoxAndButtonWidth; //
    private int numberOfActiveTextboxes = 1;
    private final List<String> currentTrustees;
    private final List<GuiTextField> textFieldList = new ArrayList<>();

    /**
     * @author 02skaplan
     */
    public GuiLockMenu(EntityPlayer editingPlayer, EntityRollingStock rollingStock) {
        this.editingPlayer = editingPlayer;
        this.rollingStock = rollingStock;
        currentTrustees = rollingStock.getTrustedList();
        MAX_TRUSTEES = 6;
        textBoxWidth = 100;
        textBoxHeight = 20;
        textBoxAndButtonWidth = textBoxWidth + 17 + 5;

    }

    /**
     * Initial setup for buttons and GUI anchors.
     */
    @Override
    public void initGui() {
        GUI_ANCHOR_MID_X = (this.width) / 2;
        GUI_ANCHOR_Y = (this.height) / 2 - (MENU_TEXTURE_HEIGHT / 2);
        GUI_ANCHOR_X = GUI_ANCHOR_MID_X - (MENU_TEXTURE_WIDTH) / 2;
        this.buttonList.clear();
        this.buttonList.add(this.lockUnlockButton = new GuiButtonLockMenu(0, GUI_ANCHOR_MID_X - (17 / 2), GUI_ANCHOR_Y + 10, 17, 25, rollingStock.locked ? GuiButtonLockMenu.Type.LOCKED : GuiButtonLockMenu.Type.UNLOCKED));
        this.buttonList.add(this.closeButton = new GuiButtonLockMenu(1, GUI_ANCHOR_X + MENU_TEXTURE_WIDTH - 22, GUI_ANCHOR_Y + 4, 17, 17, GuiButtonLockMenu.Type.CLOSE));

        textFieldList.clear();
        for (int i = 0; i < MAX_TRUSTEES; i ++) {
            this.buttonList.add(new GuiButtonLockMenu(i + 2, (GUI_ANCHOR_MID_X + (textBoxWidth / 2)), (GUI_ANCHOR_Y + 55 + (i * (textBoxHeight + 5))) + 2, 17, 17, GuiButtonLockMenu.Type.REMOVE));
            textFieldList.add(new GuiTextField(fontRendererObj, (GUI_ANCHOR_MID_X - (textBoxAndButtonWidth / 2)), (GUI_ANCHOR_Y + 55 + (i * (textBoxHeight + 5))), textBoxWidth, textBoxHeight));
            textFieldList.get(i).setCanLoseFocus(true);
            textFieldList.get(i).setMaxStringLength(16);
            if (i < currentTrustees.size())
                textFieldList.get(i).setText(currentTrustees.get(i));
        }
        numberOfActiveTextboxes = currentTrustees.size() == 0 ? 1 : currentTrustees.size() + 1;
        this.updateButtons();
    }

    private void updateButtons() {
        this.lockUnlockButton.showButton = true;
        this.lockUnlockButton.visible = true;
        this.lockUnlockButton.setType(rollingStock.getTrainLockedFromPacket() ? GuiButtonLockMenu.Type.LOCKED : GuiButtonLockMenu.Type.UNLOCKED, this.lockUnlockButton.getTexture());
        this.closeButton.showButton = true;
        this.closeButton.visible = true;
        for (int i = 0; i < MAX_TRUSTEES; i ++) {
            textFieldList.get(i).setVisible(i < numberOfActiveTextboxes);
            ((GuiButtonLockMenu) this.buttonList.get(i + 2)).visible = i < numberOfActiveTextboxes;
            ((GuiButtonLockMenu) this.buttonList.get(i + 2)).showButton = i < numberOfActiveTextboxes;
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float par3) {
        // Draw background.
        mc.renderEngine.bindTexture(new ResourceLocation(Info.resourceLocation, Info.guiPrefix + "gui_lockmenu.png"));
        this.drawTexturedModalRect(GUI_ANCHOR_X, GUI_ANCHOR_Y, 0, 0, MENU_TEXTURE_WIDTH, MENU_TEXTURE_HEIGHT);
        super.drawScreen(mouseX, mouseY, par3);
        for (int i = 0; i < numberOfActiveTextboxes; i++) {
            textFieldList.get(i).drawTextBox();
        }
        // Draw Hovering Tooltips
        // I split this up to hopefully reduce the amount of statements it has to process.
        fontRendererObj.drawString(StatCollector.translateToLocal("lockmenu.Title_Trusted_Players.name"), GUI_ANCHOR_MID_X - (fontRendererObj.getStringWidth(StatCollector.translateToLocal("lockmenu.Title_Trusted_Players.name")) / 2), GUI_ANCHOR_Y + 40, -16777216);
        if (lockUnlockButton.getTexture() == GuiButtonLockMenu.Texture.ACTIVE)
            if (rollingStock.locked)
                drawHoveringText(Collections.singletonList(StatCollector.translateToLocal("lockmenu.Unlock.name")), mouseX, mouseY, fontRendererObj);
            else
                drawHoveringText(Collections.singletonList(StatCollector.translateToLocal("lockmenu.Lock.name")), mouseX, mouseY, fontRendererObj);
        if (closeButton.getTexture() == GuiButtonLockMenu.Texture.ACTIVE)
            drawHoveringText(Collections.singletonList(StatCollector.translateToLocal("lockmenu.Save and Close.name")), mouseX, mouseY, fontRendererObj);

        if (!textFieldList.get(numberOfActiveTextboxes - 1).isFocused()) { // Get the last text box on the page...
            if (!textFieldList.get(numberOfActiveTextboxes - 1).getText().equalsIgnoreCase("")) { // If the text box is not empty...
                if (numberOfActiveTextboxes < MAX_TRUSTEES) { // Add another text box if there is room on the page.
                    numberOfActiveTextboxes++;
                    updateButtons();
                }
            }
        }

    }

    @Override
    protected void actionPerformed(GuiButton clickedButton) {
        if (clickedButton.enabled) {
            editingPlayer.playSound("random.click", 1f, 1f);
            if (clickedButton.id == 0) { // Main Lock Button
                rollingStock.locked = !rollingStock.locked;
                rollingStock.setTrainLockedFromPacket(rollingStock.locked);
                updateButtons();
            } else if (clickedButton.id == 1) { // Save and Close Button
                sendUpdatePacketToServer();
                this.mc.thePlayer.closeScreen();
            } else if (clickedButton.id > 1) { // Line Select Delete Buttons
                int textBoxToBeCleared = clickedButton.id - 2;
                textFieldList.get(textBoxToBeCleared).setText("");
                if (textBoxToBeCleared < numberOfActiveTextboxes) {
                    for (int i = textBoxToBeCleared; i < MAX_TRUSTEES - 1; i++) {
                        textFieldList.get(i).setText(textFieldList.get(i + 1).getText());
                    }
                    textFieldList.get(numberOfActiveTextboxes - 1).setText("");
                    if (numberOfActiveTextboxes != 1)
                        numberOfActiveTextboxes--;
                    updateButtons();
                }
            }
        }
    }
    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
    }

    @Override
    public void mouseClicked(int x, int y, int par3) {
        super.mouseClicked(x, y, par3);
        for (GuiTextField textField : textFieldList) {
            textField.mouseClicked(x, y, par3);
        }
    }

    @Override
    protected void keyTyped(char eventChar, int eventKey) {
        if (eventKey == 1) { // If ESC...
            sendUpdatePacketToServer();
            this.mc.thePlayer.closeScreen();
        }
        for (int i = 0; i < MAX_TRUSTEES; i++) {
            GuiTextField textField = textFieldList.get(i);
            if (textField.isFocused()) {
                textField.textboxKeyTyped(eventChar, eventKey);
                if (eventChar == '\r') {
                    textField.setFocused(false);
                }
                break;
            }
        }
    }

    private void sendUpdatePacketToServer() {
        List<String> newTrustees = new ArrayList<>();
        for (int i = 0; i < numberOfActiveTextboxes; i++) {
            if (!textFieldList.get(i).getText().isEmpty()) {
                newTrustees.add(textFieldList.get(i).getText().trim());
            }
        }
        Traincraft.lockChannel.sendToServer(new PacketSetTrainLockedToClient(rollingStock.locked, newTrustees, rollingStock.getEntityId()));
    }

}
