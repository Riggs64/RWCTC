package train.client.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import org.lwjgl.opengl.GL11;
import train.common.Traincraft;
import train.common.api.EntityRollingStock;
import train.common.core.network.PacketTextureOverlayConfig;
import train.common.library.Info;
import train.common.overlaytexture.OverlayTextureManager;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Collections;

/**
 * @author 02skaplan
 * <p>Fixed Overlay GUI for Paintbrush</p>
 */
@SuppressWarnings("unchecked")
@SideOnly(Side.CLIENT)
public class GuiFixedOverlay extends GuiScreen {
    private final EntityPlayer editingPlayer;
    private final EntityRollingStock rollingStock;
    final private int MENU_TEXTURE_WIDTH = 206;
    final private int MENU_TEXTURE_HEIGHT = 200;
    final private int RESULTS_PER_PAGE = 8;
    /**
     * X-coordinate of top left of GUI.
     */
    private int GUI_ANCHOR_X;
    /**
     * X-coordinate of top center of GUI.
     */
    private int GUI_ANCHOR_MID_X;
    /**
     * Y-coordinate of top left of GUI.
     */
    private int GUI_ANCHOR_Y;
    private boolean hasNextPage;

    private GuiButtonPaintbrushMenu arrowUp;
    private GuiButtonPaintbrushMenu arrowDown;
    private GuiButtonPaintbrushMenu textureOne;
    private GuiButtonPaintbrushMenu textureTwo;
    private GuiButtonPaintbrushMenu textureThree;
    private GuiButtonPaintbrushMenu textureFour;
    private GuiButtonPaintbrushMenu textureFive;
    private GuiButtonPaintbrushMenu textureSix;
    private GuiButtonPaintbrushMenu textureSeven;
    private GuiButtonPaintbrushMenu textureEight;
    private GuiButtonPaintbrushMenu closeMenuButton;
    private int optionsOnCurrentPage;
    private final int totalOptions;
    private int currentPage;
    private Integer selectedOverlay = null;
    private BufferedImage subTextureRenderImage;
    private ResourceLocation subTextureRenderImageLocation;

    /**
     * @author 02skaplan
     */
    public GuiFixedOverlay(EntityPlayer editingPlayer, EntityRollingStock rollingStock) {
        this.editingPlayer = editingPlayer;
        this.rollingStock = rollingStock;
        currentPage = 0;
        totalOptions = rollingStock.getOverlayTextureContainer().getSpecificationFixed().getNumberOfOverlaysOnSheet();
        if (rollingStock.getOverlayTextureContainer().getType() == OverlayTextureManager.Type.FIXED) { // Set page to the page with the currently selected texture.
            selectedOverlay = rollingStock.getOverlayTextureContainer().getSpecificationFixed().getSelectedOverlay();
            currentPage = (selectedOverlay - 1) / RESULTS_PER_PAGE;
        }
        optionsOnCurrentPage = Math.min(RESULTS_PER_PAGE, totalOptions);
        hasNextPage = optionsOnCurrentPage + RESULTS_PER_PAGE * currentPage < totalOptions;
        updateSubtextureChoices();
    }

    /**
     * Initial setup for buttons and GUI anchors.
     */
    @Override
    public void initGui() {
        GUI_ANCHOR_MID_X = (this.width) / 2;
        GUI_ANCHOR_Y = (this.height) / 2 - (MENU_TEXTURE_HEIGHT / 2);
        GUI_ANCHOR_X = GUI_ANCHOR_MID_X - MENU_TEXTURE_WIDTH;
        this.buttonList.clear();
        this.buttonList.add(this.arrowUp = new GuiButtonPaintbrushMenu(0, GUI_ANCHOR_X + 388, GUI_ANCHOR_Y + 59, 12, 38, GuiButtonPaintbrushMenu.Type.ARROWUP));
        this.buttonList.add(this.arrowDown = new GuiButtonPaintbrushMenu(1, GUI_ANCHOR_X + 388, GUI_ANCHOR_Y + 103, 12, 38, GuiButtonPaintbrushMenu.Type.ARROWDOWN));
        this.buttonList.add(this.closeMenuButton = new GuiButtonPaintbrushMenu(2, GUI_ANCHOR_X + 382, GUI_ANCHOR_Y + 10, 22, 22, GuiButtonPaintbrushMenu.Type.CLOSE));

        this.buttonList.add(this.textureOne = new GuiButtonPaintbrushMenu(3, GUI_ANCHOR_X + 10, GUI_ANCHOR_Y + 12, 85, 85, GuiButtonPaintbrushMenu.Type.SELECTIONBOX));
        this.buttonList.add(this.textureTwo = new GuiButtonPaintbrushMenu(4, GUI_ANCHOR_X + 104, GUI_ANCHOR_Y + 12, 85, 85, GuiButtonPaintbrushMenu.Type.SELECTIONBOX));
        this.buttonList.add(this.textureThree = new GuiButtonPaintbrushMenu(5, GUI_ANCHOR_X + 198, GUI_ANCHOR_Y + 12, 85, 85, GuiButtonPaintbrushMenu.Type.SELECTIONBOX));
        this.buttonList.add(this.textureFour = new GuiButtonPaintbrushMenu(6, GUI_ANCHOR_X + 292, GUI_ANCHOR_Y + 12, 85, 85, GuiButtonPaintbrushMenu.Type.SELECTIONBOX));
        this.buttonList.add(this.textureFive = new GuiButtonPaintbrushMenu(7, GUI_ANCHOR_X + 10, GUI_ANCHOR_Y + 103, 85, 85, GuiButtonPaintbrushMenu.Type.SELECTIONBOX));
        this.buttonList.add(this.textureSix = new GuiButtonPaintbrushMenu(8, GUI_ANCHOR_X + 104, GUI_ANCHOR_Y + 103, 85, 85, GuiButtonPaintbrushMenu.Type.SELECTIONBOX));
        this.buttonList.add(this.textureSeven = new GuiButtonPaintbrushMenu(9, GUI_ANCHOR_X + 198, GUI_ANCHOR_Y + 103, 85, 85, GuiButtonPaintbrushMenu.Type.SELECTIONBOX));
        this.buttonList.add(this.textureEight = new GuiButtonPaintbrushMenu(10, GUI_ANCHOR_X + 292, GUI_ANCHOR_Y + 103, 85, 85, GuiButtonPaintbrushMenu.Type.SELECTIONBOX));
        this.updateButtons();
    }

    private void updateButtons() {
        this.arrowUp.visible = (currentPage != 0);
        this.arrowUp.showButton = (currentPage != 0);
        this.arrowDown.visible = hasNextPage;
        this.arrowDown.showButton = hasNextPage;
        this.textureOne.showButton = true;
        this.textureOne.visible = true;
        this.textureTwo.showButton = optionsOnCurrentPage > 1;
        this.textureTwo.visible = optionsOnCurrentPage > 1;
        this.textureThree.showButton = optionsOnCurrentPage > 2;
        this.textureThree.visible = optionsOnCurrentPage > 2;
        this.textureFour.showButton = optionsOnCurrentPage > 3;
        this.textureFour.visible = optionsOnCurrentPage > 3;
        this.textureFive.showButton = optionsOnCurrentPage > 4;
        this.textureFive.visible = optionsOnCurrentPage > 4;
        this.textureSix.showButton = optionsOnCurrentPage > 5;
        this.textureSix.visible = optionsOnCurrentPage > 5;
        this.textureSeven.showButton = optionsOnCurrentPage > 6;
        this.textureSeven.visible = optionsOnCurrentPage > 6;
        this.textureEight.showButton = optionsOnCurrentPage > 7;
        this.textureEight.visible = optionsOnCurrentPage > 7;
        this.closeMenuButton.showButton = true;
        this.closeMenuButton.visible = true;
        if (selectedOverlay != null) {
            if (currentPage == ((selectedOverlay - 1) / RESULTS_PER_PAGE)) { // If overlay is on current page, set the selected overlay to active and the rest to inactive.
                int numberOfActiveOverlayInGUI = (selectedOverlay - 1) % RESULTS_PER_PAGE; // Which button corresponds to the active overlay...
                for (int i = 3; i < this.buttonList.size(); i++) {
                    if (buttonList.get(i) instanceof GuiButtonPaintbrushMenu) {
                        if (i - 3 == numberOfActiveOverlayInGUI) {
                            ((GuiButtonPaintbrushMenu) buttonList.get(i)).setType(GuiButtonPaintbrushMenu.Type.SELECTIONBOX, GuiButtonPaintbrushMenu.Texture.ACTIVE);
                        } else {
                            ((GuiButtonPaintbrushMenu) buttonList.get(i)).setType(GuiButtonPaintbrushMenu.Type.SELECTIONBOX, GuiButtonPaintbrushMenu.Texture.INACTIVE);
                        }
                    }
                }
            } else { // If selected overlay is not on current page, set all buttons to inactive.
                for (int i = 3; i < this.buttonList.size(); i++) {
                    if (buttonList.get(i) instanceof GuiButtonPaintbrushMenu) {
                        ((GuiButtonPaintbrushMenu) buttonList.get(i)).setType(GuiButtonPaintbrushMenu.Type.SELECTIONBOX, GuiButtonPaintbrushMenu.Texture.INACTIVE);
                    }
                }
            }
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float par3) {
        // Draw background.
        mc.renderEngine.bindTexture(new ResourceLocation(Info.resourceLocation, Info.guiPrefix + "gui_paintbrush_menu_right.png"));
        this.drawTexturedModalRect(GUI_ANCHOR_MID_X, GUI_ANCHOR_Y, 0, 0, MENU_TEXTURE_WIDTH, MENU_TEXTURE_HEIGHT);
        mc.renderEngine.bindTexture(new ResourceLocation(Info.resourceLocation, Info.guiPrefix + "gui_paintbrush_menu_left.png"));
        this.drawTexturedModalRect(GUI_ANCHOR_X, GUI_ANCHOR_Y, 0, 0, MENU_TEXTURE_WIDTH, MENU_TEXTURE_HEIGHT);
        renderSubtextureChoices();

        super.drawScreen(mouseX, mouseY, par3);

        // Draw Hovering Tooltips
        // I split this up to hopefully reduce the amount of statements it has to process.
        if (mouseX > closeMenuButton.xPosition - 5) { // If mouse is on the right-hand side after the textures.
            if (closeMenuButton.getTexture() == GuiButtonPaintbrushMenu.Texture.ACTIVE)
                drawHoveringText(Collections.singletonList(StatCollector.translateToLocal("paintbrushmenu.Close Menu.name")), mouseX, mouseY, fontRendererObj);
            else if (arrowUp.getTexture() == GuiButtonPaintbrushMenu.Texture.ACTIVE && arrowUp.visible)
                drawHoveringText(Collections.singletonList(StatCollector.translateToLocal("paintbrushmenu.Previous Page.name")), mouseX, mouseY, fontRendererObj);
            else if (arrowDown.getTexture() == GuiButtonPaintbrushMenu.Texture.ACTIVE && arrowDown.visible)
                drawHoveringText(Collections.singletonList(StatCollector.translateToLocal("paintbrushmenu.Next Page.name")), mouseX, mouseY, fontRendererObj);
        }
    }

    @Override
    protected void actionPerformed(GuiButton clickedButton) {
        if (clickedButton.enabled) {
            editingPlayer.playSound("random.click", 1f, 1f);
            // Select Color
            if (clickedButton.id < 3) { // Page up or down button.
                if (clickedButton.id == 0) { // If page up...
                    currentPage--;
                } else if (clickedButton.id == 1) { // If page down...
                    currentPage++;
                } else { // Close button...
                    this.mc.thePlayer.closeScreen();
                }
                hasNextPage = optionsOnCurrentPage + RESULTS_PER_PAGE * currentPage < totalOptions;
                optionsOnCurrentPage = Math.min(RESULTS_PER_PAGE, totalOptions - currentPage * RESULTS_PER_PAGE);
                updateButtons();
                updateSubtextureChoices();
            } else if (clickedButton.id < 11){ // Overlay selection button.
                int newOverlayNumber = (currentPage * RESULTS_PER_PAGE) + (clickedButton.id - 2);
                // Send an update packet to the server of texture change.
                // We don't actually change the texture on the client yet; we let the server know we changed it.
                // After the server recognizes that we changed it, it will send an update packet out to all clients.
                if (newOverlayNumber != 0) {
                    rollingStock.getOverlayTextureContainer().getSpecificationFixed().setSelectedOverlay(newOverlayNumber);
                    Traincraft.overlayTextureChannel.sendToServer(new PacketTextureOverlayConfig(OverlayTextureManager.Type.FIXED, rollingStock.getEntityId(), Minecraft.getMinecraft().thePlayer.worldObj.provider.dimensionId, rollingStock.getOverlayTextureContainer().getOverlayConfigTag()));
                }
                this.mc.thePlayer.closeScreen();
            }
        }
    }
    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
    }

    @Override
    protected void keyTyped(char eventChar, int eventKey) {
        if (eventKey == 1 || eventChar == 'e') { // If ESC...
            this.mc.thePlayer.closeScreen();
        } else {
            switch (eventChar) {
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8': {
                    if (Character.getNumericValue(eventChar) <= optionsOnCurrentPage) {
                        if (totalOptions >= Character.getNumericValue(eventChar)) {
                            rollingStock.getOverlayTextureContainer().getSpecificationFixed().setSelectedOverlay((currentPage * RESULTS_PER_PAGE) + (Character.getNumericValue(eventChar)));
                            Traincraft.overlayTextureChannel.sendToServer(new PacketTextureOverlayConfig(OverlayTextureManager.Type.FIXED, rollingStock.getEntityId(), Minecraft.getMinecraft().thePlayer.worldObj.provider.dimensionId, rollingStock.getOverlayTextureContainer().getOverlayConfigTag()));
                        }
                    }
                }
            }
        }
    }
    private void renderSubtextureChoices() {
        int offsetX = GUI_ANCHOR_X + 10;
        int offsetY = GUI_ANCHOR_Y + 12;
        int subTextureHeight = rollingStock.getOverlayTextureContainer().getSpecificationFixed().getHeightOfEachOverlay();
        int subTextureWidth = rollingStock.getOverlayTextureContainer().getSpecificationFixed().getWidthOfEachOverlay();
        int subTextureOffsetX = (85 / 2) - (subTextureWidth / 2);
        int subTextureOffsetY = (85 / 2) - (subTextureHeight / 2);

        mc.renderEngine.bindTexture(subTextureRenderImageLocation);
        GL11.glPushMatrix();
        GL11.glTranslated(0, 0, 400);
        for (int i = 0; i < optionsOnCurrentPage; i++) {
            this.drawTexturedModalRect(offsetX + subTextureOffsetX, offsetY + subTextureOffsetY, 0, i * subTextureHeight, subTextureWidth, subTextureHeight);
            offsetX += 95;
            if (offsetX > GUI_ANCHOR_X + 372) {
                offsetX = GUI_ANCHOR_X + 10;
                offsetY += 92;
            }
        }
        GL11.glPopMatrix();
    }

    /**
     * <p>Loads current selection of subtexture choices onto a separate (1:1 aspect ratio) resource image to use when
     * rendering the subtexture choices. If subtexture sheet is not 1:1, drawTexturedModalRect will draw incorrectly.</p>
     */
    private void updateSubtextureChoices() {
        int subTextureWidth = rollingStock.getOverlayTextureContainer().getSpecificationFixed().getWidthOfEachOverlay();
        int subTextureHeight = rollingStock.getOverlayTextureContainer().getSpecificationFixed().getHeightOfEachOverlay();
        int offsetY = currentPage * RESULTS_PER_PAGE * subTextureHeight;
        try {
            BufferedImage overlaySheet = ImageIO.read(Minecraft.getMinecraft().getResourceManager().getResource(new ResourceLocation(Info.resourceLocation, Info.fixedOverlayTexturePrefix + rollingStock.getOverlayTextureContainer().getSpecificationFixed().getOverlaySheetFilePath())).getInputStream());
            subTextureRenderImage = new BufferedImage(256, 256, BufferedImage.TYPE_INT_ARGB);
            for (int i = 0; i < optionsOnCurrentPage; i++) {
                subTextureRenderImage.getGraphics().drawImage(overlaySheet.getSubimage(0, offsetY, subTextureWidth, subTextureHeight), 0, i * subTextureHeight, subTextureWidth, subTextureHeight, null);
                offsetY += subTextureHeight;
            }
            subTextureRenderImage.getGraphics().dispose();
            //todo remove anonymous reference to dynamictexture to fix potential for memory leak
            subTextureRenderImageLocation = Minecraft.getMinecraft().getTextureManager().getDynamicTextureLocation("", new DynamicTexture(subTextureRenderImage));
        } catch (IOException ignored) {
            System.out.println("[RWCTC] Fixed overlay menu rendering error.");
        }
    }
}
